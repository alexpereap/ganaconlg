<?php

use Symfony\Component\Routing;

$routes = new Routing\RouteCollection();

//Homepage
$routes->add('homepage', new Routing\Route('/', array(
    '_controller' => 'LgWorldCup\Controller\MainController::promoOutAction'
)));

//Like
$routes->add('like', new Routing\Route('/like', array(
    '_controller' => 'LgWorldCup\Controller\MainController::likeAction'
)));

//Register
$routes->add('register', new Routing\Route('/register', array(
		'_controller' => 'LgWorldCup\Controller\MainController::registerAction'
)));

//Facebook login
$routes->add('facebook_login', new Routing\Route('/facebook-login', array(
'_controller' => 'LgWorldCup\Controller\MainController::facebookLoginAction'
)));

//Thanks
$routes->add('thanks', new Routing\Route('/thanks', array(
		'_controller' => 'LgWorldCup\Controller\MainController::thanksAction'
)));

//Register list
$routes->add('register_list', new Routing\Route('/register-list', array(
		'_controller' => 'LgWorldCup\Controller\MainController::registerListAction'
)));

//Deleter register
$routes->add('delete_register', new Routing\Route('/delete-register/{id}', array(
		'_controller' => 'LgWorldCup\Controller\MainController::deleteRegisterAction'
)));

//Download excel
$routes->add('download_excel', new Routing\Route('/descargar-excel', array(
		'_controller' => 'LgWorldCup\Controller\MainController::downloadExcelAction'
)));

//Generate winners
$routes->add('generate_winners', new Routing\Route('/ganadores', array(
		'_controller' => 'LgWorldCup\Controller\MainController::generateWinnersAction'
)));

return $routes;