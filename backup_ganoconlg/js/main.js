function showAlertModal(message)
{
	$('#alertModal .message').html(message);
	$('#alertModal').modal('show');
}

function fillForm()
{
	FB.api('/me?fields=id,name,email,birthday', function(response) 
	{
		$('#register_name').val(response.name);
		$('#register_email').val(response.email);
		var birthdate = response.birthday.split("/");
		$('#register_birthdate_day').val(birthdate[1]);
		$('#register_birthdate_month').val(birthdate[0]);
		$('#register_birthdate_year').val(birthdate[2]);
	});
}

function facebookLogin()
{
	FB.login(function(response)
	{
		if (response.authResponse) {
			window.location = "http://ganoconlg.com/facebook-login";
		}
	}, {scope: 'basic_info, email, user_birthday, publish_actions'});
};

var sending = false;
$(document).ready(function()
{
	//Facebook
	$.ajaxSetup({ cache: true });
	$.getScript('//connect.facebook.net/es_LA/all.js', function()
	{
		FB.init({
			appId: '1452880464948036',
	        status     : true,
	        xfbml      : true
	    });     
		FB.Event.subscribe('auth.statusChange', function(response) 
		{
		    if (response.status === 'connected') {
		      fillForm();
		    } else {
		      facebookLogin();
		    }
		});
	});
	  
	$('.facebookButton').click(function()
	{
		facebookLogin();
	});
	
	//Form	
	$("section#form form").validate(
	{
		onkeyup: false,
		onclick: false,
		onfocusout: false,
		errorPlacement: function(error, element) 
		{
		},
		highlight: function(element, errorClass, validClass) 
		{
		    $(element).addClass(errorClass).removeClass(validClass);
		},
		unhighlight: function(element, errorClass, validClass) 
		{
		    $(element).removeClass(errorClass).addClass(validClass);
		},
		invalidHandler: function(event, validator)
		{
			showAlertModal("Debes completar todos los campos obligatorios.");
		},
		submitHandler: function(form)
		{
			var fixedMonto = $('#register_monto').val().replace(',', '');
			$('#register_monto').val(fixedMonto);
			
			if(!sending)
			{				
				sending = true;
				$('#sending').show();
				
				$.ajax({
	                type: "POST",
	                url: $(form).attr('action'),
	                data: $(form).serialize(),
	                dataType: 'json',
	                error: function()
	                {
	                	sending = false;
	                	$('#sending').hide();
	                	showAlertModal('No se pudo completar el registro correctamente. Espere unos minutos y vuelva a intentarlo.');
	                },
	                success: function(data, textStatus, jqXHR)
	                {
	                	$('#sending').hide();
	                	
	                	if (textStatus === 'error') 
			            {
			                console.log("error");
			                sending = false;
			            } else 
			            {		         
			            	if (data.onError == 'true')
			            	{
			            		showAlertModal(data.message);
			            		sending = false;
			            	}else
			            	{
			            		$(form).trigger("reset");
			            		self.location.href = data.redirectUrl;
			            	}
			            }
	                }
				});
			}
		}
	});
});