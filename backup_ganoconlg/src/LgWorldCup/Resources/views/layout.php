<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>LG Premia con 10,000</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        
        <link rel="shortcut icon" href="<?php echo $view['assets']->getUrl('favicon.png') ?>" type="image/png" />
		
        <link rel="stylesheet" href="<?php echo $view['assets']->getUrl('css/bootstrap.min.css') ?>">
        <link rel="stylesheet" href="<?php echo $view['assets']->getUrl('css/main.css') ?>">
        <link rel="stylesheet" href="css/main.css">
        
	<script src="<?php echo $view['assets']->getUrl('js/vendor/html5shiv.js') ?>"></script> 
        <?php /*if($container->get('mobile_detect')->isMobile()): ?>
        <link rel="stylesheet" href="<?php echo $view['assets']->getUrl('css/main.mobile.css') ?>">
        <?php endif;*/ ?>     
    </head>
    <body<?php echo $container->get('mobile_detect')->isMobile()?' class="mobile"':''?>>
    	<?php echo $content ?>    	
    	
    	<div id="alertModal" class="modal fade">
  			<div class="modal-dialog">
    			<div class="modal-content">
      				<div class="modal-header">
        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      				</div>
      				<div class="modal-body">
        				<p class="message">One fine body&hellip;</p>
      				</div>
    			</div>
  			</div>
		</div>
		
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-49612361-1', 'ganaconlge.com');
	  ga('send', 'pageview');
	</script>
    	<script src="<?php echo $view['assets']->getUrl('js/vendor/jquery-1.10.2.min.js') ?>"></script>
        <script src="<?php echo $view['assets']->getUrl('js/vendor/bootstrap.min.js') ?>"></script>
        <script src="<?php echo $view['assets']->getUrl('js/vendor/jquery.validate.min.js') ?>"></script>
        <script src="<?php echo $view['assets']->getUrl('js/main.js') ?>"></script>       
    </body>
</html>