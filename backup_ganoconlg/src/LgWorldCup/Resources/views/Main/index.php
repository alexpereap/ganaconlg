<section id="form">
	<img src="images/form-background.jpg" />
	<form action="<?php echo $container->get('routing.generator')->generate('register') ?>" method="post">		
		<?php //if(!$facebook->getUser()): ?>
		<a href="#" class="facebookButton"><img src="images/facebook-connect-button.png"></a>
		<?php //endif; ?>			
		<fieldset class="mainFields">
			<div class="row-field clearfix">
				<div class="field name">
					<label for="register_name">Nombre y apellidos:</label>
					<input type="text" name="register[name]" id="register_name"
						data-rule-required="true" 
					/>
					<img src="images/candado.png" class="candado">
				</div>
				<div class="field cedula">
					<label for="register_cedula">Cédula:</label>
					<input type="text" name="register[cedula]" id="register_cedula"
						data-rule-required="true" 
					/>
					<img src="images/candado.png" class="candado">
				</div>
			</div>
			<div class="row-field clearfix">
				<div class="field email">
					<label for="register_email">E-mail:</label>
					<input type="text" name="register[email]" id="register_email"
						data-rule-required="true"
						data-rule-email="true" 
					/>
					<img src="images/candado.png" class="candado">
				</div>
				<div class="field phone">
					<label for="register_phone">Teléfono:</label>
					<input type="text" name="register[phone]" id="register_phone"
						data-rule-required="true" 
					/>
					<img src="images/candado.png" class="candado">
				</div>
			</div>
			<div class="row-field clearfix">
				<div class="field birthdate">
					<label for="register_birthdate">Fecha de nacimiento:</label>
					<select name="register[birthdate][day]" id="register_birthdate_day">
						<?php for ($day=1;$day<=31;$day++): ?>
						<option value="<?php echo str_pad($day, 2, "0", STR_PAD_LEFT); ?>"><?php echo str_pad($day, 2, "0", STR_PAD_LEFT); ?></option>
						<?php endfor; ?>
					</select>
					<select name="register[birthdate][month]" id="register_birthdate_month">
						<?php $months = array('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'); ?>
						<?php foreach ($months as $month => $monthName): ?>
						<option value="<?php echo str_pad($month+1, 2, "0", STR_PAD_LEFT); ?>"><?php echo $monthName ?></option>
						<?php endforeach; ?>
					</select>
					<select name="register[birthdate][year]" id="register_birthdate_year">
						<?php for ($year=1940;$year<=2006;$year++): ?>
						<option value="<?php echo $year ?>"><?php echo $year ?></option>
						<?php endfor; ?>
					</select>
				</div>
				<div class="field country">
					<label for="register_product">Producto:</label>
					<select name="register[product]">
						<option value="Televisor">Televisor</option>
						<option value="Mini componente">Mini componente</option>
						<option value="Equipo de Sonido">Equipo de Sonido</option>
						<option value="Teatro en casa">Teatro en casa</option>
						<option value="DVD">DVD</option>
						<option value="Blu-ray">Blu-ray</option>
						<option value="Aire Acondicionado">Aire Acondicionado</option>
						<option value="Refrigeradora">Refrigeradora</option>
						<option value="Lavadora">Lavadora</option>
						<option value="Microondas">Microondas</option>
						<option value="Lavaplatos">Lavaplatos</option>
						<option value="Aspiradoras">Aspiradoras</option>
					</select>
				</div>
			</div>
			<div class="row-field clearfix">
				<div class="field factura">
					<label for="register_factura">N° Factura:</label>
					<input type="text" name="register[factura]" id="register_factura"
						data-rule-required="true" 
					/>
				</div>
				<div class="field monto">
					<label for="register_monto">Monto:</label>
					<input type="text" name="register[monto]" id="register_monto"
						data-rule-required="true"
						data-rule-number="true"  
					/>
				</div>
				<div class="field tienda">
					<label for="register_tienda">Tienda:</label>
					<select name="register[tienda]">
						<option value="Panafoto">Panafoto</option>
						<option value="Audiofoto">Audiofoto</option>
						<option value="Casa Gala">Casa Gala</option>
						<option value="Multimax">Multimax</option>
						<option value="Hometek">Hometek</option>
						<option value="Créditos Mundiales">Créditos Mundiales</option>
						<option value="Spigel">Spigel</option>
						<option value="Jerusalem">Jerusalem</option>
						<option value="Daka">Daka</option>
						<option value="Casa Confort">Casa Confort</option>
						<option value="Elektra">Elektra</option>
						<option value="Raenco">Raenco</option>
						<option value="Infox">Infox</option>
						<option value="E-Vision">E-Vision</option>
						<option value="Photura">Photura</option>
						<option value="Unica">Unica</option>
						<option value="Do it center">Do it center</option>
						<option value="otros">otros</option>
					</select>
					<img src="images/candado.png" class="candado">
				</div>
			</div>	
		</fieldset>
		
		<fieldset class="otherFields">
			<div class="row-field clearfix">
				<p class="newsletterTitle">¿Eres de los que le gusta adquirir los nuevos productos antes que los demás?</p>
				<div class="field newsletter">
					<input type="radio" name="register[newsletter]" id="register_newsletter_yes" value="Sí" checked /> <label for="register_newsletter_yes">Sí</label>
					<input type="radio" name="register[newsletter]" id="register_newsletter_no" value="No" /> <label for="register_newsletter_no">No</label>
				</div>
				<p class="termsTitle">Términos y condicones:</p>
				<div class="field terms">
					<input type="checkbox" name="register[terms]" id="register_terms"
						data-rule-required="true" 
					/>
					<label for="register_terms">He leído y acepto los <a href="terminos-y-condiciones.pdf" target="_blank">términos y condiciones</a>.</label>
				</div>				
			</div>
			<button type="submit"><img src="images/submitButton.png"></button>
			<p id="sending">Enviando...</p>
		</fieldset>
		
		<div class="support">
			<p>¿Necesitas soporte? Escríbenos a <a href="mailto:zinia.english@lge.com" target="_blank">zinia.english@lge.com</a></p>
		</div>
	</form>
</section>