<?php

namespace LgWorldCup\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Templating\PhpEngine;
use Symfony\Component\Templating\TemplateNameParser;
use Symfony\Component\Templating\Loader\FilesystemLoader;
use Gecky\Controller\Controller;

class MainController extends Controller
{		
	public static $REGISTER_ERROR = 'No se pudo completar el registro correctamente. Espere unos minutos y vuelva a intentarlo.';
	public static $RECEIPT_EXISTS = 'No se pudo completar el registro debido a que la factura ya ha sido usada previamente.';
	
	public function indexAction(Request $request)
	{
		$facebook = $this->container->get('facebook');
		$signedRequest = $facebook->getSignedRequest();
		$loginUrl = $facebook->getConfiguredLoginUrl();
		
		$mobileDetect = $this->container->get('mobile_detect');		
		$isMobile = $mobileDetect->isMobile();
		
		if (!$isMobile && $signedRequest == null)
		{
			//return new RedirectResponse($facebook->getTabUrl());
		}
				
		if($signedRequest && !$signedRequest['page']['liked'])
		{
			//return new RedirectResponse('like');
		}
		
		return $this->render('Main/index.php', array(				
			'facebook' => $facebook,
			'isMobile' => $isMobile,
			'loginUrl' => $loginUrl
		));
	}
	
	public function likeAction(Request $request)
	{
		return $this->render('Main/like.php');
	} 
	
	public function thanksAction(Request $request)
	{
		return $this->render('Main/thanks.php');
	}
	
	public function promoOutAction(Request $request)
	{
		return $this->render('Main/promoOut.php');
	}
	
	public function registerAction(Request $request)
	{
		$response = array('onError' => 'true', 'message' => self::$REGISTER_ERROR);
		
		/** REGISTER INTO DB **/
		$register = $request->get('register');
		$name = $register['name'];
		$cedula = $register['cedula'];
		$email = $register['email'];
		$phone = $register['phone'];
		$birthdate = $register['birthdate'];
		$product = $register['product'];
		$factura = $register['factura'];
		$monto = $register['monto'];
		$tienda = $register['tienda'];
		$newsletter = (isset($register['newsletter']) && $register['newsletter']!='')?1:0;
		$terms = $register['terms'];		

		$birthdate = $birthdate['year'].'-'.$birthdate['month'].'-'.$birthdate['day'];
		
		//Guardado en base de datos
		try {
			$conn = $this->container->get('database')->getConnection();
			$result = $conn->insert('user', array(
					'name' => $name, 'cedula' => $cedula, 'email' => $email, 'phone' => $phone,
					'birthdate' => $birthdate, 'product' => $product, 'factura' => $factura, 'monto' => $monto,
					'tienda' => $tienda, 'newsletter' => $newsletter,
					'created_at' => date('Y-m-d')
			));
			
			$userId = $conn->lastInsertId();
			
			if($result == true && $userId)
			{
				$response = ['onError' => 'false', 'message' => 'proceso ok.', 'redirectUrl' => $this->container->get('routing.generator')->generate('thanks')];
				
				/** EMAIL **/
				$destinatario = $email;
				$asunto = "[LG premia con 10,000] Registro realizado correctamente";
				$cuerpo = '
<html>
	<head>
		<title>LG premia con 10,000</title>
	</head>
	<body>
		<p>�Enhorabuena '.$name.'!</p>
		<br>
		<p>Tus datos han sido ingresados y est�s participando para la t�mbola del 7 de julio, donde tienes oportunidad de ganar 5,000 3,000. o 2,000 mil d�lares en productos LG para que armes tu estadio en casa.</p>
		<br><br>
		<p>�Buena suerte!</p>
	</body>
</html>
';				
				//Env�o en formato HTML
				$headers = "MIME-Version: 1.0\r\n";
				$headers .= "Content-type: text/html; charset=utf-8\r\n";
				
				//Direcci�n del remitente
				//$headers .= "From: ".$fullname." <".$email.">\r\n";
				
				@mail($destinatario, $asunto, $cuerpo, $headers);
				
				/** FACEBOOK **/
				$facebook = $this->container->get('facebook');
				if($facebook->getUser())
				{
					$ret_obj = $facebook->api('/me/feed', 'POST',
						array(
							'link' => $facebook->getAppHost(),
							'name' => 'LG Premia con 10,000',
							'message' => 'Estoy participando en la Promo LG premia con 10,000.',
							'caption' => 'Estoy participando en la Promo LG premia con 10,000',
							'picture' => $facebook->getAppHost().'/images/logo.png'
						)
					);
				}
			}else 
			{
				//using response error
			}
		}catch (\Exception $e)
		{
			if($e instanceof \Doctrine\DBAL\DBALException)
			{
				$response = array('onError' => 'true', 'message' => self::$RECEIPT_EXISTS);
			}
		}	
		
		return new JsonResponse($response);
	}
	
	public function facebookLoginAction(Request $request)
	{
		$facebook = $this->container->get('facebook');
		if(!$facebook->getUser())
		{
			$loginUrl = $facebook->getConfiguredLoginUrl();
			return new RedirectResponse($loginUrl);
		}
		
		return new RedirectResponse($this->container->get('routing.generator')->generate('homepage'));
	}
	
	public function registerListAction(Request $request)
	{
		//Get all data
		$conn = $this->container->get('database')->getConnection();
		$registers = $conn->fetchAll('SELECT * FROM user AS u ORDER BY u.is_winner DESC, u.created_at DESC');
		
		return $this->render('Main/registerList.php', array(
			'registers' => $registers 
		), 'admin_layout');
	}
	
	public function deleteRegisterAction(Request $request)
	{
		//Get all data
		$conn = $this->container->get('database')->getConnection();
		$conn->delete('user', array('id' => $request->get('id')));
		
		return new RedirectResponse($this->container->get('routing.generator')->generate('register_list'));
	}
	
	public function generateWinnersAction(Request $request)
	{
		$conn = $this->container->get('database')->getConnection();
		
		//Unset old winners
		$conn->update('user', array('is_winner' => false), array(1 => 1));
		
		//Generate winners
		$possibleWinners = array();
		$registers = $conn->fetchAll('SELECT * FROM user AS u');
		foreach ($registers as $register)
		{
			$posibilities = ceil($register['monto'] / 200);
			for ($i=1; $i <= $posibilities; $i++)
			{
				$possibleWinners[] = $register['id'];
			}
		}
		
		shuffle($possibleWinners);
		$winners = array();
		foreach ($possibleWinners as $possibleWinner)
		{
			if(count($winners) >= 1)
				break;
			
			if(!in_array($possibleWinner, $winners))
			{
				$winners[] = $possibleWinner;
			}
		}
		
		foreach ($winners as $winner)
		{
			$conn->update('user', array('is_winner' => true), array('id' => $winner));
		}
		
		return new RedirectResponse($this->container->get('routing.generator')->generate('register_list'));
	}
	
	public function downloadExcelAction(Request $request)
	{
		//Guardado en base de datos
		$conn = $this->container->get('database')->getConnection();
		$registers = $conn->fetchAll('SELECT * FROM user AS u ORDER BY u.is_winner DESC, u.created_at DESC');
	
		$view = $this->templating->render('Main/downloadExcel.php', array(
			'registers' => $registers
		));
	
		$response = new Response($view); 
		$response->headers->set('Content-Type', 'application/octet-stream');
		$response->headers->set('Content-Disposition', 'attachment; filename=registros.xls');
		$response->headers->set('Pragma', 'no-cache');
		$response->headers->set('Expires', '0');
		
		return $response;
	}
	
	protected function getViewsDir()
	{
		return __DIR__.'/../Resources/views/%name%';
	}
}