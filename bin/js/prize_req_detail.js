$(document).ready(function(){

    $("#request_status").change(function(){

        if( $(this).val() == 'aprobado' ){

            $("#sendApprovalMail").prop('checked',true);
            $("#reqNotimailWrapper").slideDown();

        } else {

            $("#sendApprovalMail").prop('checked',false);
            $("#reqNotimailWrapper").slideUp();
        }
    });
});