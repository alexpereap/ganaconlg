var controllers = {};
var cmsApp = angular.module('cmsApp', [] );


$(document).ready(function(){

    $("#cancelBtn").click(function(){

        if( confirm("Deseas cancelar el envío del fomrulario") ){
            window.location.href = $("#cancelBtn").attr('route');
        }
    });

    $('.cms-datepicker').datepicker({
        format: "yyyy-mm-dd",
        language: 'es'
    });

    $(".make-fancy-multiselect").multiSelect();

    $("#sellpointsList").jstree({
        "plugins" : [ "checkbox" ]
    });


    $("#prizePackageForm").submit(function(){

        sell_point_ids = $("#sellpointsList").jstree("get_bottom_checked");

        /*if( sell_point_ids.length < 1 ){
            alert("Selecciona almenos un punto de venta");
            return false;
        }*/

        $("#sellpointIds").val( implode(',', sell_point_ids) );

    });

    if( $("#prizeList").length > 0 ){
        $("a[title=Eliminar]").click(function(){

            return confirm("Estas seguro que quieres eliminar este registro?");

        });
    }

});


controllers.sellPointController =
    function sellPointController($scope, $http){

        $scope.cityStoresSwitch = true;
        $scope.cityStores = null;
        $scope.cityStoresEmptyValue = "Selecciona una ciudad";
        $scope.saveDisabled = false;

        $scope.updateSellPoint = function(city_id){
            if( city_id != '' ){

                $scope.cityStoresSwitch = true;
                $scope.saveDisabled = true;

                 $http.get("cms/get_city_stores?city_id="+city_id)
                .success(function(response) {

                    $scope.cityStoresSwitch = false;
                    $scope.saveDisabled = false;


                    if( response.success  ){
                        $scope.cityStores = response.data;
                        $scope.cityStoresSwitch = false;
                        $scope.cityStoresEmptyValue = "Selecciona una cadena";
                    } else {
                        $scope.cityStoresSwitch = true;
                        $scope.cityStores = null;
                        $scope.cityStoresEmptyValue = "Selecciona una ciudad";
                    }

                });
            } else {

                $scope.cityStoresSwitch = true;
                $scope.cityStores = null;
                $scope.cityStoresEmptyValue = "Selecciona una ciudad";
            }
        }

    };
/*control para clonar varias veces el mismo elemento*/
controllers.prizesController =
    function prizesController($scope, $http){

        $scope.selected_values = null;

        $scope.store_qty = 1;
        $scope.store_blocks = [ { id: 1 } ];

        $scope.cityStoresSwitch = new Array;
        $scope.cityStores = new Array;
        $scope.cityStoresEmptyValue = new Array;

        $scope.addStoreBlock = function(){
            current_id = ++$scope.store_qty;
            $scope.store_blocks.push( {id:current_id  } );

            $scope.cityStoresSwitch[ current_id ] = true;
            $scope.cityStoresEmptyValue[ current_id ] = "Selecciona una ciudad";
        }


        $scope.cityStoresSwitch[1] = true;
        $scope.cityStoresEmptyValue[1] = "Selecciona una ciudad";

        $scope.updateSellPoint = function(city_id, store_block_id){
            if(  city_id != undefined  && city_id.trim() != '' ){


                $scope.cityStoresSwitch[store_block_id] = true;

                 $http.get("cms/get_city_stores?city_id="+city_id)
                .success(function(response) {

                    $scope.cityStoresSwitch[store_block_id] = false;

                    if( response.success  ){
                        $scope.cityStores[store_block_id] = response.data;
                        $scope.cityStoresSwitch[store_block_id] = false;
                        $scope.cityStoresEmptyValue[store_block_id] = "Selecciona una cadena";
                    } else {
                        $scope.cityStoresSwitch[store_block_id] = true;
                        $scope.cityStores[store_block_id] = null;
                        $scope.cityStoresEmptyValue[store_block_id] = "Selecciona una ciudad";
                    }

                });
            } else {

                $scope.cityStoresSwitch[store_block_id] = true;
                $scope.cityStores[store_block_id] = null;
                $scope.cityStoresEmptyValue[store_block_id] = "Selecciona una ciudad";
            }

        }

        // for testing purposes
        $scope.checkValues = function(){
            console.log($scope.store_qty);
            console.log($scope.store_blocks);
            console.log($scope.cityStoresSwitch);
            console.log($scope.cityStores);
            console.log($scope.cityStoresEmptyValue);
        }
};

// Register App controllers
cmsApp.controller( controllers );
