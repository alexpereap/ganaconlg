// JavaScript Document



$(document).ready(function () {

    $(".ui-loader").hide();

	//tooltip
   $("[rel=tooltip]").tooltip({html:true});

	//Date picker
	$('.input-group.date').datepicker({
		format: "yyyy-mm-dd",
		language: 'es',
		endDate: "-0d",
		startDate: "2014-10-27"
	});

	//Lightbox prizes
	$('.imgPrize').click(function(){
		$('.prizeImg').empty();
		$($(this).parents('div').html()).appendTo('.prizeImg');
		$('#modalPrize').modal({show:true});
	});



});