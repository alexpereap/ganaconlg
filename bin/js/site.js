var controllers = {};
var siteApp = angular.module('siteApp', [] );

// online case IDS
var onlineCityId = 205;
var onlineStoreId = 268;
var onlineSellpointID = 515;

$(document).ready(function(){

    $("#loginForm").bootstrapValidator();

    // You previously need to initialize it with show: false so it won't show until you manually do it.
    // $('#myModal').modal({ show: false})

    $("#contactForm").bootstrapValidator({

         onSuccess: function(e) {

            $.ajax({
                url : 'site/send_contact_form',
                type: 'post',
                data: ({
                    nombre : $("#cformNombre").val(),
                    email  : $("#cformEmail").val(),
                    msg    : $("#cformMsg").val()
                }),
                success:function(data){

                    $('#modalContact').modal('hide');
                    $('#modalSuccessCForm').modal('show');
                }
            });
        }
    });

    $("#contactForm").submit(function(e){

        e.preventDefault();
        return false;
    });



        /*validate only numbers*/
          $('#celPhone').keyup(function (){
            this.value = (this.value + '').replace(/[^0-9]/g, '');
          });

          $('#name').keyup(function (){
            this.value = (this.value + '').replace(/[^a-zA-Z ñáéíóú ÑÁÉÍÓÚ]/g, '');
          });

           $('#lastName').keyup(function (){
            this.value = (this.value + '').replace(/[^a-zA-Z ñáéíóú ÑÁÉÍÓÚ]/g, '');
          });


         // opens bills modal
         if( $("#openBillsModal").val() == 'yes' ){
            $("#triggermodalReceipt").click();
         }

        /*$(document).on("click", '.linkTerms', function(){
            ga_data = $(this).attr('data-marcacion').split(',');
            ga( ga_data[0], ga_data[1], ga_data[2], ga_data[3], ga_data[4] );
         });

        $(document).on("click", '.link-view-benefict', function(){
            ga_data = $(this).attr('data-marcacion').split(',');
            ga( ga_data[0], ga_data[1], ga_data[2], ga_data[3], ga_data[4] );
         });

        $(document).on("click", '.link-delete-benefict', function(){
            ga_data = $(this).attr('data-marcacion').split(',');
            ga( ga_data[0], ga_data[1], ga_data[2], ga_data[3], ga_data[4] );
         });*/
});


controllers.prizeRequestController =
    function prizeRequestController($scope, $http, $sce){

        $scope.prizes = [];
        $scope.index = [];
        $scope.currentSerial = null;
        $scope.tvRef = null;
        $scope.currentTvData = null;

        // combobox data
        $scope.stores = null;
        $scope.sellpoints = null;

        // disables stores combobox
        $scope.disabledStores = true;
        $scope.disabledSellPoints = true;

        $scope.modalAlertMessage = 'Escoge una referencia de la lista';

        $scope.modalErrorAlertMessage = new Object;
        $scope.disabledSubmitBtn = false;

        $scope.enable_promo_code = false;

        $scope.init = function(){
            $scope.prizes = angular_preload.prizes;
            $scope.stores = angular_preload.stores;
            $scope.sellpoints = angular_preload.sellpoints;
            $scope.disabledStores = angular_preload.disabledStores;
            $scope.disabledSellPoints = angular_preload.disabledSellPoints;

            // makes safe content
            for( x in $scope.prizes ){

                $scope.prizes[x].description = $sce.trustAsHtml( $scope.prizes[x].description );
                $scope.prizes[x].terms = $sce.trustAsHtml( $scope.prizes[x].terms );

                if( $scope.prizes[x].has_promo_code === true ){
                    $scope.enable_promo_code = true;
                }

            }

            $scope.selectedSellpointId = angular_preload.selectedSellpointId;
        }

        $scope.test = function(){

            console.log($scope.prizes);
            console.log($scope.stores);
            console.log($scope.sellpoints);
        }


        $scope.addTv = function(){

            // gets current tv data

            $("#serial").addClass('ui-autocomplete-loading');

             $http.get("site/get_tv_prize_by_ref?serial="+$scope.currentSerial)
                .success(function(response) {


                    $("#serial").removeClass('ui-autocomplete-loading');

                    if( response.success === true ){
                        $scope.currentTvData = response.data;

                        if($scope.currentTvData != null ){


                            // checks quanty availability
                            if( $scope.currentTvData.quanty == 0 ){
                                $scope.modalAlertMessage = '¡Lo sentimos! Las existencias de este beneficio estan agotadas';
                                $('#modalAlert').modal();

                                return;
                            }

                            // checks if this tv has been already added (we can't re use serials)
                            for( x in $scope.prizes ){
                                if( $scope.prizes[x].id == $scope.currentTvData.id ){
                                    $scope.modalAlertMessage = '¡No puedes agregar el mismo serial dos veces!';
                                    $('#modalAlert').modal();

                                    return;
                                }
                            }

                            // checks date avialability
                            /*if( $scope.currentTvData.date_availability === false ){
                                // $scope.modalAlertMessage = '¡Lo sentimos! la fecha para reclamar este beneficio ha caducado. (El beneficio era váido desde: ' + $scope.currentTvData.available_from + ' hasta: ' +$scope.currentTvData.available_until + ').';
                                $scope.modalAlertMessage = '¡Lo sentimos! La referencia de TV que ingresaste no cuenta actualmente con un beneficio.';
                                $('#modalAlert').modal();

                                return;
                            }*/


                            switch( $scope.currentTvData.date_availability ){

                                // no beneficts available
                                case false:
                                $scope.modalAlertMessage = '¡Lo sentimos! La referencia de TV que ingresaste no cuenta actualmente con un beneficio.';
                                $('#modalAlert').modal();
                                return;
                                break;

                                // some beneficts available
                                case 'partial':
                                $scope.modalAlertMessage = $scope.currentTvData.unavailable_message;
                                $('#modalAlert').modal();
                                break;
                            }

                            // more than 3 products validation fixed
                            /*if( $scope.prizes.length >= 3 ){
                                $scope.modalAlertMessage = '¡Lo sentimos! No puedes agregar más de 3 productos por factura.';
                                $('#modalAlert').modal();

                                return;
                            }*/

                            if( $scope.currentSerial == null ){
                                $scope.modalAlertMessage = 'Especifica el serial';
                                $('#modalAlert').modal();

                                return;
                            }



                            // checks if whether to hide or unhide the promo code field
                            if( $scope.currentTvData.has_promo_code === true ){
                                $scope.enable_promo_code = true;
                            }

                            // converts this data to safe html
                            $scope.currentTvData.description = $sce.trustAsHtml( $scope.currentTvData.description );
                            $scope.currentTvData.terms = $sce.trustAsHtml( $scope.currentTvData.terms );

                            $scope.currentTvData.serial = $scope.currentSerial;

                            $scope.currentTvData.marcacion_terms = "send,event, Formulario,Click, Términos-y-Condiciones_Promo" + ($scope.prizes.length +1) + "_Datos-de-tu-TV'";
                            $scope.currentTvData.marcacion_ver = "send,event, Formulario,Click, Ver-promo_Promo" + ($scope.prizes.length +1) + "_Datos-de-tu-TV'";
                            $scope.currentTvData.marcacion_trash = "send,event, Formulario,Click, Eliminar_Promo" + ($scope.prizes.length +1) + "_Datos-de-tu-TV'";


                            $scope.prizes.push($scope.currentTvData);
                            $scope.currentTvData = null;
                            $scope.currentSerial = null;
                            $scope.tvRef = null;


                        }

                    } else {
                        $scope.modalErrorAlertTitle   = 'Serial no disponible:';
                        $scope.modalErrorAlertMessage.message = '<p>';
                        $scope.modalErrorAlertMessage.message += 'Lo sentimos, este serial no está disponible';
                        $scope.modalErrorAlertMessage.message += '</p>';

                        $scope.modalErrorAlertMessage.message = $sce.trustAsHtml($scope.modalErrorAlertMessage.message);
                        $('#modalErrorAlert').modal();
                        return;
                    }
                });


        }

        $scope.removeTv = function (tv_id){

            enable_promo_code = false;

            for( x in $scope.prizes ){

                if( $scope.prizes[x].tv_id == tv_id ){
                    index = parseInt(x);
                    break;
                }
            }

            // removes tv
            if (index > -1) {
                $scope.prizes.splice(index, 1);
            }

            // checks if whether to hide or unhide the promo code field
            // if some prize has promo code option the prom code field wont be hide
            for( x in $scope.prizes ){
                if( $scope.prizes[x].has_promo_code === true ){
                    enable_promo_code = true;
                    break;
                }
            }

            $scope.enable_promo_code = enable_promo_code;
        }

        $scope.setStores = function(cityId){

            $scope.disabledStores = true;
            $scope.selectedSellpointId = null;

            // resets store and sellpoint data
            $scope.sellpoints = null;
            $scope.disabledSellPoints = true;

            $http.get("site/ajax_get_city_stores?city_id="+cityId)
                .success(function(response) {


                    if( response.length > 0 ){
                        $scope.stores = response;

                        // special case online store
                        if( cityId == onlineCityId ){
                            setTimeout(function(){
                                $("#storeSelect option").last().attr('selected','selected');
                                $scope.setSellpoints( onlineStoreId );
                            }, 100);
                        } else {
                            $("#code").removeAttr('disabled');
                        }

                        $scope.disabledStores = false;
                    } else {

                        $scope.stores = null;
                        $scope.selectedSellpointId = null;

                    }

                });
        }

        $scope.setSellpoints = function( storeId ){

            $scope.disabledSellPoints = true;
            $scope.selectedSellpointId = null;

            $http.get("site/ajax_get_store_sell_points?store_id="+storeId)
            .success(function(response) {


                if( response.length > 0 ){
                    $scope.sellpoints = response;

                    if( storeId == onlineStoreId ){
                        setTimeout(function(){
                            $("#sellpointSelect option").last().attr('selected','selected');
                            $scope.selectedSellpointId = onlineSellpointID;

                            $("#code").attr('disabled','disabled');

                            $("input[name=sell_point_id]").val( onlineSellpointID );

                        }, 110);
                    } else {
                        $("#code").removeAttr('disabled');
                    }

                    $scope.disabledSellPoints = false;
                } else {
                    $scope.sellpoints = null;
                    $scope.selectedSellpointId = null;
                }

            });
        }


        $scope.sendForm = function(){
            $("#registerForm").submit();
        }

        $scope.gaclick = function(a,b,c,d,e){

            console.log(a);
            //ga(a,b,c,d,e);
        }

        $("#registerForm").submit(function(){

            var error_msg = '';
            var allowedExtensions = ['jpg','jpeg','png','gif'];

            if( !$("#checkHabeas").is(':checked') ){
                error_msg += '<li>Por favor acepta el tratamiento de datos.</li>';
            }

            if( $("#name").val() == '' ){
                error_msg += '<li>El campo nombre es obligatorio.</li>';
            }

            if( $("#lastName").val() == '' ){
                error_msg += '<li>El campo apellido es obligatorio.</li>';
            }

            if( $("#celPhone").val() == '' ){
                error_msg += '<li>El campo celular es obligatorio.</li>';
            }


            if( $("#registerForm").attr('edit-mode') == 'yes' ){

                if( $("#nid").val() == '' ){
                    error_msg += '<li>No has especificado el número de cédula.</li>';
                }

            }

            if( $scope.prizes.length == 0 ){
                error_msg += '<li>No has añadido ningún televisor.</li>';
            }

            if( $scope.selectedSellpointId == null || $scope.selectedSellpointId == ''  ){
                error_msg += '<li>No has seleccionado algún punto de venta.</li>';
            }

            if( $("#purchase_date").val() == '' ){
                error_msg += '<li>Especifica la fecha de compra.</li>';
            }

            if( $("#registerForm").attr('edit-mode') != 'yes' )
            if( $("#bill_img").val() == '' ){
                error_msg += '<li>Hace falta la foto de la factura.</li>';
            } else {
                ext = $("#bill_img").val().split('.');
                extension = ext[ ext.length -1 ].toLowerCase();

                if( !in_array( extension, allowedExtensions ) ){
                    error_msg += '<li>El archivo de la foto de factura no es válido, solo admitimos jpg, png, gif.</li>';
                }
            }



            // error_msg = '';

            if( error_msg != '' ){
                $scope.modalErrorAlertTitle   = 'Se encontraron los siguientes errores en tu solicitud:';
                $scope.modalErrorAlertMessage.message = '<ul>';
                $scope.modalErrorAlertMessage.message += error_msg;
                $scope.modalErrorAlertMessage.message += '</ul>';

                $scope.modalErrorAlertMessage.message = $sce.trustAsHtml($scope.modalErrorAlertMessage.message);
                $('#modalErrorAlert').modal();

                return false;
            } else {

                var validForm = true;
                $scope.disabledSubmitBtn = true;

                // validate terms y cons
                $(".checkTerms").each(function(){


                    if( !$(this).prop('checked') ){
                        $scope.modalAlertMessage = 'Debes aceptar los términos y condiciones de cada beneficio.';
                        $('#modalAlert').modal();

                        // kills form sending
                        validForm =  false;
                        $scope.disabledSubmitBtn = false;
                        return false;
                    }

                });

                // kill assoc with terms
                if( validForm === false ){
                    return false;
                }

                return validForm;
            }

        });

        $("#inputTvRef").autocomplete({
            source: 'site/get_available_prizes_tvs_byref',
            minLength : 2,
            select : function(event, ui){

                $scope.currentTvData = ui.item;

            },
            change: function(event, ui) {
                if( ui.item == null ){

                    $scope.modalAlertMessage = 'Escoge una referencia de la lista.';
                    $('#modalAlert').modal();


                    $scope.currentTvId = null;

                    $(this).val('');
                    $scope.currentSerial = null;
                    $(this).focus();
                }
            }
        });

    };

// Register App controllers
siteApp.controller( controllers );