<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cms extends CI_Controller {

    private  $user_nid_imgs_folder = "uploads/nid_imgs";

    function __construct(){
        parent::__construct();

        if( !$this->session->userdata('logged_in') &&  $this->router->method != 'login' && $this->router->method != 'check_login_credentials' ){

            redirect( 'cms/login' );
        }

        $this->load->library('grocery_CRUD');
    }

    public function index(){

        // test commit
        $this->load->view("cms/opener");
        $this->load->view("cms/closure");
    }

    // codes management
    public function codes(){

        die("module not available");

        $this->grocery_crud->set_table('tbl_codes');

        $action = $this->grocery_crud->getState();

        if( $action != 'edit' && $action != 'list' ){
            $this->grocery_crud->set_field_upload('value', 'assets/uploads/files/');
            $this->grocery_crud->callback_before_upload(array($this,'excel_validation'));
        }


        switch( $action ){
            case 'add':
                $this->grocery_crud->display_as('value', 'Archivo (excel xls, xlsx)');
                $this->grocery_crud->unset_fields('available');
                break;

            case 'edit':
                $this->grocery_crud->display_as('value', 'Código');
                break;

            default:
                $this->grocery_crud->display_as('value', 'Código');
                break;
        }

        $this->grocery_crud->display_as('available', 'Disponible');
        $this->grocery_crud->required_fields('value');

        $this->grocery_crud->callback_insert(array($this,'insert_custom_lg_codes'));

        $output = $this->grocery_crud->render();

        $output->in_codes = true;
        $output->in_codes_list = true;

        $this->load->view("cms/opener", $output);
        $this->load->view("cms/codes/codes");
        $this->load->view("cms/closure");
    }

    public function promo_code_txt(){
        $this->grocery_crud->set_table('tbl_txt_promocode');

        $this->grocery_crud->display_as('value','Texto');
        $this->grocery_crud->required_fields('value');

        $this->grocery_crud->unset_export();
        $this->grocery_crud->unset_print();
        $this->grocery_crud->unset_add();
        $this->grocery_crud->unset_delete();

        $output = $this->grocery_crud->render();

        $output->in_codes = true;
        $output->in_codes_text = true;

        $this->load->view("cms/opener", $output);
        $this->load->view("cms/codes/code_txt");
        $this->load->view("cms/closure");
    }

    public function email_mod(){
        $this->grocery_crud->set_table('tbl_mod_email');

        $this->grocery_crud->display_as('value','Email');
        $this->grocery_crud->required_fields('value');

        $this->grocery_crud->set_rules('value','Email','valid_email');

        $this->grocery_crud->unset_export();
        $this->grocery_crud->unset_print();
        $this->grocery_crud->unset_add();
        $this->grocery_crud->unset_delete();

        $output = $this->grocery_crud->render();

        $output->in_prize = true;
        $output->in_prize_email = true;

        $this->load->view("cms/opener", $output);
        $this->load->view("cms/mod/mod_detail");
        $this->load->view("cms/closure");
    }

    // participants management
    public function participants(){

        $this->grocery_crud->set_table('tbl_user');

        $this->grocery_crud->display_as('email','Email')
        ->display_as('nid', 'Número de Cédula')
        ->display_as('name','Nombre/s')
        ->display_as('lastname','Apellido/s')
        ->display_as('cellphone','Celular')
        ->display_as('nid_img_front','Imágen de cédula frente')
        ->display_as('nid_img_back','Imágen de cédula atras')
        ->display_as('created_at','Fecha de creación')
        ->display_as('last_login','Última conexión')
        ->display_as('verified','Usuario verificado');


        $this->grocery_crud->columns('email', 'nid', 'name', 'lastname', 'cellphone', 'created_at', 'last_login', 'verified' );

        $this->grocery_crud->set_field_upload('nid_img_front',$this->user_nid_imgs_folder);
        $this->grocery_crud->set_field_upload('nid_img_back',$this->user_nid_imgs_folder);

        $this->grocery_crud->unset_fields('created_at','last_login');

        $output = $this->grocery_crud->render();

        $output->in_users = true;
        $output->in_users_participants = true;


        $this->load->view("cms/opener", $output);
        $this->load->view("cms/users/participants_users");
        $this->load->view("cms/closure");
    }

    // promos management
    public function promos(){

        $this->grocery_crud->set_table('tbl_promo');
        $this->grocery_crud->unset_export();
        $this->grocery_crud->unset_print();

        $this->grocery_crud->display_as('title','Titulo')
        ->display_as('media', 'Archivo (jpg, png, gif) 1140 x 700px')
        ->display_as('available_from','Disponible desde')
        ->display_as('available_until','Disponible hasta')
        ->display_as('order','Orden');

        $this->grocery_crud->field_type('order','dropdown', $this->makeDropdownNumbers(0,50) );

        $this->grocery_crud->callback_add_field('link',function () {
            return '<input type="text" maxlength="50" placeholder="ejemplo: http://google.com.co"  value="" name="link">';
        });

        $this->grocery_crud->field_type('link', 'string');

        $this->grocery_crud->unset_fields('created_at');

        $this->grocery_crud->set_field_upload('media','assets/uploads/files');

        $this->grocery_crud->required_fields('title','media', 'available_from', 'available_until', 'order' );

        $this->grocery_crud->callback_before_upload(array($this,'promos_validation'));
        $output = $this->grocery_crud->render();

        $output->in_promos = true;

        $this->load->view("cms/opener", $output);
        $this->load->view("cms/promos/list");
        $this->load->view("cms/closure");
    }

    // helper function for numeric dropdown grocery crud
    public function promos_validation( $files_to_upload, $field_info ){


        $allowed_extensions = array('jpg', 'jpeg', 'png', 'swf');

        foreach( $files_to_upload as $file ){

            $file_ext = end(explode('.', $file['name']) );

            if( !in_array( $file_ext , $allowed_extensions) ){

                return "Solo son permitidos archivos de tipo jpg, swf, png";
            }
        }

        return true;

    }

    public function cms_users(){

        $this->grocery_crud->set_table('tbl_cms_user');

        $this->grocery_crud->unset_export();
        $this->grocery_crud->unset_print();

        $this->grocery_crud->display_as('username','Usuario')
        ->display_as('password','Contraseña');

        $this->grocery_crud->field_type('password', 'password');

        $this->grocery_crud->callback_before_insert(array($this,'encrypt_password_callback'));
        $this->grocery_crud->callback_before_update(array($this,'encrypt_password_callback'));

        $this->grocery_crud->required_fields('username','password');
        $this->grocery_crud->unset_fields('created_at');

        $output = $this->grocery_crud->render();

        $output->in_users = true;
        $output->in_users_cms = true;
        $output->custom_js[] = 'js/cms_users.js';

        $this->load->view('cms/opener',$output);
        $this->load->view('cms/users/cms_users');
        $this->load->view('cms/closure');

    }

    // cadenas
    public function stores(){

        $this->grocery_crud->set_table('tbl_store');

        $this->grocery_crud->unset_export();
        $this->grocery_crud->unset_print();

        $this->grocery_crud->display_as('fk_city_id','País')
        ->display_as('name', 'Nombre');

        $this->grocery_crud->required_fields('fk_city_id','name');

        $this->grocery_crud->set_relation('fk_city_id','tbl_city','name');

        $output = $this->grocery_crud->render();

        $output->in_stores = true;
        $output->in_stores_stores = true;

        $this->load->view('cms/opener',$output);
        $this->load->view('cms/stores/store');
        $this->load->view('cms/closure');
    }

    public function sellpoints(){

        $this->grocery_crud->set_table('vw_sells_points');
        $this->grocery_crud->set_primary_key('sellpoint_id');

        $this->grocery_crud->columns('city','store','sellpoint_name');

        $this->grocery_crud->display_as('city','Ciudad')
        ->display_as('store','Cadena')
        ->display_as('sellpoint_name','Punto de venta');

        $this->grocery_crud->add_action('Eliminar', 'assets/grocery_crud/themes/flexigrid/css/images/close.png', 'cms/delete_sellpoint');
        $this->grocery_crud->add_action('Editar', 'assets/grocery_crud/themes/flexigrid/css/images/edit.png', 'cms/edit_sellpoint');

        $this->grocery_crud->unset_delete();
        $this->grocery_crud->unset_add();
        $this->grocery_crud->unset_read();
        $this->grocery_crud->unset_edit();
        $this->grocery_crud->unset_export();
        $this->grocery_crud->unset_print();

        $output = $this->grocery_crud->render();

        $output->in_stores = true;
        $output->in_stores_sellpoints = true;

        $output->custom_js[] = 'js/cms_sellpoints.js';


        $this->load->view('cms/opener',$output);
        $this->load->view('cms/stores/sell_points');
        $this->load->view('cms/closure');
    }

    public function edit_sellpoint( $sellpoint_id ){

        $output = array(
            'in_stores' => true,
            'in_stores_sellpoints' => true,
            'cities' => $this->main->getStoreAvailableCities(),
            'sellpoint' => $this->main->getSellPointById( $sellpoint_id )
            );

        $output['stores'] = $this->main->getCityStores( $output['sellpoint']->city_id );

        $this->load->view('cms/opener',$output);
        $this->load->view('cms/stores/edit_sellpoint');
        $this->load->view('cms/closure');
    }

    public function add_sellpoint(){

        $output = array(
            'in_stores' => true,
            'in_stores_sellpoints' => true,
            'cities' => $this->main->getStoreAvailableCities()
            );

        $this->load->view('cms/opener',$output);
        $this->load->view('cms/stores/add_sellpoint');
        $this->load->view('cms/closure');
    }

    public function update_sellpoint(){

        $data = array(
            'name' => $this->input->post('sellpoint'),
            'fk_store_id' => $this->input->post('storeId')
            );

        $this->main->updateSellpoint( $this->input->post('sellpoint_id'), $data );

        redirect( base_url() . 'cms/sellpoints' );
    }

    public function delete_sellpoint(){
        $r = $this->main->deleteSellpoint( $this->input->post('sellpoint_id') );
        echo json_encode( array('result' => $r));
    }

    public function save_sellpoint(){
        $data = array(
            'name' => trim($this->input->post('sellpoint')),
            'fk_store_id' => $this->input->post('storeId')
            );

        $this->main->saveSellpoint( $data );

        redirect( base_url() . 'cms/sellpoints' );
    }

    function encrypt_password_callback( $post_array ){

        $post_array['password'] = $this->encrypt->encode($post_array['password']);
        return $post_array;
    }

    private function makeDropdownNumbers( $start, $end ){

        $result = array();

        for( $start ; $start <= $end; $start++ ){
            $result[$start] = $start;
        }

        return $result;

    }

    public function get_city_stores(){
        $r = $this->main->getCityStores( $this->input->get('city_id'));

        if( count($r > 0 ) ){
            echo json_encode( array('success' => true,'data' => $r ));
        } else {
            echo json_encode( array('success' => false));
        }

    }

    public function login(){

        if( $this->session->userdata('logged_in') ){
            redirect('cms');
        }

        $this->load->view('cms/login');
    }

    public function check_login_credentials(){

        $result = $this->main->logInCmsUser( $_POST );
        echo json_encode( array('result' => $result) );
    }

    public function logout(){
        $this->session->sess_destroy();
        redirect('cms');
    }
    /*tvs*/
    public function tv(){

        $this->grocery_crud->set_table('tbl_tv');

        $this->grocery_crud->unset_export();
        $this->grocery_crud->unset_print();

        $this->grocery_crud->display_as('id','Identificacion unica')
        ->display_as('reference','Serial del tv')
        ->display_as('description', 'Descripcion del televisor')
        ->display_as('created_at','Fecha de creacion');

        $this->grocery_crud->columns('reference','created_at');

        $this->grocery_crud->unset_fields('created_at','description');
        $this->grocery_crud->required_fields('reference');
        $this->grocery_crud->callback_before_upload(array($this,'images_validation'));
        $output = $this->grocery_crud->render();

        $output->in_tv = true;
        //$output->in_stores_stores = true;

        $this->load->view('cms/opener',$output);
        $this->load->view('cms/tvs/tv');
        $this->load->view('cms/closure');
    }

    public function images_validation( $files_to_upload, $field_info )
    {


        $allowed_extensions = array('jpg', 'jpeg', 'png');

        foreach( $files_to_upload as $file )
        {

            $file_ext = end(explode('.', $file['name']) );

            if( !in_array( $file_ext , $allowed_extensions) )
            {

                return "Solo son permitidos archivos de tipo jpg, png";
            }
        }

        return true;

    }

    public function excel_validation($files_to_upload, $field_info){
        $allowed_extensions = array('xls', 'xlsx' );

        foreach( $files_to_upload as $file )
        {

            $file_ext = end(explode('.', $file['name']) );

            if( !in_array( $file_ext , $allowed_extensions) )
            {

                return "Solo son permitidos archivos de tipo excel";
            }
        }

        return true;
    }


    /*premios*/

    public function delete_prize_package($id){

        $this->main->deletePrizePackage($id);
        redirect('cms/prize');
    }

    public function prize()
    {

        $this->grocery_crud->set_table('vw_prize_beneficts');

        $this->grocery_crud->set_primary_key('id');

        $this->grocery_crud->unset_add();
        $this->grocery_crud->unset_read();
        $this->grocery_crud->unset_delete();
        $this->grocery_crud->unset_edit();
        $this->grocery_crud->unset_export();
        $this->grocery_crud->unset_print();

        // add custom actions
        $this->grocery_crud->add_action('Eliminar', 'assets/grocery_crud/themes/flexigrid/css/images/close.png', 'cms/delete_prize_package');
        $this->grocery_crud->add_action('Editar', 'assets/grocery_crud/themes/flexigrid/css/images/edit.png', 'cms/edit_prize_package');

        $this->grocery_crud->display_as('reference','Serial del Tv')
        ->display_as('title', 'titulo del beneficio')
        ->display_as('image', 'imagen del beneficio')
        ->display_as('description','Descripcion del beneficio')
        ->display_as('created_at','fecha de creacion')
        ->display_as('quanty','Cantidad')
        ->display_as('terms','Terminos y condiciones')
        ->display_as('has_promo_code', 'Código del beneficio')
        ->display_as('beneficts', 'Beneficios');

        $this->grocery_crud->field_type('has_promo_code','dropdown',array(
            'no'    => 'No',
            'yes'   => 'Si'
            ));

        $this->grocery_crud->columns('reference','title','image','description','created_at', 'quanty',  'beneficts');

        $this->grocery_crud->set_field_upload('image','assets/uploads/files');

        $this->grocery_crud->unset_fields('created_at');

        $this->grocery_crud->set_relation('fk_tv_id','tbl_tv','reference');

        $this->grocery_crud->callback_add_field('fk_tv_id',function () {
            return $this->reference_validation();
        });

        $this->grocery_crud->callback_edit_field('fk_tv_id',function () {
            return $this->reference_validation_edit();
        });

        // custom callbacks
        $this->grocery_crud->callback_insert(array($this,'insert_custom_tbl_prize_tbl_store'));
        $this->grocery_crud->callback_update(array($this, 'update_custom_tbl_prize_tbl_store'));

        $output = $this->grocery_crud->render();

        $action = $this->grocery_crud->getState();

        $angular_data = null;


        /*if( $action == 'add' || $action == 'edit' ){
            $output->output = str_replace( '<div class="pDiv">' , '<div class="pDiv">' . $this->replace_content($angular_data) , $output->output);
        }*/



        $output->in_prize = true;
        $output->in_prize_prizes = true;


        $this->load->view('cms/opener',$output);
        $this->load->view('cms/prizes/prize');
        $this->load->view('cms/closure');
    }

    public function add_prize_package(){

        $data = array(
            'tvs'                     => $this->main->get_available_tv_prizes(),
            // 'cities'                  => $this->main->getStoreAvailableCities(),
            'prize_items'             => $this->main->getPrizeItems(),
            'in_prize'                => true,
            'in_prize_prizes'         => true,
            // 'cities_store_sellpoints' => $this->main->getCitySellpointsStoreAssoc()
        );

        $this->load->view('cms/opener',$data);
        $this->load->view('cms/prizes/prize_package');
        $this->load->view('cms/closure');
    }

    public function edit_prize_package( $prize_id ){

        $prize_items_ids = array();
        $prize_sellpoint_ids = array();

        foreach( $this->main->getPrizeElements($prize_id) as $pe ){
            $prize_items_ids[] = $pe->fk_prize_detail_id;
        }


        /* foreach( $this->main->getPrizePackageSellPoints( $prize_id ) as $sp ){
            $prize_sellpoint_ids[] = $sp->fk_sells_point_id;
        }*/


        $data = array(
            'prize_package'           => $this->main->getPrizePackageById($prize_id),
            'tvs'                     => $this->main->get_available_tv_prizes_update($prize_id),
            // 'cities'                  => $this->main->getStoreAvailableCities(),
            'prize_items'             => $this->main->getPrizeItems(),
            'in_prize'                => true,
            'in_prize_prizes'         => true,
            // 'cities_store_sellpoints' => $this->main->getCitySellpointsStoreAssoc(),
            'prize_items_ids'         => $prize_items_ids,
            // 'prize_sellpoint_ids'     => $prize_sellpoint_ids
        );

        $this->load->view('cms/opener',$data);
        $this->load->view('cms/prizes/edit_prize_package');
        $this->load->view('cms/closure');
    }

    public function save_prize_package(){

        $prize_package = array(
            'fk_tv_id'        => $this->input->post('fk_tv_id'),
            'title'           => $this->input->post('title'),
            'description'     => $this->input->post('description'),
            'quanty'          => $this->input->post('quanty'),
            'terms'           => $this->input->post('terms'),
            'has_promo_code'  => $this->input->post('has_promo_code')
        );

        if(  $_FILES['image']['name'] != '' ){


            $rf = $this->model_files->upload_file('image', './assets/uploads/files', 'jpg|jpeg|png' );

            if( $rf['success'] === true ){

                $prize_package['image'] = $rf['data']['file_name'];
            } else {
                redirect( base_url() . 'site/set_purchase_data?error=' . $rf['error'] );
            }

        }

        $prize_id = $this->main->savePrizePackageData( $prize_package );

        $this->prizeItemsAssoc($prize_id);
        // $this->prizeSellpointsAssoc($prize_id);

        redirect('cms/prize');

    }

    public function update_prize_package(){

        $prize_package = array(
            'fk_tv_id'        => $this->input->post('fk_tv_id'),
            'title'           => $this->input->post('title'),
            'description'     => $this->input->post('description'),
            'quanty'          => $this->input->post('quanty'),
            'terms'           => $this->input->post('terms'),
            'has_promo_code'  => $this->input->post('has_promo_code')
        );

        if(  $_FILES['image']['name'] != '' ){


            $rf = $this->model_files->upload_file('image', './assets/uploads/files', 'jpg|jpeg|png' );

            if( $rf['success'] === true ){

                $prize_package['image'] = $rf['data']['file_name'];
            } else {
                redirect( base_url() . 'site/set_purchase_data?error=' . $rf['error'] );
            }

        }

        $prize_id = $this->input->post('prize_id');

        $this->main->updatePrizePackage($prize_id, $prize_package);
        $this->prizeItemsAssoc($prize_id, true);
        // $this->prizeSellpointsAssoc($prize_id, true);

        redirect('cms/edit_prize_package/' . $prize_id);

    }

    private function prizeItemsAssoc( $prize_id, $clean_up = false ){

        if( $clean_up === true ){
            $this->main->deletePrizeElements( $prize_id );
        }

        // prize items assoc
        foreach( $this->input->post('prize_items') as $prize_detail_id ){
            $prize_elements[] = array(
                'fk_prize_id'        => $prize_id,
                'fk_prize_detail_id' => $prize_detail_id
            );
        }

        $this->main->savePrizeElements($prize_elements);
    }

    private function prizeSellpointsAssoc( $prize_id, $clean_up = false ){

        if( $clean_up === true ){

            $this->main->deletePrizeStores( $prize_id );

        }

        $sell_points_ids = explode(',', $this->input->post('sellpointIds') );

        foreach( $sell_points_ids as $sp_id ){
            $prize_stores_data[] = array(
                'fk_prize_id'       => $prize_id,
                'fk_sells_point_id' => $sp_id
            );
        }

        $this->main->insertPrizeStores( $prize_stores_data );
    }

    public function prize_detail(){

        $this->grocery_crud->set_table('tbl_prize_detail');

        $this->grocery_crud->set_field_upload('image','assets/uploads/files');
        $this->grocery_crud->unset_fields('created_at','last_login', 'has_list_of_codes','description');

        $this->grocery_crud->display_as('title', 'Titulo del beneficio')
        ->display_as('image', 'Imágen (está será la imágen del beneficio, se verá en el email)')
        ->display_as('description', 'Descripción (esto sólo se verá en CMS)')
        ->display_as('has_list_of_codes', '¿Cuenta con lista de códigos?')
        ->display_as('created_at', 'Fecha de creación')
        ->display_as('reclaim_description', 'Pasos a seguir para relclamar el beneficio (se verá en el email)')
        ->display_as('available_from', 'Disponible desde')
        ->display_as('available_until', 'Disponible hasta');

        $this->grocery_crud->columns('title','image',  'available_from', 'available_until' ,'created_at');

        /*$this->grocery_crud->field_type('has_list_of_codes','dropdown',array(
            'yes' => 'Si',
            'no'  => 'No'
        ));*/

        $this->grocery_crud->required_fields('title','image');
        $this->grocery_crud->callback_before_upload(array($this,'images_validation'));

        $output = $this->grocery_crud->render();

        $output->in_prize = true;
        $output->in_prize_detail = true;

        $this->load->view('cms/opener',$output);
        $this->load->view('cms/prizes/prize_detail');
        $this->load->view('cms/closure');

    }



    public function prize_codes(){

        $action = $this->grocery_crud->getState();


        $this->grocery_crud->set_table('tbl_prize_codes');
        if( $action != 'edit' && $action != 'list' ){
            $this->grocery_crud->set_field_upload('value', 'assets/uploads/files/');
            $this->grocery_crud->callback_before_upload(array($this,'excel_validation'));
        }



        switch( $action ){
            case 'add':

                $this->grocery_crud->unset_fields('created_at','available');
                $this->grocery_crud->display_as('value', 'Archivo (excel xls, xlsx)');
                break;

            case 'edit':
                $this->grocery_crud->unset_fields('created_at');
                $this->grocery_crud->display_as('value', 'Código');
                break;

            default:
                $this->grocery_crud->display_as('value', 'Código');
                break;
        }

        $this->grocery_crud->set_relation('fk_prize_detail_id','tbl_prize_detail','title' ,array('has_list_of_codes' => 'yes'));

        $this->grocery_crud->display_as('fk_prize_detail_id', 'Beneficio')
        ->display_as('available', 'Disponible')
        ->display_as('created_at', 'Fecha de creación');

        $this->grocery_crud->field_type('available','dropdown',array(
            'yes' => 'Si',
            'no'  => 'No'
        ));


        $this->grocery_crud->callback_insert(array($this,'insert_custom_prize_codes'));

        $this->grocery_crud->required_fields('fk_prize_detail_id','value' );

        $output = $this->grocery_crud->render();

        $output->in_prize = true;
        $output->in_prize_codes = true;

        $this->load->view('cms/opener',$output);
        $this->load->view('cms/prizes/prize_codes');
        $this->load->view('cms/closure');
    }

    public function insert_custom_prize_codes($post_array){
        $prize_detail_id = $post_array['fk_prize_detail_id'];

        $this->load->model('excel_ops');
        $this->excel_ops->setPrizeCodes( $post_array['fk_prize_detail_id'], $post_array['value'] );

        return true;
    }

    public function insert_custom_lg_codes($post_array){

        $this->load->model('excel_ops');
        $this->excel_ops->setLgCodes( $post_array['value'] );

        return true;
    }


    public function insert_custom_tbl_prize_tbl_store($post_array)
    {

        $store_id=$post_array['storeId'];
        unset($post_array['storeId']);

        $this->db->insert('tbl_prize',$post_array);
        $prize_id = $this->db->insert_id();

        foreach ($store_id as $key => $value) {
          $data[] = array(
             'fk_prize_id' => $prize_id,
             'fk_store_id' => $store_id[$key]
             );
        }

       $this->main->insertPrizeStores($data);
    }

    public function update_custom_tbl_prize_tbl_store($post_array, $primary_key){

        $store_id = $post_array['storeId'];
        unset($post_array['storeId']);
        $prize_id = $primary_key;

        foreach ($store_id as $key => $value) {
          $data[] = array(
             'fk_prize_id' => $prize_id,
             'fk_store_id' => $value
             );
        }


        $this->main->deletePrizeStores($prize_id);
        $this->main->insertPrizeStores($data);

        return $this->db->update('tbl_prize',$post_array,array('id' => $primary_key));
    }

    public function reference_validation()
    {

        $data['tvs']= $this->main->get_available_tv_prizes();
        $print= "<select name='fk_tv_id' id='field-fk_tv_id' data-placeholder='Seleccionar Referencia'>";
        $print.="<option value=''>Selecciona una Referencia</option>";
        foreach ($data['tvs'] as $row)
        {
            $print.= "<option value='".$row->id."'>".$row->reference."</option>";
        }
        $print.= "</select>";
        return $print;
    }

    public function replace_content( $angular_data = null )
    {
        $output = array(
            'cities'       => $this->main->getStoreAvailableCities(),
            'angular_data' => $angular_data
            );

       /* $this->load->view('cms/opener',$output);
        $this->load->view('cms/stores/add_sellpoint');*/
        $cadena = $this->load->view('cms/prizes/add_store_prize', $output, true);

        return $cadena;
    }

    public function prize_requests(){

        $this->grocery_crud->set_table('vw_prize_requests');
        $this->grocery_crud->set_primary_key('prize_application_id');

        $this->grocery_crud->columns('name','lastname','nid', 'prize_request_qty', 'store_name', 'city_name', 'tv_references', 'promotional_code_txt', 'request_date', 'status');

        $this->grocery_crud->display_as('name','Nombre/s')
            ->display_as('lastname', 'Apellido/s')
            ->display_as('nid', 'No de Cédula')
            ->display_as('prize_request_qty', 'Cantidad de tvs comprados')
            ->display_as('request_date', 'Fecha de solicitud')
            ->display_as('status', 'Estado')
            ->display_as('store_name', 'Cadena')
            ->display_as('city_name', 'Ciudad')
            ->display_as('tv_references', 'Referencia/s de tv')
            ->display_as('code_lg', 'Código de beneficio LG')
            ->display_as('promotional_code_txt', 'Código promocional');

        $this->grocery_crud->field_type('status','dropdown',array(
            'email_confirmacion' => 'Email de confirmación',
            'en_revision'        => 'En Revisión',
            'no_aprobado'        => 'No aprobado',
            'aprobado'           => 'Aprobado',
            'entregado'          => 'Entregado'
            ));

        $this->grocery_crud->unset_delete();
        $this->grocery_crud->unset_add();
        $this->grocery_crud->unset_read();
        $this->grocery_crud->unset_edit();

        $this->grocery_crud->order_by('request_date','DESC');

        $this->grocery_crud->add_action('Ver Detalle', 'assets/grocery_crud/themes/flexigrid/css/images/magnifier.png', 'cms/prize_request_detail');

        $output = $this->grocery_crud->render();

        $output->in_prize = true;
        $output->in_prize_requests = true;

        $this->load->view('cms/opener',$output);
        $this->load->view('cms/prizes/prize_requests');
        $this->load->view('cms/closure');
    }

    public function prize_request_detail($prize_application_id){

        $purchaseData = $this->main->getPurchaseDataByPrizeAppId( $prize_application_id );

        $data = array(
            'prize_detail'         => $this->main->getPrizeApplicationDetail( $prize_application_id ),
            'in_prize'             => true,
            'in_prize_requests'    => true,
            'promotional_code_txt' => $purchaseData->promotional_code_txt,
            'custom_js'            => array('js/prize_req_detail.js')
        );

        $this->load->view('cms/opener',$data);
        $this->load->view('cms/prizes/prize_request_detail');
        $this->load->view('cms/closure');
    }

    public function set_prize_request_status(){


        if( $this->input->post('sendApprovalMail') == 1 ){
            // send approval email

            $prize_application_id = $this->input->post('prize_application_id');
            $appRequestDetail = $this->main->getPrizeApplicationDetail($prize_application_id);

            $prizes_view = array();

            foreach( $appRequestDetail as $ar ){

                $prize_detail = $this->main->getPrizeDetailsByTvId( $ar->tv_id, array('prize_date_availability' => 1));

                foreach( $prize_detail as $pd ){
                    $prizes_view[] = $pd;

                    if( $pd->prize_detail_has_list_of_codes == 'yes' ){

                        $code = $this->main->pickUpCode( $pd->prize_detail_id );
                        $prizes_view[ count($prizes_view) -1 ]->code = $code->value;
                    }
                }

                // TODO: resta de inventario

                $tv_id_associate= $this->main->getPackageTvbyId($ar->tv_id);
                $value_rest= $tv_id_associate->quanty;
                $update_quanty= $value_rest - 1;

                $this->main->update_quanty_prize($ar->tv_id,$update_quanty);


            }

            $data = array(
                'prizes_view' => $prizes_view
                );

            $mail_body = $this->load->view('emails/email-accepted', $data, true);

            $r = $this->model_mails->singleEmail( $appRequestDetail[0]->email, $mail_body, 'LG - masqueuntv.com Solicitud aprobada' );
        }


        $r = $this->main->setPrizeApplicationStatus( $this->input->post('prize_application_id'), $this->input->post('request_status') );
        redirect( base_url() . 'cms/prize_requests' );

    }

    private function reference_validation_edit(){

        $prize_id = $this->grocery_crud->getStateInfo()->primary_key;

        $data = $this->main->get_available_tv_prizes_update( $prize_id );

        $print= "<select name='fk_tv_id' id='field-fk_tv_id' data-placeholder='Seleccionar Referencia'>";
        $print.="<option value=''>Selecciona una Referencia</option>";

        $selected = '';

        foreach ($data as $row)
        {
            $row->prize_id == $prize_id ? $selected = 'selected' : $selected = '';
            $print.= "<option $selected value='".$row->id."'>".$row->reference."</option>";
        }
        $print.= "</select>";
        return $print;
    }

    public function autoload_tool(){

        $data['in_load_tool'] = true;

        $this->load->view('cms/opener',$data);
        $this->load->view('cms/autoload_tool/index');
        $this->load->view('cms/closure');
    }

    public function autoload_data(){

        $this->load->model('autoload_tool');

        $benefict_img = $this->model_files->upload_file('benefictImg', './assets/uploads/files', 'jpg|JPG|png|PNG|gif|GIF' );
        $public_img   = $this->model_files->upload_file('publicImg', './assets/uploads/files', 'jpg|JPG|png|PNG|gif|GIF' );
        $excel_file   = $this->model_files->upload_file('excelFile', './assets/uploads/autoload_files', 'xls|xlsx' );

        // benefictImg, publicImg

        if( $benefict_img['success'] === true && $public_img['success'] === true && $excel_file['success'] === true  ){

            $excel_file_name   = $excel_file['data']['file_name'];
            $benefict_img_name = $benefict_img['data']['file_name'];
            $public_img_name   = $public_img['data']['file_name'];

            $result = $this->autoload_tool->execFile( $excel_file_name, $benefict_img_name, $public_img_name );
            $result['in_load_tool'] = true;

            $this->load->view('cms/opener',$result);
            $this->load->view('cms/autoload_tool/result');
            $this->load->view('cms/closure');

        } else {
            // redirect( base_url() . 'site/set_purchase_data?error=' . $excel_file['error'] );
            die("error subiendo archivos, verifica que los formatos de archivo sean los indicados e intentalo de nuevo");
        }
    }
}

