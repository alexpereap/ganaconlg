<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once  dirname(__FILE__) .  '/../../vendor/swiftmailer/lib/swift_required.php';

class Site extends CI_Controller {

    public function index(){
        // index page
        /*krumo($this->session->all_userdata());
        $this->load->view('test/login');*/

        $this->session->sess_destroy();

        $data = array(
            'in_login'           => true,
            'slides'             => $this->main->getAvailableHomeSlides()
        );

        $this->load->view('site/header', $data);
        $this->load->view('site/login');
        $this->load->view('site/footer');
    }

    // update purchase data
    public function update_purchase_data(){


        $user_id = $this->session->userdata('id');

        // USER UPDATE
        /*$code = $this->input->post('code');

        $code_id = $this->main->getCode($code);
        $code_id = $code_id->id;*/


        // watch out of user_id issue (session issues)
        if( $user_id == null || empty($user_id) ){

            redirect('site/register_issue');
            return;
        }

        // watch out post corruption --> caused by larger files above php.ini upload_max_filesize property value
        if( count($_POST) == 0 ){
            redirect('site/size_issue');
            return;
        }

        // nid override
        $nid = trim($this->input->post('nid'));

        if( empty($nid) ){
            $nid = $this->session->userdata('nid');
        }

        $data = array(
         'name'       => $this->input->post('name'),
         'lastname'   => $this->input->post('lastname'),
         'cellphone'  => $this->input->post('cellphone'),
         'email'      => $this->session->userdata('email'),
         'nid'        => $nid,
         'last_login' => date('Y-m-d g:h:i')
        );


        $this->main->updateUser($user_id, $data);

        // PURCHASE DATA UPADATE

        $data = array(
            'fk_user_id'           => $user_id,
            'fk_sells_point_id'    => $this->input->post('sell_point_id'),
            'purchase_date'        => $this->input->post('purchase_date'),
            'promotional_code_txt' => $this->input->post('promotional_code')
        );

        if( $_FILES['bill_img']['name'] != '' ){
            $result = $this->model_files->upload_file('bill_img', './uploads/bill_imgs','jpg|jpeg|png' );

            if( $result['success'] === true  ){
                $data['bill_img'] =  $result['data']['file_name'];
            } else {
                redirect( base_url() . 'site/set_purchase_data?error=file' );
            }
        }

        $purchase_data_id = $this->input->post('purchase_data_id');


        $this->main->updatePurchaseData( $purchase_data_id, $data );

        // tvs purchase data RE assocc

        // firsts clean current data
        $this->main->deletePurchaseDataTvByPurchaseDataId( $purchase_data_id );

        // then inserts the new data
        $data = array();
        if( isset( $_POST['purchase_data_tv'] ) ){

            foreach( $_POST['purchase_data_tv'] as $t ){
                $data[] = array(
                    'fk_purchase_data_id' => $purchase_data_id,
                    'fk_tv_id'            => $t['tv_id'],
                    'serial_tv'           => $t['serial']
                );
            }

            if( count($data) > 0 ){
                $this->main->insertPurchaseDataTvs( $data );
            }

        }

        $this->sendUserConfirmEmail( $this->input->post('prize_application_id') );


        // encripted user id
        $token = $this->uriencryption->encode($user_id);

        redirect('site/update_data_success?token=' . $token );

    }

    public function update_data_success(){

        $token = $this->input->get('token');

        $data = array(
            'message_title'     => '¡Actualización satisfactoria!',
            'message_content'   => 'Hemos envíado un email de confirmación de datos a tu correo electronico. Por favor revísalo para confirmar tu solicitud.
            <br>Haz click <a href="site/check_status?token=' . $token . '" >aquí</a> para revisar el estado de tu solicitud.'
        );

        $this->load->view('site/header', $data);
        $this->load->view('site/message');
        $this->load->view('site/footer');
    }

    // save purchase data on DB.
    public function save_purchase_data(){

        // watch out post corruption --> caused by larger files above php.ini upload_max_filesize property value
        if( count($_POST) == 0 ){
            redirect('site/size_issue');
            return;
        }

        $this->load->model("model_files");

        // if we're dealing with a new user, insert it on database
        if( $this->session->userdata('new_user') === true ){
            $user_id = $this->insert_user_data();

        } else {
            $user_id = $this->session->userdata('id');

            // optional email update
            if( $this->session->userdata('priority_email') != $this->session->userdata('email') && !empty($user_id) ){

                $this->main->updateUser( $user_id, array('email'  => $this->session->userdata('priority_email') ) );
            }
        }

        // watch out of user_id issue
        if( $user_id == null || empty($user_id) ){

            redirect('site/register_issue');
            return;
        }


        // online buy case (the online user does not have a code)
        $code = '';
        $code_id = null;

        /*krumo($user_id);
        krumo($_POST);
        krumo($_FILES);*/

        // Purchase data insert

        $data = array(
            'fk_user_id'           => $user_id,
            'fk_sells_point_id'    => $this->input->post('sell_point_id'),
            'purchase_date'        => $this->input->post('purchase_date'),
            'fk_codes_id'          => $code_id,
            'promotional_code_txt' => $this->input->post('promotional_code'),
            'code_txt'             => $code
        );

        if( $_FILES['bill_img']['name'] != '' ){
            $result = $this->model_files->upload_file('bill_img', './uploads/bill_imgs','jpg|jpeg|png' );

            if( $result['success'] === true  ){
                $data['bill_img'] =  $result['data']['file_name'];
            } else {
                redirect( base_url() . 'site/set_purchase_data?error=file' );
            }
        }

        $purchase_data_id = $this->main->insertPurchaseData( $data );

        // tvs purchase data assocc

        $data = array();
        if( isset( $_POST['purchase_data_tv'] ) ){

            foreach( $_POST['purchase_data_tv'] as $t ){
                $data[] = array(
                    'fk_purchase_data_id' => $purchase_data_id,
                    'fk_tv_id'            => $t['tv_id'],
                    'serial_tv'           => $t['serial']
                );
            }

            if( count($data) > 0 ){
                $this->main->insertPurchaseDataTvs( $data );
            }

            // krumo($data);
        }

        // prize application insert

        $data = array(
            'fk_purchase_data_id' => $purchase_data_id,
        );

        // krumo($data);

        $prize_application_id = $this->main->insertPrizeApplication($data);

        // Sends email confirm data to person
        // $this->sendUserConfirmEmail( $prize_application_id );

        /*$token = $this->uriencryption->encode($user_id);
        redirect('site/request_success?token=' . $token );*/

        $token = $this->uriencryption->encode($prize_application_id);
        $this->main->verifyUser( $user_id );

        $this->accept_request( $token );

    }


    public function request_success(){

        $token = $this->input->get('token');

        $data = array(
            'message_title'     => '¡Registro satisfactorio!',
            'message_content'   => 'Hemos enviado un email de confirmación con tus datos a tu correo electrónico. Por favor revisalo para confirmar tu solicitud.
            <br>Haz click <a href="site/check_status?token=' . $token . '" >aquí</a> para revisar el estado de tu solicitud.'
        );

        $this->load->view('site/header', $data);
        $this->load->view('site/message');
        $this->load->view('site/footer');
    }

    public function sendUserConfirmEmail( $prize_application_id ){



        $data = array(
            'application_info' => $this->main->getPrizeApplicationDetail( $prize_application_id ),
            'encrypted_app_id' => $this->uriencryption->encode($prize_application_id) // email token
        );

        $mail_body = $this->load->view('emails/email-register-confirm', $data, true);

        // SEND LOGIC
        $this->model_mails->singleEmail( $data['application_info'][0]->email, $mail_body, 'LG - ganoconlg.com Confirmación de datos' );

    }

    // inserts new user data
    private function insert_user_data(){
        $data = array(
            'name'       => $this->input->post('name'),
            'lastname'   => $this->input->post('lastname'),
            'cellphone'  => $this->input->post('cellphone'),
            'email'      => $this->session->userdata('email'),
            'nid'        => $this->session->userdata('nid'),
            'last_login' => date('Y-m-d g:h:i')
        );


        $this->session->set_userdata($data);

        $uid = $this->main->insertUser($data);
        $this->session->set_userdata('id', $uid);
        $this->session->set_userdata('new_user', false);

        return $uid;
    }

    // public view for purchase data
    public function set_purchase_data(){
        $this->load->view('test/purchase_data');
    }

    public function register(){


        // redirects to login if there is no active session
        if( $this->session->userdata('loggedin_public') === false )
            redirect('/');

        $data = array(
            'cities'         => $this->main->getStoreAvailableCities(),
            'promo_code_txt' => $this->main->getPromoCodeText(),
            'user_purchase_data' => array()
        );


        if( $this->session->userdata('new_user') === false ){
            $user_id = $this->session->userdata('id');

            $data['user_purchase_data'] = $this->main->getPurchaseDataPrizeApplicationByUserId( $user_id );

            foreach( $data['user_purchase_data'] as $key_user_purchase_data => $pd ){

                $data['user_purchase_data'][$key_user_purchase_data]->prize_application_status_human = $this->prizeStatusHumanReadable($pd->prize_application_status);

                // gets the associated prize aplication id
                $prize_application = $this->main->getPrizeApplicationByPurchaseDataId( $pd->id );

                $appRequestDetail = $this->main->getPrizeApplicationDetail( $pd->prize_application_id );

                foreach( $appRequestDetail as $ar ){

                    // $prize_detail = $this->main->getPrizeDetailsByTvId( $ar->tv_id , array('prize_date_availability' => 1));
                    $prize_detail = $this->main->getPrizeDetailsByTvId( $ar->tv_id );

                    foreach( $prize_detail as $pd ){
                        $data['user_purchase_data'][$key_user_purchase_data]->prizes_detail[] = $pd;

                    }

                }

            }

        }

        $this->load->view('site/header', $data);
        $this->load->view('site/register');
        $this->load->view('site/footer');
    }

    private function prizeStatusHumanReadable($status){

        switch ($status){

            case 'email_confirmacion':
                return "Email confirmación";
                break;

            case 'en_revision':
                return "En revisión";
                break;

            case 'no_aprobado':
                return "No Aprobado";
                break;

            case 'aprobado':
                return "Aprobado";
                break;

            case 'entregado':
                return "Entregado";
                break;
        }


    }

    public function register_update(){


        // echo $this->uriencryption->encode(14);

        $prize_application_id = intval($this->uriencryption->decode( $this->input->get('token') ));

        if( is_int( $prize_application_id ) ){

            $appRequestDetail = $this->main->getPrizeApplicationDetail($prize_application_id);

            if( count( $appRequestDetail ) > 0 ){

               // if this is already aprobed take me out of here
               if( $appRequestDetail[0]->prize_status == 'aprobado' || $appRequestDetail[0]->prize_status == 'entregado'   ){
                    redirect('site/already_approved');
                    return;
                }

                if( $appRequestDetail[0]->prize_status == 'en_revision' ){
                    redirect('site/in_revision');
                    return;
                }

                $uid = $appRequestDetail[0]->user_id;
                $u = $this->main->getUserById($uid);

                $this->loginExistantUser($u);

                $angular_data = array();

                // preload angular data
                $angular_prizes = array();

                $purchase_data = $this->main->getPurchaseDataByPrizeAppId($prize_application_id);

                foreach( (array) $appRequestDetail as $ard ){

                    $r = $this->main->getAvailableTvPrize( $ard->tv_id );
                    $data_tv_purchase = $this->main->getPurchaseDataTv( $purchase_data->id, $ard->tv_id );

                    $beneficts_availability = $this->parseBenefictsAvailability( $ard->tv_id );

                     $angular_prizes[] = array(
                        'id'                  => $r->tv_id,
                        'tv_id'               => $r->tv_id,
                        'label'               => $r->reference,
                        'value'               => $r->reference,
                        'title'               => $r->prize_title,
                        'terms'               => $r->prize_terms,
                        'image'               => $r->prize_image,
                        'description'         => $r->prize_description,
                        'quanty'              => $r->prize_quanty,
                        'date_availability'   => $beneficts_availability->date_availability,
                        'serial'              => $data_tv_purchase->serial_tv,
                        'has_promo_code'      => filter_var($r->prize_has_promo_code, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE)
                    );
                }

                $angular_data['prizes'] = $angular_prizes;
                $sellpoint = $this->main->getSellPointById( $purchase_data->fk_sells_point_id );


                $angular_data['stores'] = $this->main->getCityStores( $sellpoint->city_id );
                $angular_data['sellpoints'] = $this->main->getStoreSellPoints( $sellpoint->store_id );

                $data = array(
                    'cities'         => $this->main->getStoreAvailableCities(),
                    'promo_code_txt' => $this->main->getPromoCodeText(),
                    'edit_mode'      => true,
                    'angular_data'   => $angular_data,
                    'sellpoint'      => $sellpoint,
                    'purchase_data'  => $purchase_data,
                    'code'           => $this->main->getCodeById( $purchase_data->fk_codes_id )
                );

                // prevents unset code if was an online store
                if( count($data['code']) == 0 ){
                    $data['code'] = (object) array('value' => null);
                }

                $this->load->view('site/header', $data);
                $this->load->view('site/register');
                $this->load->view('site/footer');

            } else{
                $this->token_error("token invalido");
            }

        } else {

            $this->token_error("token invalido");
        }
    }

    public function token_error($str){
        die($str);
    }

    // check if user exists by nid
    public function user_exists($nid){

        if( empty($nid) ) {

            return array('result' => false);
        }
        $u = $this->main->getUserByNid($nid);

        count( $u ) > 0 ? $result['result'] = true : $result['result'] = false;
        return ($result);
    }

    // ajax user exists
    public function ajax_users_exists(){
        echo  json_encode($this->user_exists($_GET['nid']));
    }

    // stores basic login data for firstime user on session
    public function login(){

        if( $_SERVER['REQUEST_METHOD'] != "POST" ){
            die('bad request method');
            return;
        }

        $nid = $this->input->post('nid');

        // checks if is already registred
        $u = $this->main->getUserByNid($nid);
        if( count($u) > 0 ){

            $this->loginExistantUser($u);

        } else {

            $data = array(
                'nid'             => $this->input->post('nid'),
                'email'           => $this->input->post('email'),
                'loggedin_public' => true,
                'new_user'        => true
            );

            $this->session->set_userdata($data);
        }

        // opc email (for further changes of email)

        $this->session->set_userdata('priority_email', $this->input->post('email'));
        redirect('site/register');

    }

    /**
    * @param $u (user data array)
    */
    private function loginExistantUser( $u ){

        $this->session->set_userdata( (array)$u );


            $this->main->updateLastLogin($u->id);

            $this->session->set_userdata('loggedin_public', true);
            $this->session->set_userdata('new_user', false);
    }

    public function logout(){
        $this->session->sess_destroy();
    }

    // callback all neccesary info prior to prelogin
    public function pre_login($nid, $codeVal = null){

        if( !$codeVal )
            $codeVal = null;

        $u    = $this->main->getUserByNid($nid);
        $code = $this->main->getCode($codeVal);

        $code_available = false;

        if( count($code) > 0 ){
            $code_available = $code->available == 'si' ? true : false;
        }

        $result = array(
            'user_exists' => count($u) > 0 ? true : false,
            'code' => array(
                'value' => $codeVal,
                'exists' => count($code) > 0 ? true : false,
                'available' => $code_available,
            )
             // TODO : 'user_codes' => array[]
        );

        return $result;
    }

    // ajax info pre login
    public function ajax_pre_login(){
        echo json_encode ( $this->pre_login($this->input->get('nid'), $this->input->get('code')) );
    }

    // autocomplete refs tv
    public function get_available_prizes_tvs_byref(){
        $ref = $this->input->get('term');

        $tvs = array();

        if( strlen($ref) >= 2 ){

            $result = $this->main->getAvailablePrizesTvsByRef($ref);
            foreach( $result as $r ){

                $beneficts_availability = $this->parseBenefictsAvailability( $r->tv_id );

                $tvs[] = array(
                    'id'                  => $r->tv_id,
                    'tv_id'               => $r->tv_id,
                    'label'               => $r->reference,
                    'value'               => $r->reference,
                    'title'               => $r->prize_title,
                    'terms'               => $r->prize_terms,
                    'image'               => $r->prize_image,
                    'description'         => $r->prize_description,
                    'quanty'              => $r->prize_quanty,
                    'date_availability'   => $beneficts_availability->date_availability,
                    'unavailable_message' => $beneficts_availability->unavailable_message,
                    'has_promo_code'      => filter_var($r->prize_has_promo_code, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE)
                );
            }

        }
        echo json_encode($tvs);
    }

    // gets prizes details
    public function parseBenefictsAvailability($tv_id){

        $beneficts = $this->main->getTvBeneficts( $tv_id );

        $beneficts_count = count($beneficts);
        $unavailable_count = 0;
        $unavailable_products = array();

        foreach( $beneficts as $b ){

            // if not date available
            if( filter_var($b->prize_date_availability, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE) === false ){
                $unavailable_count++;
                $unavailable_products[] = $b->prize_detail_title;
            }
        }

        if( $unavailable_count == 0 ){
            $date_availability = TRUE; // all beneficts available
        } else if( $unavailable_count == $beneficts_count ){
            $date_availability = FALSE; // no beneficts available
        } else{
            $date_availability = 'partial'; // some beneficts available
        }

        $unavailable_message = "Los siguientes beneficios no están disponibles para esta referencia de TV: " . implode(', ',$unavailable_products);


        return (object) array(
            'date_availability'   => $date_availability,
            'unavailable_message' => $unavailable_message
        );

    }

    public function get_user_assoc_tvs_qty($user_id){
        return $this->main->getUserAssocTvsQty($user_id);
    }

    public function ajax_get_user_assoc_tvs_qty(){
        echo json_encode( array('qty' => $this->main->getUserAssocTvsQty( $this->input->get('user_id') )) );
    }

    public function testEmail(){

        $r = $this->model_mails->singleEmail("apexmd21@gmail.com", "hola que hace", "Prueba mandrill app");
        var_dump( $r );
    }



    public function ajax_get_city_stores(){
        $result = $this->main->getCityStores( $this->input->get('city_id') );

        echo json_encode($result);
    }

    public function ajax_get_store_sell_points(){
        $result = $this->main->getStoreSellPoints( $this->input->get('store_id') );

        echo json_encode($result);
    }

    public function ajax_get_code(){
        $result = $this->main->getCode( $this->input->get('code') );

        if( count($result) > 0 ){
            echo json_encode(array(
                'success'   => true,
                'data'      => $result
            ));
        } else {
            echo json_encode(array(
                'success'   => false,
            ));
        }

    }

    public function confirm_data(){

        $prize_application_id = intval($this->uriencryption->decode( $this->input->get('token') ));

        if( is_int( $prize_application_id ) ){

            $appRequestDetail = $this->main->getPrizeApplicationDetail($prize_application_id);

            if( count( $appRequestDetail ) > 0 ){

                // if this is already aprobed take me out of here
                if( $appRequestDetail[0]->prize_status == 'aprobado' || $appRequestDetail[0]->prize_status == 'entregado' ){
                    redirect('site/already_approved');
                    return;
                }

                if( $appRequestDetail[0]->prize_status == 'en_revision' ){
                    redirect('site/in_revision');
                    return;
                }


                $this->main->updatePrizeApplicationStatus( $prize_application_id, 'en_revision' );
                // $this->sendAdminReviewEmail( $prize_application_id );

                $user_id = $appRequestDetail[0]->user_id;

                $this->main->verifyUser( $user_id );

                // $token = $this->uriencryption->encode($user_id);
                // redirect('site/confirm_data_success?token=' . $token );

                // auto accept request
                $this->accept_request();

            } else {
                $this->token_error("token error");
            }
        } else {
            $this->token_error("token error");
        }
    }

    public function sendAdminReviewEmail( $prize_application_id ){



        $purchaseData = $this->main->getPurchaseDataByPrizeAppId( $prize_application_id );

        $data = array(
            'application_info' => $this->main->getPrizeApplicationDetail( $prize_application_id ),
            'encrypted_app_id' => $this->uriencryption->encode($prize_application_id),
            'denial_options'   => $this->main->getDenialOptions(),
            'token'            => $this->uriencryption->encode( $prize_application_id ),
            'promotional_code_txt' => $purchaseData->promotional_code_txt
        );


        $mail_body = $this->load->view('emails/email-admin-review', $data, true);

        // TODO: SEND LOGIC
        $this->model_mails->singleEmail( $this->main->getModEmail(), $mail_body, 'LG - ganoconlg.com Moderación de usuario' );

    }

    public function deny_request(){



        $prize_application_id = intval($this->uriencryption->decode( $this->input->get('token') ));

        if( is_int( $prize_application_id ) ){

            $appRequestDetail = $this->main->getPrizeApplicationDetail($prize_application_id);

            if( count( $appRequestDetail ) > 0 ){

                $this->input->get('denial_option') == 1 ? $denial = null : $denial = $this->main->getDenialOptionById( $this->input->get('denial_option') );

                $data = array(
                    'denial'           => $denial,
                    'application_info' => $appRequestDetail,
                    'token'            => $this->uriencryption->encode( $prize_application_id ),
                    'observation'      => $this->input->get('observation')
                );

                $mail_body = $this->load->view('emails/email-declined', $data, true);
                $this->main->updatePrizeApplicationStatus( $prize_application_id, 'no_aprobado' );

                $this->model_mails->singleEmail( $appRequestDetail[0]->email, $mail_body, 'LG - ganoconlg.com Solicitud de beneficio rechazada' );

                redirect('site/deny_success');

            } else {
                $this->token_error("token invalido");
            }
        } else {
            $this->token_error("token invalido");
        }
    }

    public function form_deny_request(){

        $data = array(
            'denial_options'   => $this->main->getDenialOptions(),
            'token'            => $this->input->get('token')
        );

        $this->load->view('site/header', $data);
        $this->load->view('site/deny_request_form');
        $this->load->view('site/footer');
    }

    public function accept_request( $token = null ){

        if( empty( $token ) ){
            $token = $this->input->get('token');
        }

        $prize_application_id = intval($this->uriencryption->decode( $token ));

        if( is_int( $prize_application_id ) ){

            $appRequestDetail = $this->main->getPrizeApplicationDetail($prize_application_id);

            if( count( $appRequestDetail ) > 0 ){


                // if this is already aprobed take me out of here
               if( $appRequestDetail[0]->prize_status == 'aprobado' || $appRequestDetail[0]->prize_status == 'entregado' ){
                    redirect('site/already_approved');
                    return;
               }

               $prizes_view = array();

               foreach( $appRequestDetail as $ar ){

                    $prize_detail = $this->main->getPrizeDetailsByTvId( $ar->tv_id, array('prize_date_availability' => 1));

                    foreach( $prize_detail as $pd ){
                        $prizes_view[] = $pd;
                        $prizes_view[ count($prizes_view) -1 ]->fk_prize_application_id = $prize_application_id;

                        if( $pd->prize_detail_has_list_of_codes == 'yes' ){

                            $code = $this->main->pickUpCode( $pd->prize_detail_id );
                            $prizes_view[ count($prizes_view) -1 ]->code = $code->value;
                        }
                    }

                    // TODO: resta de inventario

                    $tv_id_associate= $this->main->getPackageTvbyId($ar->tv_id);
                    $value_rest= $tv_id_associate->quanty;
                    $update_quanty= $value_rest - 1;

                    $this->main->update_quanty_prize($ar->tv_id,$update_quanty);


               }

               // store in database prizes view
               $this->main->insertPrizeViews($prizes_view);

               $data = array(
                    'prizes_view' => $prizes_view
               );



               // user email
               $mail_body = $this->load->view('emails/email-accepted', $data, true);

               $this->model_mails->singleEmail( $appRequestDetail[0]->email, $mail_body, 'LG - ganoconlg.com Solicitud aprobada' );
               $this->main->updatePrizeApplicationStatus( $prize_application_id, 'aprobado' );

               // moderation email
               $data['user'] = (object)array(
                    'name'     => $appRequestDetail[0]->name . ' ' . $appRequestDetail[0]->lastname,
                    'document' => $appRequestDetail[0]->nid,
                    'email'    => $appRequestDetail[0]->email,
                    'phone'    => $appRequestDetail[0]->cellphone
               );

               $mail_body = $this->load->view('emails/email-accepted-moderation', $data, true);
               $mod_email = $this->main->getModEmail();

               $this->model_mails->singleEmail( $mod_email, $mail_body, 'LG - ganoconlg.com Solicitud aprobada' );

               // redirect('site/approved_success');

               // redirect to prizes overview
               redirect('site/prizes_overview?token=' . $token );


            } else {
                $this->token_error("token invalido");
            }
        } else {
            $this->token_error("token invalido");
        }
    }

    public function confirm_data_success(){

        $token = $this->input->get('token');

        $data = array(
            'message_title'     => '¡Has confirmado tus datos!',
            'message_content'   => 'Hemos envíado tu solicitud para que sea evaluada por un asesor, pronto nos pondremos en contacto contigo.
            <br>Haz click <a href="site/check_status?token=' . $token . '" >aquí</a> para revisar el estado de tu solicitud.'
        );

        $this->load->view('site/header', $data);
        $this->load->view('site/message');
        $this->load->view('site/footer');
    }

    public function deny_success(){

        $data = array(
            'message_title'     => '¡Se ha rechazado la solicitud!',
            'message_content'   => 'Se ha enviado un email al cliente con las razones especificas del rechazo de su solicitud.'
        );

        $this->load->view('site/header', $data);
        $this->load->view('site/message');
        $this->load->view('site/footer');
    }

    public function already_approved(){

        $data = array(
            'message_title'     => 'Solicitud aprobada',
            'message_content'   => 'Esta solicitud ya ha sido aprobada'
        );

        $this->load->view('site/header', $data);
        $this->load->view('site/message');
        $this->load->view('site/footer');
    }

    public function in_revision(){

        $data = array(
            'message_title'     => 'Estamos revisando tu solicitud',
            'message_content'   => 'Esta solicitud se encuentra en revisión, pronto un asesor de LG se colocará en contacto contigo.'
        );

        $this->load->view('site/header', $data);
        $this->load->view('site/message');
        $this->load->view('site/footer');
    }

    public function approved_success(){

        $data = array(
            'message_title'     => '¡Se ha aprobado la solicitud!',
            'message_content'   => 'Hemos envíado un email con los detalles de los beneficios a tu correo electronico. Por favor revísalo.'
        );

        $this->load->view('site/header', $data);
        $this->load->view('site/message');
        $this->load->view('site/footer');
    }

    public function test(){
        $this->main->getModEmail();
    }

    public function check_status(){

        $uid = $this->uriencryption->decode( $this->input->get('token') );
        $u = $this->main->getUserById($uid);

        if( count($u) > 0 ){
            $this->loginExistantUser($u);

            redirect('site/register?action=openBillsModal');

        } else {
            die("Lo sentimos no hemos encontrado este usuario");
        }

    }

    public function register_issue(){

        $data = array(
            'message_title'     => 'Hubo un problema',
            'message_content'   => 'Detectamos un problema al procesar tus datos, puede que se trate de un problema con tu conexión de internet, por favor intentalo de nuevo haciendo click <a href="site/" >aquí</a>'
        );

        $this->load->view('site/header', $data);
        $this->load->view('site/message');
        $this->load->view('site/footer');
    }

    public function size_issue(){

        $data = array(
            'message_title'     => 'Hubo un problema',
            'message_content'   => 'Lo sentimos pero el peso de las imágenes excede el límite permitido. Asegúrate de no superar los 6 MB por archivo, por favor intentalo de nuevo haciendo click <a href="site/register" >aquí</a>'
        );

        $this->load->view('site/header', $data);
        $this->load->view('site/message');
        $this->load->view('site/footer');
    }

    public function php_info(){
        phpinfo();
    }


    public function send_contact_form(){
        $mod_email = $this->main->getModEmail();

        $nombre  = $this->input->post('nombre');
        $email   = $this->input->post('email');
        $mensaje = $this->input->post('msg');

        $mail_content = "<p>Nombre: $nombre</p>";
        $mail_content .= "<p>Email: $email</p>";
        $mail_content .= "<p>Mensaje:</p><p>$mensaje</p>";

        $this->model_mails->singleEmail( $mod_email, $mail_content, 'Un usuario te esta contactando desde ganoconlg.com' );
    }

    public function get_tv_prize_by_ref(){

        $ref = $this->input->get('serial');
        $r = $this->main->getTvPrizeByRef($ref);

        if( count($r) > 0 ){

            $beneficts_availability = $this->parseBenefictsAvailability( $r->tv_id );

            $result = array(
                'success' => true,
                'data'    =>  array(
                    'id'                  => $r->tv_id,
                    'tv_id'               => $r->tv_id,
                    'label'               => $r->reference,
                    'value'               => $r->reference,
                    'title'               => $r->prize_title,
                    'terms'               => $r->prize_terms,
                    'image'               => $r->prize_image,
                    'description'         => $r->prize_description,
                    'quanty'              => $r->prize_quanty,
                    'date_availability'   => $beneficts_availability->date_availability,
                    'unavailable_message' => $beneficts_availability->unavailable_message,
                    'has_promo_code'      => filter_var($r->prize_has_promo_code, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE)
                    )
                );

           echo json_encode($result);
           return;
        } else {

            $result['success'] = false;
            echo json_encode( $result );
            return;
        }
    }

    public function prizes_overview(){

        $prize_application_id = intval($this->uriencryption->decode( $this->input->get('token') ));
        $prizes_view = $this->main->getPrizeViewByApplicationId( $prize_application_id );

        if( count( $prizes_view ) > 0 ){

            $data = array(
                'prizes_view'   => $prizes_view,
                'prize_request' => $this->main->getPrizeRequestByPrizeApplicationId( $prize_application_id )
            );

            $this->load->view('site/header', $data);
            $this->load->view('site/prize');
            $this->load->view('site/footer');

        }
    }
}

