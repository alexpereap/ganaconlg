<form class="form-horizontal" method="POST" action="<?php echo base_url() ?>cms/update_sellpoint" >
<input type="hidden" name="sellpoint_id" value="<?php echo $sellpoint->sellpoint_id ?>" >
<fieldset ng-controller="sellPointController" ng-init='cityStoresSwitch = false; cityStores = <?php echo json_encode( $stores ) ?>' >

<!-- Form Name -->
<legend>Punto de venta</legend>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="sellpoint">Punto de venta</label>
  <div class="col-md-6">
  <input id="sellpoint" name="sellpoint" value="<?php echo $sellpoint->sellpoint_name ?>" required type="text" placeholder="Nombre del punto de venta" class="form-control input-md">

  </div>
</div>

<!-- Select Basic -->
<div class="form-group">
  <label class="col-md-4 control-label" for="city">Ciudad</label>
  <div class="col-md-6">
    <select ng-model="city_id" ng-change="updateSellPoint(city_id)" id="city" required name="city" class="form-control">
      <option value="">Seleccione...</option>
      <?php foreach( $cities as $c ): ?>
      <option <?php if( $c->id == $sellpoint->city_id ) echo "ng-selected='true'"; ?> value="<?php echo $c->id ?>"><?php echo $c->name ?></option>
      <?php endforeach; ?>
    </select>
  </div>
</div>

<!-- Select Basic -->
<div class="form-group">
  <label class="col-md-4 control-label" for="cityStores">Cadena</label>
  <div class="col-md-6">
    <select required ng-disabled="cityStoresSwitch" id="cityStores" name="storeId" class="form-control">
      <option value="">{{ cityStoresEmptyValue }}</option>
      <option value="{{ cs.id }}" ng-selected="cs.id == <?php echo $sellpoint->store_id ?>" ng-repeat="cs in cityStores" >{{ cs.name }}</option>
    </select>
  </div>
</div>

<!-- Button (Double) -->
<div class="form-group">
  <label class="col-md-4 control-label" for="saveBtn"></label>
  <div class="col-md-8">
    <button id="saveBtn" ng-disabled="saveDisabled" name="saveBtn" type="submit" class="btn btn-success">Guardar</button>
    <button id="cancelBtn" type="button" route="<?php echo base_url() ?>cms/sellpoints" name="cancelBtn" class="btn btn-danger">Cancelar</button>
  </div>
</div>

</fieldset>
</form>
