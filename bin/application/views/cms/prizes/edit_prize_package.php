<script src="<?php echo base_url(); ?>vendor/tinymce/js/tinymce/tinymce.min.js" ></script>
<script>

$(document).ready(function(){
  init_tinymce();
});

function init_tinymce(){
    tinymce.init({
      selector:'textarea',
      menubar: 'edit insert view format table tools' ,
      plugins: [
            "advlist autolink lists link image charmap print preview anchor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table contextmenu paste"
            ],
      toolbar: " terminology  | undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | fontselect | fontsizeselect",
      fontsize_formats: "8pt 9pt 10pt 11pt 12pt 26pt 36pt",
      language:'es'
    });
}
</script>

<form method="POST" id="prizePackageForm" class="form-horizontal" action="cms/update_prize_package" ng-controller="prizesController"

<?php if( isset( $angular_data ) ): ?>

  ng-init = '
    store_qty = <?php echo $angular_data->store_qty; ?>;
    store_blocks = <?php echo json_encode( $angular_data->store_blocks ) ?>;
    cityStoresSwitch = <?php echo json_encode( $angular_data->cityStoresSwitch ) ?>;
    cityStores = <?php echo json_encode( $angular_data->cityStores ) ?>;
    cityStoresEmptyValue = <?php echo json_encode( $angular_data->cityStoresEmptyValue ) ?>;
    selected_values = <?php echo json_encode($angular_data->selected_values) ?>;
  '
<?php endif; ?>

enctype="multipart/form-data" ><!-- end form tag -->

<input type="hidden" name="prize_id" value="<?php echo $prize_package->id ?>" >


<fieldset  >

<!-- Form Name -->
<legend>Asignar tv a promoción</legend>

<!-- Select Basic -->
<div class="form-group">
  <label class="col-md-4 control-label" for="fk_tv_id">Serial televisor</label>
  <div class="col-md-4">
    <select id="fk_tv_id" required name="fk_tv_id" class="form-control">
        <option value="" selected>Seleccione...</option>
      <?php foreach( $tvs as $t ): ?>
      <option  <?php if( $t->id == $prize_package->fk_tv_id ) echo "selected" ?> value="<?php echo $t->id ?>"><?php echo $t->reference ?></option>
      <?php endforeach; ?>
    </select>
  </div>
</div>

<!-- Select Basic -->
<div class="form-group">
  <label class="col-md-4 control-label" for="fk_tv_id">Promociones</label>
  <div class="col-md-4">
    <select class="make-fancy-multiselect" required multiple id="prize_items" name="prize_items[]" class="form-control">
      <?php foreach( $prize_items as $p ): ?>
        <option <?php if( in_array( $p->id ,  $prize_items_ids ) ) echo "selected" ?> value="<?php echo $p->id ?>"><?php echo $p->title ?></option>
      <?php endforeach; ?>
    </select>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label">
    Detalles de la promoción:
  </label>
  <div class="col-md-4">

  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="title">Titulo</label>
  <div class="col-md-4">
  <input id="title" value="<?php echo $prize_package->title ?>" name="title" type="text" placeholder="" class="form-control input-md" required="">

  </div>
</div>

<!-- File Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for="image">Imágen</label>
  <div class="col-md-4">
    <input id="image" name="image" class="input-file" type="file">
  </div>
</div>

<!-- Textarea -->
<div class="form-group">
  <label class="col-md-4 control-label" for="description">Descripción</label>
  <div class="col-md-6">
    <textarea rows="10" class="form-control" id="description" name="description"><?php echo $prize_package->description ?></textarea>
  </div>
</div>


<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="quanty">Cantidad</label>
  <div class="col-md-1">
  <input value="<?php echo $prize_package->quanty ?>" id="quanty" name="quanty" type="number" required placeholder="" class="form-control input-md" required="">

  </div>
</div>

<div class="form-group">
  <!-- <label class="col-md-4 control-label" for="has_promo_code">¿Cuenta con código promocional?</label> -->
  <!-- <div class="col-md-1">
    <select  id="has_promo_code" required="" name="has_promo_code" class="form-control">
      <option <?php if( $prize_package->has_promo_code == 'no' ) echo "selected" ?> value="no">No</option>
      <option <?php if( $prize_package->has_promo_code == 'yes' ) echo "selected" ?> value="yes">Si</option>
    </select>
  </div> -->
  <input type="hidden" name="has_promo_code" value="no" >
</div>

<!-- Textarea -->
<div class="form-group">
  <label class="col-md-4 control-label" for="terms">Términos y condiciones</label>
  <div class="col-md-6">
    <textarea rows="10" class="form-control" id="terms" name="terms"><?php echo $prize_package->terms ?></textarea>
  </div>
</div>


<?php /*
<div class="form-group">
  <label class="col-md-4 control-label" for="terms">Disponibilidad en puntos de venta:</label>
  <div class="col-md-6">
    <div id="sellpointsList">
      <ul>
      <?php foreach( $cities_store_sellpoints as $css ): ?>
        <li><?php echo $css->name ?>

        <?php if( count( $css->stores ) > 0 ): ?>
        <ul>
          <?php foreach( $css->stores as $cs  ): ?>
            <li><?php echo $cs->name ?>
              <?php if( count( $cs->sellpoints ) ): ?>
                <ul>
                  <?php foreach( $cs->sellpoints as $s ): ?>
                    <li  <?php if( in_array( $s->id , $prize_sellpoint_ids ) ) echo 'data-jstree=\'{"selected":true}\'' ?> id="<?php echo $s->id ?>" ><?php echo $s->name; ?></li>
                  <?php endforeach; ?>
                </ul>
              <?php endif; ?>

            </li>
          <?php endforeach; ?>
        </ul>
        <?php endif; ?>

        </li>
      <?php endforeach; ?>
      </ul>
    </div>
  </div>
</div>
*/ ?>


<input type="hidden" name="sellpointIds"  id="sellpointIds" >

<!-- Button (Double) -->
<div class="form-group">
  <label class="col-md-4 control-label" for="saveBtn"></label>
  <div class="col-md-8">
    <button id="saveBtn" ng-disabled="saveDisabled" name="saveBtn" type="submit" class="btn btn-success">Guardar</button>
    <button id="cancelBtn" type="button" route="<?php echo base_url() ?>cms/prize" name="cancelBtn" class="btn btn-danger">Cancelar</button>
  </div>
</div>


</fieldset>
</form>
