<!DOCTYPE html>
<html lang="en" ng-app="cmsApp" >
  <head>
    <base href="<?php echo base_url(); ?>">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title></title>


    <!-- Custom styles for this template -->
    <style>
    body {
      padding-top: 20px;
      padding-bottom: 20px;
    }

    .navbar {
      margin-bottom: 20px;
    }

    </style>



    <?php
      if( isset($css_files) )
      foreach($css_files as $file): ?>
      <link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
    <?php endforeach; ?>

    <?php
      if( isset($js_files) )
      foreach($js_files as $file): ?>
      <script src="<?php echo $file; ?>"></script>
    <?php endforeach; ?>


    <?php if( !isset( $js_files ) ): ?> <script src="<?php echo base_url() ?>vendor/jquery-1.10.2.min.js" ></script> <?php endif; ?>


    <script src="vendor/angular/angular.min.js" ></script>
    <script src="vendor/angular/angular-route.js"></script>

    <?php if( !isset( $js_files ) ): ?>
    <link rel="stylesheet" href="css/datepicker.css">

    <script src="js/bootstrap-datepicker.js"></script>
    <script src="js/bootstrap-datepicker.es.js"></script>
    <?php endif; ?>


    <link rel="stylesheet" href="vendor/lou-multi-select-8712b02/css/multi-select.css">
    <script src="vendor/lou-multi-select-8712b02/js/jquery.multi-select.js" ></script>


    <link rel="stylesheet" href="vendor/vakata-jstree-aa240c1/dist/themes/default/style.css">
    <script src="vendor/vakata-jstree-aa240c1/dist/jstree.js" ></script>

    <script src="js/php.js" ></script>

    <script src="js/cms.js" ></script>
    <?php
      if( isset($custom_js) )
      foreach($custom_js as $file): ?>
      <script src="<?php echo $file; ?>"></script>
    <?php endforeach; ?>

    <!-- Bootstrap core CSS -->
    <link href="vendor/tw_bootstrap/css/bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" href="css/cms.css">

     <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="vendor/ie10-viewport-bug-workaround.js"></script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>
    <div class="container">
    <input type="hidden" id="baseUrl" value="<?php echo base_url();?>"  >

      <!-- Static navbar -->
      <div class="navbar navbar-default" role="navigation">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php echo base_url() ?>cms">LG Beneficios cms</a>
          </div>
          <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
              <li class="<?php if( isset($in_promos) ): ?>active<?php endif ?>" ><a href="<?php echo base_url() ?>cms/promos">Imágenes home</a></li>
              <!-- Tvs -->
            <li class="<?php if( isset($in_tvs) ): ?>active<?php endif ?>" ><a href="<?php echo base_url() ?>cms/tv">Seriales</a></li>
            <!--<li class="dropdown <?php if( isset($in_codes) ): ?>active<?php endif ?>"  >
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Códigos <span class="caret"></span></a>
                <ul class="dropdown-menu" role="menu">
                  <li class="<?php if( isset($in_codes_text) ): ?>active<?php endif ?>" ><a href="<?php echo base_url() ?>cms/promo_code_txt">Texto código de beneficio</a></li>
                </ul>
              </li>-->

              <!-- Users -->
              <li class="dropdown <?php if( isset($in_users) ): ?>active<?php endif ?>"  >
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Usuarios <span class="caret"></span></a>
                <ul class="dropdown-menu" role="menu">
                  <li class="<?php if( isset($in_users_cms) ): ?>active<?php endif ?>" ><a href="<?php echo base_url() ?>cms/cms_users">Usuarios cms</a></li>
                  <li class="<?php if( isset($in_users_participants) ): ?>active<?php endif ?>" ><a href="<?php echo base_url() ?>cms/participants">Participantes</a></li>
                </ul>
              </li>

              <!-- Stores -->
              <li class="dropdown <?php if( isset($in_stores) ): ?>active<?php endif ?>"  >
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Puntos de venta<span class="caret"></span></a>
                <ul class="dropdown-menu" role="menu">
                  <li class="<?php if( isset($in_stores_stores) ): ?>active<?php endif ?>" ><a href="<?php echo base_url() ?>cms/stores">Cadenas</a></li>
                  <li class="<?php if( isset($in_stores_sellpoints) ): ?>active<?php endif ?>" ><a href="<?php echo base_url() ?>cms/sellpoints">Puntos de venta</a></li>
                </ul>
              </li>

              <li class="dropdown <?php if( isset($in_prize) ): ?>active<?php endif ?>"  >
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">Beneficios <span class="caret"></span></a>
                  <ul class="dropdown-menu" role="menu">
                  <li class="<?php if( isset($in_prize_detail) ): ?>active<?php endif ?>" ><a href="<?php echo base_url() ?>cms/prize_detail">Beneficios</a></li>
                  <!-- <li class="<?php if( isset($in_prize_codes) ): ?>active<?php endif ?>" ><a href="<?php echo base_url() ?>cms/prize_codes">Asociar lista de códigos</a></li> -->
                  <li class="<?php if( isset($in_prize_prizes) ): ?>active<?php endif ?>" ><a href="<?php echo base_url() ?>cms/prize">Asignar serial a beneficio</a></li>
                  <li class="<?php if( isset($in_prize_requests) ): ?>active<?php endif ?>" ><a href="<?php echo base_url() ?>cms/prize_requests">Solicitudes de beneficios</a></li>
                  <li class="<?php if( isset($in_prize_email) ): ?>active<?php endif ?>" ><a href="<?php echo base_url() ?>cms/email_mod">Email de moderación</a></li>
                  <li class="<?php if( isset($in_load_tool) ): ?>active<?php endif ?>" ><a href="<?php echo base_url() ?>cms/autoload_tool">Carga automática de beneficios</a></li>
                </ul>
              </li>


            </ul>

           <ul class="nav navbar-nav navbar-right">
                <li><a title="cerrar sesión" href="<?php echo base_url() ?>cms/logout">Cerrar sesión <span class="glyphicon glyphicon-off" ></span></a></li>
            </ul>

          </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
      </div>


