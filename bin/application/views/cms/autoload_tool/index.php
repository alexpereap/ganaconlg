<form class="form-horizontal" method="POST" enctype="multipart/form-data" action="cms/autoload_data" >
<fieldset>

<!-- Form Name -->
<legend>Carga automática de beneficios</legend>

<!-- File Button --> 
<div class="form-group">
  <label class="col-md-4 control-label" for="excelFile">Archivo Excel</label>
  <div class="col-md-4">
    <input id="excelFile" name="excelFile" class="input-file" type="file">
  </div>
</div>

<!-- File Button --> 
<div class="form-group">
  <label class="col-md-4 control-label" for="benefictImg">Imágen del beneficio que se verá en el email</label>
  <div class="col-md-4">
    <input id="benefictImg" name="benefictImg" class="input-file" type="file">
  </div>
</div>

<!-- File Button --> 
<div class="form-group">
  <label class="col-md-4 control-label" for="publicImg">Imágen del beneficio que se verá en el sitio público</label>
  <div class="col-md-4">
    <input id="publicImg" name="publicImg" class="input-file" type="file">
  </div>
</div>

<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for="singlebutton">&nbsp;</label>
  <div class="col-md-4">
    <button id="singlebutton" name="singlebutton" type="submit" class="btn btn-primary">Envíar</button>
  </div>
</div>


</fieldset>
</form>
