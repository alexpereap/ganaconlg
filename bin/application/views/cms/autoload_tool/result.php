<div>
    <p>Se cargarón <?php echo $success ?> beneficios satisfactoriamente</p>

    <?php if( count($errors) > 0 ): ?>
        <p>Los siguientes seriales presentarón problemas al cargar el beneficio, por favor revisa su informacion:</p>
        <?php foreach( $errors as $e ): ?>
            <p>Fila: <?php echo $e['fila'] ?>, Serial: <?php echo $e['serial'] ?></p>
        <?php endforeach; ?>
    <?php else: ?>
    <p>No hubo errores cargando algún beneficio</p>
    <?php endif; ?>
</div>