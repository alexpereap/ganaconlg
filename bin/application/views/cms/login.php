<!DOCTYPE html>
<html lang="en">
  <head>
    <base href="<?php echo base_url() ?>">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Gano con LG | Iniciar sesión</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url() ?>vendor/tw_bootstrap/css/bootstrap.css" rel="stylesheet">
    <script src="<?php echo base_url() ?>assets/grocery_crud/js/jquery-1.10.2.min.js" ></script>
    <script src="js/login.js" ></script>
    <!-- Custom styles for this template -->
    <link href="<?php echo base_url() ?>css/signin.css" rel="stylesheet">


    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="vendor/ie10-viewport-bug-workaround.js"></script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->


  </head>

  <body>

    <div class="container">

      <form class="form-signin" role="form" id="loginForm" >
        <h2 class="form-signin-heading">Iniciar sesión</h2>
        <input type="text" class="form-control" placeholder="Nombre de usuario" required id="cmsUsername" autofocus>
        <input type="password" class="form-control" placeholder="Contraseña" id="cmsPassword" required>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Entrar</button>
      </form>

    </div> <!-- /container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="vendor/tw_bootstrap/js/bootstrap.min.js"></script>
  </body>
</html>
