<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>LG Electronics Colombia</title>
</head>

<body>
<table width="580" border="0" align="center" cellpadding="0" cellspacing="0" style="font-family:Tahoma, Geneva, sans-serif; color:#666666; padding:0 10px;">
  <tr>
    <td style="border-bottom: 1px solid #CCC; padding:20px 0"><table width="580" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="381" align="left" valign="middle"><img src="<?php echo base_url() ?>img/email/lg-logo.png" width="112" height="54" style="margin-left:20px; display:block; border:0;"></td>
          <td width="100" align="right" valign="bottom"><img src="<?php echo base_url() ?>img/email/siguenosen.gif" width="100" height="33"></td>
          <td width="33" align="center" valign="bottom"><a href="http://www.facebook.com/LGLatino" target="_blank"><img src="<?php echo base_url() ?>img/email/social-facebook.gif" width="33" height="33" alt="Facebook" style="display:block; border:0;"></a></td>
          <td width="33" align="center" valign="bottom"><a href="http://www.twitter.com/LivingLG" target="_blank"><img src="<?php echo base_url() ?>img/email/social-twitter.gif" alt="Twitter" width="33" height="33" style="display:block; border:0;"></a></td>
          <td width="33" align="center" valign="bottom"><a href="http://www.youtube.com/livingLGvideo" target="_blank"><img src="<?php echo base_url() ?>img/email/social-youtube.gif" alt="Youtube" width="33" height="33" style="display:block; border:0;"></a></td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td style="padding:20px 0"><p style="color:#c32357; font-size:20px; margin:5px 0;">Lo sentimos, tu información no fue validada correctamente por alguna de las siguientes razones:</p>
      <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-top:3px solid #cccccc; background-color:#eeeeee; padding:20px 10px; margin-top:20px;">
        <tr>
          <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td style="font-size:12px;">
                <?php if( !empty( $denial ) ): ?>
                <ul style=" list-style:disc;">
                  <li><?php echo $denial ?></li>
                </ul>
              <?php endif; ?>
              <?php if( !empty( $observation ) ): ?>
                <p><?php echo $observation ; ?></p>
              <?php endif; ?>
                </td>
              </tr>
            </table></td>
        </tr>
      </table>
      <table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-top:15px;">
        <tr>
          <td align="center" valign="top"><a target="_BLANK" href="<?php echo base_url() ?>site/register_update?token=<?php echo $token ?>" ><img src="<?php echo base_url() ?>img/email/btVerify.gif" width="290" height="70" style="border:0;"></a></td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td style="border-top: 1px solid #CCC; padding:20px 0; font-size:14px">Línea de atención Panamá 800-5454, República Dominicana 1-800-751-5454<br/>
      <a href="http://www.lge.com.pa" target="_blank" style="color:#666666; text-decoration:none;">www.lge.com.pa</a><br/>
      <span style="color:#c32357">Con LG todo es posible.</span><br/>
      <br/>
      <span style="font-size:11px;">Copyright © 2014 LG Electronics. Todos los derechos reservados.</span></td>
  </tr>
</table>
</body>
</html>
