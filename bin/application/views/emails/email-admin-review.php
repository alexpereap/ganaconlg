<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>LG Electronics Colombia</title>
</head>

<body>
<table width="580" border="0" align="center" cellpadding="0" cellspacing="0" style="font-family:Tahoma, Geneva, sans-serif; color:#666666; padding:0 10px;">
  <tr>
    <td style="border-bottom: 1px solid #CCC; padding:20px 0"><table width="580" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="381" align="left" valign="middle"><img src="<?php echo base_url() ?>img/email/lg-logo.png" width="112" height="54" style="margin-left:20px; display:block; border:0;"></td>
          <td width="100" align="right" valign="bottom"><img src="<?php echo base_url() ?>img/email/siguenosen.gif" width="100" height="33"></td>
          <td width="33" align="center" valign="bottom"><a href="http://www.facebook.com/LGLatino" target="_blank"><img src="<?php echo base_url() ?>img/email/social-facebook.gif" width="33" height="33" alt="Facebook" style="display:block; border:0;"></a></td>
          <td width="33" align="center" valign="bottom"><a href="http://www.twitter.com/LivingLG" target="_blank"><img src="<?php echo base_url() ?>img/email/social-twitter.gif" alt="Twitter" width="33" height="33" style="display:block; border:0;"></a></td>
          <td width="33" align="center" valign="bottom"><a href="http://www.youtube.com/livingLGvideo" target="_blank"><img src="<?php echo base_url() ?>img/email/social-youtube.gif" alt="Youtube" width="33" height="33" style="display:block; border:0;"></a></td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td style="padding:20px 0"><p style="color:#c32357; font-size:20px; margin:5px 0;">El usuario (<?php echo $application_info[0]->name ?> <?php echo $application_info[0]->lastname ?>) ha ingresado su información en el sistema.</p>
      <p style="font-size:14px;  margin:5px 0">Ayúdale a recibir nuestro regalo validando su registro.</p>
      <img src="<?php echo base_url() ?>img/email/datos-personales2.gif" width="580" height="60" style="display:block; border:0; margin-top:20px;">
      <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-top:3px solid #cccccc; background-color:#eeeeee; padding:20px 10px">
        <tr>
          <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="30%" style="font-size:12px; padding:3px 0;"><b>Nombre:</b></td>
                <td width="70%" style="font-size:12px;"><?php echo $application_info[0]->name ?></td>
              </tr>
              <tr>
                <td style="font-size:12px; padding:3px 0;"><b>Apellido:</b></td>
                <td style="font-size:12px;"><?php echo $application_info[0]->lastname ?></td>
              </tr>
              <tr>
                <td style="font-size:12px; padding:3px 0;"><b>Cédula:</b></td>
                <td style="font-size:12px;"><?php echo $application_info[0]->nid ?></td>
              </tr>
              <tr>
                <td style="font-size:12px; padding:3px 0;"><b>E-mail:</b></td>
                <td style="font-size:12px;"><?php echo $application_info[0]->email ?></td>
              </tr>
              <tr>
                <td style="font-size:12px; padding:3px 0;"><b>Teléfono Celular:</b></td>
                <td style="font-size:12px;"><?php echo $application_info[0]->cellphone ?></td>
              </tr>
            </table>
            </td>
        </tr>
      </table>
      <img src="<?php echo base_url() ?>img/email/datos-tv2.gif" width="580" height="60" style="display:block; border:0; margin-top:20px;">
      <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-top:3px solid #cccccc; background-color:#eeeeee; padding:20px 10px">
        <tr>
          <td>
            <?php

              $show_promo_code = false;
              foreach( $application_info  as $ai ):

              if( $ai->prize_has_promo_code == 'yes' )
                $show_promo_code = true;
            ?>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding:10px 0;">
              <tr>
                <td width="30%" style="font-size:12px; padding:3px 0;"><b>Serial:</b></td>
                <td width="70%" style="font-size:12px;"><?php echo $ai->tv_reference ?></td>
              </tr>
            </table>
          <?php endforeach; ?>

          <?php if( $show_promo_code === true ): ?>
          <table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding:10px 0;" >
            <tr>
                <td width="30%" style="font-size:12px; padding:3px 0;"><b>Código de beneficio:</b></td>
                <td width="70%" style="font-size:12px;"><?php echo $promotional_code_txt ?></td>
              </tr>
          </table>
          <?php endif; ?>
          </td>
        </tr>
      </table>
      <img src="<?php echo base_url() ?>img/email/datos-compra2.gif" width="580" height="60" style="display:block; border:0; margin-top:20px;">
      <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-top:3px solid #cccccc; background-color:#eeeeee; padding:20px 10px">
        <tr>
          <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="30%" style="font-size:12px; padding:3px 0;"><b>Nombre:</b></td>
                <td width="70%" style="font-size:12px;"><?php echo $application_info[0]->name ?></td>
              </tr>
              <tr>
                <td style="font-size:12px; padding:3px 0;"><b>Apellido:</b></td>
                <td style="font-size:12px;"><?php echo $application_info[0]->lastname ?></td>
              </tr>
              <tr>
                <td style="font-size:12px; padding:3px 0;"><b>Cédula:</b></td>
                <td style="font-size:12px;"><?php echo $application_info[0]->nid ?></td>
              </tr>
              <tr>
                <td style="font-size:12px; padding:3px 0;"><b>E-mail:</b></td>
                <td style="font-size:12px;"><?php echo $application_info[0]->email ?></td>
              </tr>
              <tr>
                <td style="font-size:12px; padding:3px 0;"><b>Teléfono Celular:</b></td>
                <td style="font-size:12px;"><?php echo $application_info[0]->cellphone ?></td>
              </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td style="font-size:12px; padding:3px 0;"><b>Foto Facutra:</b></td>
              </tr>
              <tr>
                <td align="center" valign="top"><img src="<?php echo base_url() ?>timthumb.php?src=<?php echo base_url() ?>uploads/bill_imgs/<?php echo $application_info[0]->bill_img ?>&w=400&h=266&zc=2" width="400" height="266" style="border:0;"></td>
              </tr>
            </table></td>
        </tr>
      </table>
      <table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-top:15px;">
        <tr>
          <td align="center" valign="top" style="border-top: 1px solid #CCC;"><a href="<?php echo base_url() ?>site/accept_request?token=<?php echo $token; ?>"><img src="<?php echo base_url() ?>img/email/btApprove.gif" width="290" height="70" style="border:0;"></a></td>
        </tr>
        <tr><td align="center" valign="top" style="border-top: 1px solid #CCC;"><a href="<?php echo base_url() ?>site/form_deny_request?token=<?php echo $token; ?>"><img src="<?php echo base_url() ?>img/email/btDecline.gif" width="290" height="70" style="border:0;"></a></td></tr>
      </table></td>
  </tr>
  <tr>
    <td style="border-top: 1px solid #CCC; padding:20px 0; font-size:14px">Línea de atención Panamá 800-5454, República Dominicana 1-800-751-5454<br/>
      <a href="http://www.lge.com.pa" target="_blank" style="color:#666666; text-decoration:none;">www.lge.com.pa</a><br/>
      <span style="color:#c32357">Con LG todo es posible.</span><br/>
      <br/>
      <span style="font-size:11px;">Copyright © 2014 LG Electronics. Todos los derechos reservados.</span></td>
  </tr>
</table>
</body>
</html>
