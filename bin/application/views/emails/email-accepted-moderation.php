<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>LG Electronics Colombia</title>
</head>

<body>
<table width="580" border="0" align="center" cellpadding="0" cellspacing="0" style="font-family:Tahoma, Geneva, sans-serif; color:#666666; padding:0 10px;">
  <tr>
    <td style="border-bottom: 1px solid #CCC; padding:20px 0"><table width="580" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="381" align="left" valign="middle"><img src="<?php echo base_url() ?>img/email/lg-logo.png" width="112" height="54" style="margin-left:20px; display:block; border:0;"></td>
          <td width="100" align="right" valign="bottom"><img src="<?php echo base_url() ?>img/email/siguenosen.gif" width="100" height="33"></td>
          <td width="33" align="center" valign="bottom"><a href="http://www.facebook.com/LGLatino" target="_blank"><img src="<?php echo base_url() ?>img/email/social-facebook.gif" width="33" height="33" alt="Facebook" style="display:block; border:0;"></a></td>
          <td width="33" align="center" valign="bottom"><a href="http://www.twitter.com/LivingLG" target="_blank"><img src="<?php echo base_url() ?>img/email/social-twitter.gif" alt="Twitter" width="33" height="33" style="display:block; border:0;"></a></td>
          <td width="33" align="center" valign="bottom"><a href="http://www.youtube.com/livingLGvideo" target="_blank"><img src="<?php echo base_url() ?>img/email/social-youtube.gif" alt="Youtube" width="33" height="33" style="display:block; border:0;"></a></td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td style="padding:20px 0"><p style="color:#c32357; font-size:20px; margin:5px 0;">Un usuario ha reclamado los siguientes beneficios en ganoconlg.com</p>
      <p style="font-size:14px;  margin:5px 0">
        <ul>
          <li><div style="width:100px;display: inline-block;">Nombre:</div><?php echo $user->name ?></li>
          <li><div style="width:100px;display: inline-block;">Documento:</div><?php echo $user->document ?></li>
          <li><div style="width:100px;display: inline-block;">Email:</div><?php echo $user->email; ?></li>
          <li><div style="width:100px;display: inline-block;">Telefono:</div><?php echo $user->phone; ?></li>
        </ul>
      </p>
      <?php foreach( $prizes_view as $pv ): ?>
      <table width="580" border="0" cellspacing="0" cellpadding="0" style="border-top:1px solid #cccccc; margin:20px 0; padding-top:20px">
        <tr>
          <td width="45%" align="center" valign="middle"><img src="<?php echo base_url() ?>timthumb.php?src=<?php echo base_url() ?>assets/uploads/files/<?php echo $pv->prize_detail_image ?>&w=200&h=200&zc=2" width="200" height="200" style="display:block; border:0;"></td>
          <td width="55%" align="left" valign="middle"><p style="color:#c32357; font-size:18px; margin:5px 0;">&nbsp;</p>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-top:3px solid #cccccc; background-color:#eeeeee; padding:20px 10px 10px">
              <tr>
                <td><p style="font-size:13px; margin:0;"><?php echo $pv->prize_detail_reclame_description ?></p>

                  <?php if( $pv->prize_detail_has_list_of_codes == 'yes' ): ?>
                  <span style="background-color:#ffffff; color:#333333; width:100%; text-align:center; font-size:18px; display:block; margin-top:20px; padding:5px 0;"><?php echo $pv->code ?></span></td>
                <?php endif; ?>
              </tr>
            </table></td>
        </tr>
      </table>
      <?php endforeach; ?>
      </td>
  </tr>
  <tr>
    <td style="border-top: 1px solid #CCC; padding:20px 0; font-size:14px">Línea de atención Panamá 800-5454, República Dominicana 1-800-751-5454<br/>
      <a href="http://www.lge.com.pa" target="_blank" style="color:#666666; text-decoration:none;">www.lge.com.pa</a><br/>
      <span style="color:#c32357">Con LG todo es posible.</span><br/>
      <br/>
      <span style="font-size:11px;">Copyright © 2014 LG Electronics. Todos los derechos reservados.</span></td>
  </tr>
</table>
</body>
</html>
