 <!-- FOOTER-->
    <footer class="brdrTop">
       <div class="row">
            <div class="col-sm-6">
                <h4>Línea de atención Panamá 800-5454, República Dominicana 1-800-751-5454</h4>
                <a  href="http://www.lge.com.pa" target="_blank">www.lge.com.pa</a>
                <h4 class="colorRed">Con LG todo es posible</h4>
            </div>
            <div class="col-sm-6 text-right"> <a href="#modalTerms" data-toggle="modal" class="linkTerms trnstn">Términos y Condiciones</a></div>
       </div>
       <div class="row">
            <div class="col-sm-12">
            <br>
                <p class="small">Copyright © 2014 LG Electronics. Todos los derechos reservados</p>
            </div>
       </div>
    </footer>
    <!-- FOOTER END-->
</div>


</body>
</html>