<!-- CAROUSEL-->
    <div id="myCarousel" class="carousel slide" data-interval="3000" data-ride="carousel">
        <!-- Carousel indicators -->
        <ol class="carousel-indicators">
            <?php for( $i = 1; $i <= count( $slides ); $i++ ): ?>
            <li data-target="#myCarousel" data-slide-to="0" <?php if( $i == 1 ): ?>class="active"><?php endif; ?></li>
            <?php endfor; ?>
        </ol>
        <!-- Carousel items -->
        <div class="carousel-inner">
            <?php foreach( $slides as $key => $s ): ?>
            <div class="item  <?php if( $key == 0 ) echo 'active' ?>" >
            <?php if( $s->link != '' ): ?>
                <a href="<?php echo $s->link ?>" target="_BLANK" >
            <?php endif; ?>
                <img src="assets/uploads/files/<?php echo $s->media ?>" />

            <?php if( $s->link != '' ): ?>
                </a>
            <?php endif; ?>

            </div>
            <?php endforeach; ?>
        </div>
        <!-- Carousel nav -->
        <?php if( count( $slides) > 1 ): ?>
        <a class="carousel-control left hidden-sm hidden-xs" href="#myCarousel" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left"></span>
        </a>
        <a class="carousel-control right hidden-sm hidden-xs" href="#myCarousel" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right"></span>
        </a>
        <?php endif; ?>
        <!-- login -->
      <div id="login" class="shdw">
            <!-- ./FORM -->
                <form action="site/login" id="loginForm" method="POST" class="form-horizontal">
                    <fieldset>
                    <!-- Form Name -->
                    <div class="form-group">
                        <div class="col-lg-12">
                            <legend>Registra tu compra y descubre más que un TV.</legend>
                        </div>
                    </div>

                    <!-- Text input-->
                    <div class="form-group">
                      <div class="col-lg-12">
                        <input data-bv-emailaddress="true" data-bv-emailaddress-message="Dirección de correo no valida" id="email" name="email" type="text" placeholder="Email" class="form-control input-md" data-bv-notempty-message="Este campo es requerido" required>
                      </div>
                    </div>

                    <!-- Text input-->
                    <div class="form-group">
                      <div class="col-lg-12">
                        <input id="identification" data-bv-regexp="true" data-bv-regexp-regexp="^[0-9]*$" data-bv-regexp-message="La cédula solo puede contener números" name="nid" type="text" placeholder="Cédula" class="form-control input-md" data-bv-notempty-message="Este campo es requerido" required>
                      </div>
                    </div>

                    <!-- Button -->
                    <div class="form-group">

                      <div class="col-lg-12 text-right">
                        <button  id="send" name="send" class="btn btnForm trnstn" type="submit">CONTINUAR</button>
                      </div>
                    </div>
                    </fieldset>
                 </form>
            <!-- ./End FORM -->
        </div>
    </div>
    <!-- CAROUSEL END-->