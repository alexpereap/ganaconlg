<?php if(isset($edit_mode)): ?>
<script>
  var angular_preload ={
    prizes : <?php echo json_encode($angular_data['prizes']) ?>,
    stores : <?php echo json_encode( $angular_data['stores'] ) ?>,
    sellpoints : <?php echo json_encode($angular_data['sellpoints']) ?>,
    disabledStores : false,
    disabledSellPoints : false,
    selectedStoreId : <?php echo $sellpoint->store_id ?>,
    currentCode : '<?php echo $code->value ?>',
    selectedSellpointId : <?php echo $sellpoint->sellpoint_id ?>
  };
</script>
<?php endif; ?>

<section <?php if(isset($edit_mode)): ?> ng-init="init()" <?php endif; ?> >
    <?php
      if( isset( $user_purchase_data ) )
      if( count($user_purchase_data) > 0 ):
    ?>
      <div class="row">
        <div class="receipt"><strong class="colorRed"><?php echo $this->session->userdata('name') ?></strong>, tienes <strong class="colorRed"><?php echo count( $user_purchase_data ) ?></strong> factura(s) &nbsp; <a  id="triggermodalReceipt" href="#modalReceipt" data-toggle="modal" class="btnFormSmall"><i class="fa fa-file-text-o"></i></a></div>
      </div>
    <?php endif; ?>

    <div class="row">
      <div class="col-sm-12">
        <h3 class="colorRed">Ingresa tus datos:</h3>
        <h4>Completa el formulario para acceder a los beneficios adquiridos con tu compra:</h4>
      </div>
    </div>
    <div class="row">
      <form  method="POST"

      <?php if( isset($edit_mode) ): ?>
      action="site/update_purchase_data"
      edit-mode = "yes"
      <?php else: ?>
      action="site/save_purchase_data"
      <?php endif; ?>
       class="form-horizontal" id="registerForm" enctype="multipart/form-data"

      <?php if( $this->session->userdata('nid_img_front') == '' ): ?>
        validate-cc-img-front="yes"
      <?php endif; ?>

      <?php if( $this->session->userdata('nid_img_back') == '' ): ?>
        validate-cc-img-back="yes"
      <?php endif; ?>

       ><!-- form end -->

       <?php if( $this->input->get('action') == 'openBillsModal' ): ?>
        <input type="hidden" id="openBillsModal" value="yes" >
       <?php endif; ?>

      <?php if( isset($edit_mode) ): ?>
        <input type="hidden" name="purchase_data_id" value="<?php echo $purchase_data->id ?>">
        <input type="hidden" name="prize_application_id" value="<?php echo $purchase_data->prize_application_id ?>" >
      <?php endif; ?>

        <fieldset>
          <div class="col-sm-4">
            <h3><i class="fa fa-user fa-2x"></i> &nbsp;DATOS PERSONALES:</h3>
            <div class="formBox">
              <!-- Text input-->
              <div class="form-group">
                <div class="col-sm-12">
                  <input id="name" name="name" value="<?php echo $this->session->userdata('name') ?>" type="text" placeholder="Nombre" class="form-control input-md" >
                </div>
              </div>

              <!-- Text input-->
              <div class="form-group">
                <div class="col-sm-12">
                  <input id="lastName" value="<?php echo $this->session->userdata('lastname') ?>" name="lastname" type="text" placeholder="Apellido" class="form-control input-md" >
                </div>
              </div>

              <!-- Text input-->
              <div class="form-group">
                <div class="col-sm-12">
                  <input id="celPhone" value="<?php echo $this->session->userdata('cellphone') ?>" name="cellphone" maxlength="10" type="text" placeholder="Teléfono Celular" class="form-control input-md" >
                </div>
              </div>

              <?php if(isset($edit_mode)): ?>
              <!-- Text input-->
              <div class="form-group">
                <div class="col-sm-12">
                  <input id="nid" value="<?php echo $this->session->userdata('nid') ?>" name="nid" maxlength="10" type="text" placeholder="Número de cédula" class="form-control input-md" >
                </div>
              </div>
              <?php endif; ?>


            </div>
          </div>
          <div class="col-sm-4">
            <h3><i class="fa fa-barcode fa-2x"></i> &nbsp;DATOS DE TU TV:</h3>
            <div class="formBox">
              <!-- Text input-->
              <!-- <div class="form-group">
                <div class="col-sm-12">
                  <input id="inputTvRef" ng-model="tvRef"  type="text" placeholder="Referencia TV" class="form-control input-md" >
                </div>
              </div> -->

              <!-- Text input-->
              <div class="form-group">
                <div class="col-xs-10">
                  <input ng-model="currentSerial" id="serial" type="text" placeholder="Serial No." class="form-control input-md" >
                </div>
                <div class="col-xs-2">
                  <i class="fa fa-question-circle fa-2x questionMark" rel="tooltip" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-html="true" data-title="<img src='img/serial.jpg' class='responsive'>"></i>
                </div>
              </div>
              <!-- Button -->
              <div class="form-group">
                <div class="col-sm-12"> <a  class="btnFormSmall trnstn" ng-click="addTv();"   href="javascript:void(0);" >AÑADIR <i class="fa fa-plus"></i> </a> </div>
              </div>

              <!-- Product -->

            <div id="productsWrapper">
              <table class="table product marginTop">
                <tr ng-repeat="p in prizes" >
                  <td width="10%">{{ $index + 1 }}.
                    <input type="hidden" name="purchase_data_tv[{{$index}}][tv_id]" value="{{ p.tv_id }}" >
                    <input type="hidden" name="purchase_data_tv[{{$index}}][serial]" value="{{ p.serial }}" >
                  </td>
                  <td width="70%"><strong>{{ p.value }}</strong><br><input class="checkTerms" name="" type="checkbox" value=""> Acepto <a data-marcacion="{{ p.marcacion_terms }}"  class="linkTerms colorRed" href="#modalTermsPromo_{{ p.id }}" data-toggle="modal">Términos y Condiciones</a></td>
                  <td width="10%" align="center"><a data-marcacion="{{ p.marcacion_ver }}" href="#modalPromo_{{ p.id }}" data-toggle="modal" class="more link-view-benefict">Ver Beneficio</a></td>
                  <td width="10%" align="right"><a data-marcacion="{{ p.marcacion_trash }}" ng-click="removeTv( p.tv_id )" href="javascript:void(0);" class="link-delete-benefict" ><i class="fa fa-trash-o"></i></a></td>
                </tr>

              </table>
            </div>

            </div>
          </div>
          <div class="col-sm-4">
            <h3><i class="fa fa-shopping-cart fa-2x"></i> &nbsp;DATOS DE TU COMPRA:</h3>
            <div class="formBox">
              <!-- Select Basic -->
              <div class="form-group">
                <div class="col-sm-12">
                  <select ng-model="selectedCityId" ng-change="setStores(selectedCityId)" ng-model="currentCity" class="form-control">
                    <option value="">País de Compra</option>
                    <?php  foreach( $cities as $c ): ?>
                    <option <?php if( isset($edit_mode) ): ?>
                      ng-selected="<?php echo $c->id ?> == <?php echo $sellpoint->city_id ?>"
                    <?php endif; ?>
                    value="<?php echo $c->id ?>"><?php echo $c->name ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>
              </div>

              <!-- Select Basic -->
              <div class="form-group">
                <div class="col-sm-12">
                  <select  id="storeSelect" ng-model="selectedStoreId" ng-change="setSellpoints(selectedStoreId)" ng-disabled="disabledStores"  class="form-control">
                    <option value="">Cadena</option>
                    <option <?php if( isset($edit_mode) ): ?>
                    ng-selected="<?php echo $sellpoint->store_id ?> == s.id"

                    <?php endif; ?>
                    value="{{ s.id }}"
                    ng-repeat="s in stores" >{{ s.name }}</option>
                  </select>
                </div>
              </div>

              <!-- Select Basic -->
              <div class="form-group">
                <div class="col-sm-12">
                  <select id="sellpointSelect" ng-model="selectedSellpointId" ng-disabled="disabledSellPoints"  class="form-control">
                    <option value="">Punto de Venta</option>
                    <option
                    <?php if( isset($edit_mode) ): ?>
                      ng-selected="<?php echo $sellpoint->sellpoint_id ?> == s.id"
                      <?php endif; ?> value="{{ s.id }}" ng-repeat="s in sellpoints" >{{ s.name }}</option>
                  </select>
                </div>
              </div>

              <input type="hidden" name="sell_point_id" ng-model="selectedSellpointId"  value="{{ selectedSellpointId }}" >

              <!-- Text input-->
              <div class="form-group">
                <div class="col-sm-12">
                    <div class="input-group date">
                        <input id="purchase_date"
                        <?php if( isset($purchase_data) ): ?>
                          value="<?php echo $purchase_data->purchase_date ?>"
                        <?php endif; ?> name="purchase_date" type="text" class="form-control" placeholder="Fecha de la compra"><span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                    </div>
                </div>
              </div>

              <!-- File Button -->
              <div class="form-group">
                <label class="col-sm-12 control-label" for="bill_img">Foto de la factura (Peso máximo 6MB):
                  <?php if( isset($purchase_data) ): ?>
                    <a target="_BLANK" href="uploads/bill_imgs/<?php echo $purchase_data->bill_img ?>">Ver imágen actual</a>
                  <?php endif; ?>
                </label>
                <div class="col-sm-12">
                  <input  id="bill_img" name="bill_img" class="input-file" type="file">
                </div>
              </div>

              <!-- Text input-->
              <div ng-show="enable_promo_code" class="form-group">
                <div class="col-sm-12">
                  <input id="promotional_code" <?php if( isset($edit_mode) ): ?>
                      value="<?php echo $purchase_data->promotional_code_txt ?>"
                    <?php endif; ?>
                   name="promotional_code" type="text" placeholder="<?php echo $promo_code_txt ?>" class="form-control input-md" >
                </div>
              </div>
            </div>
          </div>



            <div class="col-sm-12">
            <!-- Button -->
                <div class="form-group">
                  <div class="col-lg-12  text-center">
                    <button id="send" ng-disabled="disabledSubmitBtn"  name="send" class="btn btnForm trnstn" ng-click="sendForm()" type="button"  >ENVIAR DATOS</button>
                    <!-- <a href="javascript:void(0);" ng-click="test()" >TEST</a> -->
                  </div>
                </div>
                <div class="form-group">
                  <div  class="col-lg-12 text-center">
                    <label>Leí el <a href="#modalHabeasData" data-toggle="modal" >tratamiento de datos</a><input type="checkbox" id="checkHabeas"></label>
                  </div>
                </div>
            </div>

        </fieldset>
      </form>
    </div>
  </section>