<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="es" ng-app="siteApp" >
<base href="<?php echo site_url() ?>">
<!--<![endif]-->
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<!--[if IE]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<![endif]-->
<title>LG Electronics</title>
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">
<link rel="stylesheet" href="fonts/font-awesome-4.2.0/css/font-awesome.min.css">
<link rel="stylesheet" href="css/main.css">
<link rel="shortcut icon" href="img/favicon.ico" />
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

<?php if( isset($in_login) ): ?>
    <script type="text/javascript">
        $(document).bind("mobileinit", function () {
            $.mobile.ajaxEnabled = false;
        });

        $(document).ready(function(){

            //Carousel mobile swipe
               $("#myCarousel").swiperight(function() {
                  $(this).carousel('prev');
                });
               $("#myCarousel").swipeleft(function() {
                  $(this).carousel('next');
               });
        });
    </script>
    <script src="http://code.jquery.com/mobile/1.4.3/jquery.mobile-1.4.3.min.js"></script>
<?php endif; ?>
<script src="js/bootstrap-datepicker.js"></script>
<script src="js/bootstrap-datepicker.es.js"></script>
<script src="js/functions.js?v=0.1"></script>

<link rel="stylesheet" href="vendor/jquery-ui-1.11.1.custom/jquery-ui.css" />
<script src="vendor/jquery-ui-1.11.1.custom/jquery-ui.min.js" ></script>


<link rel="stylesheet" href="vendor/bootstrapvalidator-dist-0.5.1/dist/css/bootstrapValidator.css">
<script src="vendor/bootstrapvalidator-dist-0.5.1/dist/js/bootstrapValidator.min.js" ></script>

<script src="vendor/angular/angular.min.js" ></script>


<script src="js/php.js" ></script>
<script src="js/site.js?v=0.5" ></script>
<style>
    .ui-autocomplete-loading {
        background: white url("images/ui-anim_basic_16x16.gif") right center no-repeat;
    }
</style>
</head>
<body ng-controller="prizeRequestController" >

<!-- Modal Promo -->
<div id="modalPromo_{{ p.id }}" class="modal fade" ng-repeat="p in prizes" >
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times"></i></button>
            <img src="img/lg-logo.png">
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <img src="assets/uploads/files/{{ p.image }}" class="img-responsive" />
                </div>
                <div class="col-md-12">
                    <h3 class="modal-title">{{ p.title }}</h3>
                    <span ng-bind-html="p.description"></span>
                </div>
            </div>
        </div>
        <div class="modal-footer">
        </div>
    </div>
  </div>
</div>

<!-- Modal Terms Promo-->
<div id="modalTermsPromo_{{ p.id }}" class="modal fade" ng-repeat="p in prizes" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times"></i></button>
            </div>
            <div class="modal-body">
                <h2>Términos y Condiciones</h2>
                <h3 class="modal-title">{{ p.title }}</h3>
                <span ng-bind-html="p.terms"></span>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btnFormSmall trnstn" data-dismiss="modal">ACEPTAR</button>
            </div>
        </div>
    </div>
</div>


<div id="modalHabeasData" class="modal fade" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times"></i></button>
            </div>
            <div class="modal-body">
                <h3 class="modal-title">POLÍTICAS DE MANEJO DE INFORMACIÓN PERSONAL</h3>
                <p>
    Por medio del presente documento y en uso de mis facultades legales autorizo de manera previa y expresa a las empresas LG ELECTRONICS PANAM&Aacute; S.A., en adelante LGE, como responsables del manejo de la informaci&oacute;n, a procesar, recolectar, almacenar, usar, suprimir, y/o actualizar mi informaci&oacute;n personal, de acuerdo con las finalidades que se describen a continuaci&oacute;n:</p>
<p style="margin-left:1.0cm;">
    &nbsp;</p>
<p style="margin-left:36pt;">
    1.&nbsp;&nbsp;&nbsp; Para enviar informaci&oacute;n relativa al lanzamiento de nuevos productos de LGE.</p>
<p style="margin-left:36pt;">
    2.&nbsp;&nbsp;&nbsp; Para enviar informaci&oacute;n sobre actividades promocionales de LGE.</p>
<p style="margin-left:36pt;">
    3.&nbsp;&nbsp;&nbsp; Para redimir premios asociados con la compra de productos de LGE.</p>
<p style="margin-left:36pt;">
    4.&nbsp;&nbsp;&nbsp; Para identificar atributos de&nbsp;los productos de LGE, los cuales el consumidor considere como valor agregado.&nbsp;Esta informaci&oacute;n la contemplar&aacute; LGE en las propuestas futuras de productos para ofrecerle un valor agregado al consumidor.</p>
<p style="margin-left:36pt;">
    5.&nbsp;&nbsp;&nbsp; Para una crear una base de datos de consumidor de audio LGE.</p>
<p>
    6.&nbsp;&nbsp;&nbsp; Para todos los dem&aacute;s prop&oacute;sitos que se establezcan en la pol&iacute;tica de privacidad de LG (<a href="http://www.lg.com/">http://www.lg.com/</a><a href="http://www.lg.com/pa/privacy">pa</a><a href="http://www.lg.com/pa/privacy">/privacy</a>).</p>
<p>
    &nbsp;</p>
<p>
    &nbsp;</p>
<p>
    Finalmente, declaro que conozco que puedo consultar la pol&iacute;tica de privacidad de LG ELECTRONICS PANAM&Aacute; S.A, que contiene la pol&iacute;tica interna del tratamiento de la informaci&oacute;n recogida, as&iacute; como los procedimientos de consulta y reclamaci&oacute;n que me permitir&aacute;n hacer efectivos mis derechos al acceso, consulta, rectificaci&oacute;n, actualizaci&oacute;n y supresi&oacute;n de los datos personales en la p&aacute;gina web <a href="http://www.lg.com/">http://www.lg.com/</a><a href="http://www.lg.com/pa/privacy">pa</a><a href="http://www.lg.com/pa/privacy">/privacy</a>.</p>
<p>
    &nbsp;</p>
<p>
    Todo el material en este sitio es provisto solamente con fines legales. Ninguna informaci&oacute;n acerca del sitio puede ser copiada, distribuida o transmitida en ninguna forma para uso comercial sin el consentimiento por escrito de LG Electronics Panam&aacute; S.A. LG Electronics Panam&aacute; S.A. Se reserva la total propiedad intelectual y sus derechos respectivos en cuanto a cualquier material bajado de esta p&aacute;gina.</p>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btnFormSmall trnstn" data-dismiss="modal">ACEPTAR</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal contact -->
<div id="modalContact" class="modal fade">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times"></i></button>
            </div>
            <div class="modal-body">
                <h3 class="modal-title">Contactenos</h3>
                <form id="contactForm" class="form-horizontal">
                    <fieldset>

                        <!-- Text input-->
                        <div class="form-group">
                          <label class="col-md-4 control-label" for="cformNombre">Nombre:</label>
                          <div class="col-md-6">
                              <input data-bv-notempty-message="Este campo es requerido" id="cformNombre" name="cformNombre" type="text" placeholder="Nombre" class="form-control input-md" required="">

                          </div>
                      </div>

                      <!-- Text input-->
                      <div class="form-group">
                          <label class="col-md-4 control-label" for="cformEmail">Email:</label>
                          <div class="col-md-6">
                              <input data-bv-emailaddress="true" data-bv-emailaddress-message="Dirección de correo no valida" id="cformEmail" name="cformEmail" type="text" placeholder="Email" class="form-control input-md" data-bv-notempty-message="Este campo es requerido" required="" data-bv-field="email">
                          </div>
                      </div>

                      <!-- Textarea -->
                        <div class="form-group">
                          <label class="col-md-4 control-label" for="cformMsg">Mensaje:</label>
                          <div class="col-md-6">
                            <textarea data-bv-notempty-message="Este campo es requerido" required class="form-control" rows="6" id="cformMsg" name="cformMsg"></textarea>
                          </div>
                        </div>

                      <div class="col-md-12 text-center" >
                          <button type="submit" class="btn btnFormSmall trnstn" >Envíar</button>
                      </div>

                  </fieldset>
                </form>

            </div>

        </div>
    </div>
</div>


<!-- Modal Terms -->
<div id="modalTerms" class="modal fade">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times"></i></button>
            </div>
            <div class="modal-body">
                <h3 class="modal-title">TÉRMINOS Y CONDICIONES.</h3>

                <p style="text-align: justify;">
    Bienvenidos a GanoconLG! Por medio de &eacute;sta p&aacute;gina Web (o el Sitio), podr&aacute;s registrar tus facturas, y as&iacute; redimir los regalos a los que tengas derecho, de acuerdo con las promociones vigentes para el producto LG que has adquirido.</p>
<p style="text-align: justify;">
    <strong>1. &nbsp;Aceptaci&oacute;n de estos t&eacute;rminos y condiciones.</strong></p>
<p style="text-align: justify;">
    Estos t&eacute;rminos y condiciones (que incluyen la pol&iacute;tica de privacidad de LG Electronics Panam&aacute;, S.A) rigen para el uso de esta p&aacute;gina web. S&oacute;lo por el uso, visita y/o b&uacute;squeda en &eacute;sta p&aacute;gina web, usted acepta de manera expresa estos t&eacute;rminos y condiciones. SI USTED NO ACEPTA ESTOS T&Eacute;RMINOS Y CONDICIONES POR FAVOR NO USE &Eacute;STA P&Aacute;GINA WEB. &nbsp;</p>
<p style="text-align: justify;">
    <strong>2.&nbsp;Cambio de estos t&eacute;rminos y condiciones.</strong></p>
<p style="text-align: justify;">
    LG Electronics Panam&aacute;,S.A. se reserva el derecho a modificar estos t&eacute;rminos y condiciones bajo su propia discreci&oacute;n. Cuando LG Electronics Panam&aacute;, S.A. lo haga, tambi&eacute;n actualizar&aacute; la fecha de &quot;&uacute;ltima actualizaci&oacute;n&quot; de estos t&eacute;rminos y condiciones.</p>
<p style="text-align: justify;">
    <strong>3.&nbsp;Privacidad.</strong></p>
<p style="text-align: justify;">
    La informaci&oacute;n personal registrada en &eacute;sta p&aacute;gina web se encuentra sujeta a la pol&iacute;tica de privacidad de la informaci&oacute;n de LG; al usar est&aacute; p&aacute;gina web usted otorga su autorizaci&oacute;n expresa para procesar, recolectar, almacenar, usar, actualizar, transmitir y/o transferir su informaci&oacute;n personal, de acuerdo con la pol&iacute;tica de privacidad de LG. Por favor consulte la mencionada pol&iacute;tica de privacidad descrita m&aacute;s abajo.</p>
<p style="text-align: justify;">
    <strong>4.&nbsp;Cancelaci&oacute;n</strong>.</p>
<p style="text-align: justify;">
    Usted puede cancelar su registro en esta p&aacute;gina web en cualquier momento. Recuerde que al realizar la cancelaci&oacute;n usted perder&aacute; cualquier derecho o beneficio que le otorgue &eacute;sta p&aacute;gina web.</p>
<p style="text-align: justify;">
    <strong>5.&nbsp;Materiales de este sitio.</strong></p>
<p style="text-align: justify;">
    Los usuarios de este sitio pueden bajar o imprimir una copia de cualquiera de los materiales del sitio, para uso personal, no-comercial, siempre y cuando no se modifiquen o alteren de ninguna manera los materiales, ni tampoco borrar o cambiar ning&uacute;n anuncio de derechos de autor y acerca de la marca registrada. Todo el material en este sitio es provisto solamente con fines legales. Ninguna informaci&oacute;n acerca del sitio puede ser copiada, distribuida o transmitida en ninguna forma para uso comercial sin el consentimiento por escrito de LG Electronics Panam&aacute;, S.A.. LG Electronics Panam&aacute;, S.A. se reserva la total propiedad intelectual y sus derechos respectivos en cuanto a cualquier material bajado de esta p&aacute;gina.</p>
<p style="text-align: justify;">
    &nbsp;</p>
<p style="text-align: justify;">
    <strong>6.&nbsp;Servicio de la p&aacute;gina.</strong></p>
<p style="text-align: justify;">
    &nbsp;</p>
<p style="margin-left: 21.3pt; text-align: justify;">
    a) Los usuarios de &eacute;sta p&aacute;gina Web podr&aacute;n registrar m&aacute;ximo hasta tres (3) productos por factura dentro del sistema.</p>
<p style="margin-left: 21.3pt; text-align: justify;">
    &nbsp;</p>
<p style="margin-left: 21.3pt; text-align: justify;">
    b) Usted debe ser mayor de edad para hacer uso de &eacute;sta p&aacute;gina web. Aquellas personas que sean menores de edad, deber&aacute;n actuar a trav&eacute;s de su representante legal.</p>
<p style="margin-left: 21.3pt; text-align: justify;">
    &nbsp;</p>
<p style="margin-left: 21.3pt; text-align: justify;">
    c) LG Electronics podr&aacute; terminar o restringir el uso de &eacute;sta p&aacute;gina web, sin derecho a alg&uacute;n tipo de compensaci&oacute;n ni notificaci&oacute;n, a aquellos usuarios de los cuales se sospeche que: (1) est&aacute;n violando los presentes t&eacute;rminos y condiciones, &oacute; (2) se encuentren haciendo un uso impropio o ilegal del servicio ofrecido en sitio web.</p>
<p style="margin-left: 21.3pt; text-align: justify;">
    &nbsp;</p>
<p style="margin-left: 21.3pt; text-align: justify;">
    d) Usted es responsable por actualizar y mantener vigente aquella informaci&oacute;n que usted provea relacionada con su cuenta de usuario.</p>
<p style="margin-left: 21.3pt; text-align: justify;">
    &nbsp;</p>
<p style="margin-left: 21.3pt; text-align: justify;">
    e) LG se reserva el derecho de eliminar en cualquier momento, sin previa notificaci&oacute;n, aquellas cuentas de usuario de las cuales se crea que est&aacute;n realizando actividades fraudulentas.</p>
<p style="text-align: justify;">
    &nbsp;</p>
<p style="text-align: justify;">
    <strong>7.&nbsp;Limitaci&oacute;n de Responsabilidad</strong></p>
<p style="text-align: justify;">
    &nbsp;</p>
<p style="margin-left: 21.3pt; text-align: justify;">
    a) Los materiales de la p&aacute;gina principal de LG pueden contener inexactitudes y errores tipogr&aacute;ficos. LG no garantiza la exactitud o la integridad de los materiales &oacute; la confiabilidad de cualquier dispositivo, opini&oacute;n, declaraci&oacute;n u otra informaci&oacute;n mostrada o distribuida a trav&eacute;s de la p&aacute;gina web de LG. Usted reconoce que cualquier confianza o cualquier opini&oacute;n, anuncio, declaraci&oacute;n, memorando &oacute; informaci&oacute;n debe ser bajo su exclusivo riesgo. LG se reserva el derecho, bajo su exclusiva discreci&oacute;n, a corregir cualquier error u omisi&oacute;n en cualquier porci&oacute;n del sitio. LG puede hacer cualquier otro cambio al sitio, los materiales y los productos, programas, servicios o precios (si los hubiera) descriptos en la p&aacute;gina principal de LG en cualquier momento sin aviso previo. La p&aacute;gina principal, la informaci&oacute;n y los materiales de esta p&aacute;gina, y el software disponible, son provistos sin ninguna garant&iacute;a, expl&iacute;cita o impl&iacute;cita, de ning&uacute;n tipo, incluyendo, pero no limitado a garant&iacute;as de comercializaci&oacute;n, inviolables, o aptitud para cualquier prop&oacute;sito particular, algunas jurisdicciones no permiten la exclusi&oacute;n de las garant&iacute;as impl&iacute;citas, por lo tanto las exclusiones anteriores pueden no aplicarse en este caso.</p>
<p style="margin-left: 21.3pt; text-align: justify;">
    &nbsp;</p>
<p style="margin-left: 21.3pt; text-align: justify;">
    b) El usuario de &eacute;sta p&aacute;gina web reconoce que ni LG, ni sus subsidiarias, socios, directores, empleados ni contratistas asumen responsabilidades, ya sean directas o indirectas, previstas o imprevistas, por cualquier tipo de da&ntilde;os, ya sea da&ntilde;o emergente o lucro cesante derivado el uso de este sitio web.</p>
<p style="margin-left: 21.3pt; text-align: justify;">
    &nbsp;</p>
<p style="margin-left: 21.3pt; text-align: justify;">
    c) La informaci&oacute;n de esta p&aacute;gina es provista sin ning&uacute;n tipo de garant&iacute;a expl&iacute;cita o impl&iacute;cita, incluyendo pero no limitada a las garant&iacute;as expresas de comercializaci&oacute;n, aptitud para un prop&oacute;sito particular, o inviolables. La informaci&oacute;n de este sitio puede contener inexactitudes t&eacute;cnicas o errores tipogr&aacute;ficos. Esta informaci&oacute;n puede ser cambiada o actualizada sin aviso previo. LG Electronics puede hacer mejoras y/o cambios en los productos y/o programas descriptos en esta informaci&oacute;n en cualquier momento y sin aviso previo. Los enlaces en &eacute;ste &aacute;rea lo pueden guiar a dejar el sitio web de LG Electronics, cuya direcci&oacute;n es www.lg.com.co. Los sitios enlazados no est&aacute;n bajo el control de LG Electronics. LG Electronics no es responsable de los contenidos de ning&uacute;n sitio enlazado o ning&uacute;n enlace contenido en un sitio enlazado, o de ning&uacute;n cambio o actualizaci&oacute;n de tales sitios. La inclusi&oacute;n de cualquier enlace no implica la aprobaci&oacute;n del sitio por parte de LG Electronics.&nbsp;</p>
<p style="margin-left: 21.3pt; text-align: justify;">
    &nbsp;</p>
<p style="margin-left: 21.3pt; text-align: justify;">
    d) El uso de p&aacute;ginas web, que no son propiedad de LG ELETRONICS ni son operadas por el mismo, ser&aacute; usando bajo el propio riesgo del usuario.</p>
<p style="text-align: justify;">
    &nbsp;</p>
<p style="margin-left: 21.3pt; text-align: justify;">
    e) LG es libre de usar cualquier comentario, informaci&oacute;n, idea, concepto, revisi&oacute;n, t&eacute;cnica o cualquier material contenido en aquellas comunicaciones que usted nos env&iacute;e, incluyendo respuestas a cuestionarios; este uso es a perpetuidad, sin ning&uacute;n tipo de compensaci&oacute;n, reconocimiento, pago a su favor, incluyendo y sin limitarse a desarrollar, modificar o mejorar el uso de est&aacute; pagina web.</p>
<p style="text-align: justify;">
    &nbsp;</p>
<p style="margin-left: 21.3pt; text-align: justify;">
    f) Si cualquier disposici&oacute;n de estos t&eacute;rminos y condiciones resulta invalida o calificada como ilegal por una autoridad competente, la validez de las dem&aacute;s disposiciones aqu&iacute; contenidas se mantendr&aacute;n vinculantes para el usuario de &eacute;sta p&aacute;gina web.</p>
<p style="text-align: justify;">
    &nbsp;</p>
<p style="text-align: justify;">
    <strong>8.&nbsp;Premios &amp; Promociones</strong></p>
<p style="text-align: justify;">
    Los premios otorgados por medio de &eacute;sta p&aacute;gina web pueden variar de acuerdo con la promoci&oacute;n a la cual el consumidor tenga derecho, por lo anterior, los premios tendr&aacute;n las calidades y caracter&iacute;sticas que sean descritas en los t&eacute;rminos y condiciones de cada promoci&oacute;n en particular, por lo cual, tenga en cuenta que aplican t&eacute;rminos y adicionales de acuerdo con el producto o temporada. Los premios nunca ser&aacute;n redimibles por dinero en efectivo. Ninguna promoci&oacute;n es acumulable con otra a menos que LG en su propia autonom&iacute;a lo determine as&iacute;.</p>
<p style="text-align: justify;">
    &nbsp;</p>
<p style="text-align: justify;">
    <strong>9.&nbsp;Ley aplicable</strong></p>
<p style="text-align: justify;">
    Esta Pol&iacute;tica de privacidad se ajusta a lo que al respecto dispongan la legislaci&oacute;n paname&ntilde;a vigente aplicable. Cualquier controversia surgida en relaci&oacute;n con la presente pol&iacute;tica, deber&aacute; ser resuelta por las autoridades pertinentes.</p>
<p style="text-align: justify;">
    &nbsp;</p>
<p style="text-align: justify;">
    <strong>10.&nbsp;Servicio al cliente.</strong></p>
<p style="text-align: justify;">
    Para encontrar m&aacute;s informaci&oacute;n sobre nuestros servicios, o sobre estos t&eacute;rminos y condiciones, contactarse con nosotros ingresando a la p&aacute;gina&nbsp;https://www.lg.com/pa/soporte/contactar-atencion-al-cliente&nbsp;&nbsp;o si necesita asistencia t&eacute;cnica con su cuenta, por favor escriba un correo a&nbsp;zinia.english@lge.com</p>
<p style="text-align: justify;">
    &nbsp;</p>
<p style="text-align: justify;">
    <em>&Uacute;ltima Actualizaci&oacute;n: Jueves (16) de septiembre de 2014.</em></p>
<h2 style="text-align: justify;">
    <strong>A.&nbsp;<u>POLITICA DE PRIVACIDAD</u></strong></h2>
<p style="text-align: justify;">
    &nbsp;</p>
<p style="text-align: justify;">
    Esta pol&iacute;tica de privacidad se aplica solamente a los sitios web y servicios controlados por LG Electronics Panam&aacute;, S.A. donde se aplica la presente pol&iacute;tica de privacidad (en adelante, los &quot;Sitios&quot;). Esta pol&iacute;tica de privacidad no aplica para la informaci&oacute;n que sea publicada por o entregada por terceros. Al usar los Sitios, USTED ACEPTA LOS T&Eacute;RMINOS Y CONDICIONES CONSAGRADOS EN LA PRESENTE POLITICA, ASI COMO EL PROCESAMIENTO DE INFORMACION PERSONAL PARA LOS EFECTOS INDICADOS A CONTINUACION. SI USTED NO EST&Aacute; DE ACUERDO CON LOS T&Eacute;RMINOS Y CONDICIONES DE ESTA POL&Iacute;TICA DE PRIVACIDAD, POR FAVOR NO UTILICE LOS SITIOS NI PARTICIPE EN LOS CONCURSOS Y ACTIVIDADES QUE ORGANIZA LG Electronics Panam&aacute;, S.A.</p>
<p style="text-align: justify;">
    &nbsp;</p>
<p style="text-align: justify;">
    <strong>A. Informaci&oacute;n recopilada</strong></p>
<p style="text-align: justify;">
    &nbsp;</p>
<p style="text-align: justify;">
    Esta pol&iacute;tica de privacidad aplica s&oacute;lo para informaci&oacute;n recolectada en los Sitios, actividades, concursos, promociones y sorteos.</p>
<p style="text-align: justify;">
    &nbsp;</p>
<p style="text-align: justify;">
    LG Electronics Panam&aacute;, S.A. recolecta dos (2) tipos de informaci&oacute;n de visitantes de los sitios y las aplicaciones a saber: (1) Informaci&oacute;n de Identificaci&oacute;n Personal, y (2) Informaci&oacute;n que no le identifica personalmente como direcci&oacute;n IP o &ldquo;cookies&rdquo;.</p>
<p style="text-align: justify;">
    &nbsp;</p>
<p style="margin-left: 40.5pt; text-align: justify;">
    <strong>(1)&nbsp;Informaci&oacute;n de Identificaci&oacute;n Personal.</strong></p>
<p style="text-align: justify;">
    &quot;Informaci&oacute;n de Identificaci&oacute;n Personal&quot; es informaci&oacute;n que identifica individualmente a cada&nbsp; persona, como su nombre, direcci&oacute;n, n&uacute;mero de tel&eacute;fono, direcci&oacute;n de correo electr&oacute;nico o nombre de la empresa. LG Electronics Panam&aacute;, S.A. recoge y almacena informaci&oacute;n de identificaci&oacute;n personal que los usuarios de los sitios nos proporcionan. Los siguientes son, entre otros, algunos ejemplos de maneras en que LG Electronics Panam&aacute;, S.A puede recopilar informaci&oacute;n de identificaci&oacute;n personal en los sitios para lo cual LG Electronics Panam&aacute;, S.A podr&aacute;:</p>
<p style="text-align: justify;">
    &nbsp;</p>
<p style="margin-left: 41.4pt; text-align: justify;">
    &bull; Recoger el nombre y apellido y detalles del producto o servicio si se comunica con LG Electronics Panam&aacute;, S.A para formular con una pregunta relacionada con sus productos o servicios;</p>
<p style="margin-left: 41.4pt; text-align: justify;">
    &bull; Recoger el nombre y apellido, direcci&oacute;n de correspondencia y de facturaci&oacute;n, si el usuario compra un producto o servicio en el Sitio;</p>
<p style="margin-left: 41.4pt; text-align: justify;">
    &bull; Recopilar los datos de registro sobre cualquier producto o hardware que el usuario compre de LG Electronics Panam&aacute;, S.A. y esta informaci&oacute;n puede estar vinculada al nombre de usuario y direcci&oacute;n de correspondencia;</p>
<p style="margin-left: 41.4pt; text-align: justify;">
    &bull; Recoger el nombre y apellido, direcci&oacute;n de correspondencia, direcci&oacute;n de correo electr&oacute;nico e informaci&oacute;n de facturaci&oacute;n si se descarga la informaci&oacute;n de un Sitio;</p>
<p style="margin-left: 41.4pt; text-align: justify;">
    &bull; Recopilar su nombre, direcci&oacute;n de correo electr&oacute;nico o n&uacute;mero de tel&eacute;fono si se comunica con LG Electronics Panam&aacute;, S.A para formular una pregunta.</p>
<p style="margin-left: 41.4pt; text-align: justify;">
    &bull; Recoger el nombre y apellido, fecha de nacimiento y direcci&oacute;n de correo electr&oacute;nico si el usuario decide participar en un concurso o sorteo realizado por LG Electronics Panam&aacute;, S.A. o cualquiera de sus vinculados y/o socios comerciales.</p>
<p style="text-align: justify;">
    &nbsp;</p>
<p style="text-align: justify;">
    La lista anterior establece algunas de las situaciones en las que LG Electronics Panam&aacute;, S.A. podr&aacute; recolectar informaci&oacute;n de identificaci&oacute;n personal que puede ser recolectada en los Sitios. Si usted no desea que LG Electronics Panam&aacute;, S.A. recoja su informaci&oacute;n personal, por favor abst&eacute;ngase de proporcionarla.</p>
<p style="text-align: justify;">
    &nbsp;</p>
<p style="text-align: justify;">
    <strong><u>(2) Informaci&oacute;n que no le identifica personalmente:</u></strong></p>
<p style="text-align: justify;">
    Este tipo de informaci&oacute;n puede ser la informaci&oacute;n t&eacute;cnica o informaci&oacute;n demogr&aacute;fica, como su edad, ubicaci&oacute;n, g&eacute;nero o intereses. Estos son algunos ejemplos de informaci&oacute;n personal no identificable que LG Electronics Panam&aacute;, S.A. recopila a trav&eacute;s de los Sitios:</p>
<p style="text-align: justify;">
    &nbsp;</p>
<p style="margin-left: 41.4pt; text-align: justify;">
    &bull; Protocolo de Internet (IP) - Su direcci&oacute;n IP es un n&uacute;mero que permite a las computadoras conectadas a Internet saber d&oacute;nde enviarle datos, como las p&aacute;ginas web que usted visita.</p>
<p style="margin-left: 41.4pt; text-align: justify;">
    &bull; Cookies - Una &quot;cookie&quot; es un peque&ntilde;o archivo de texto que se pueden utilizar para recopilar informaci&oacute;n sobre su actividad en el sitio. Por ejemplo, cuando alguien visita una p&aacute;gina dentro de los Sitios, se coloca una cookie en el ordenador del usuario (si el usuario acepta cookies) o se lee si el usuario ha visitado el sitio anteriormente. Usted puede configurar la mayor&iacute;a de los navegadores para que le notifique si recibe una cookie, o puede optar por bloquear las cookies de su navegador, pero si lo hace, puede que no sea capaz de disfrutar las ventajas de las caracter&iacute;sticas personalizadas que disfrutan otros usuarios de los Sitios de LG Electronics Panam&aacute;, S.A Algunas de las cookies que LG Electronics Panam&aacute;, S.A utiliza pueden ser las cookies de Flash de Adobe &copy;. Aunque son inofensivos, estos archivos pueden contener informaci&oacute;n que no le identifica personalmente y dependiendo de su navegador las cookies, pueden no ser borradas por completo. Verifique en su navegador para determinar donde se almacenan estos tipos de cookies y c&oacute;mo se pueden eliminar.</p>
<p style="margin-left: 41.4pt; text-align: justify;">
    &bull; Informaci&oacute;n sobre el producto &ndash; LG Electronics Panam&aacute;, S.A. podr&aacute; recopilar informaci&oacute;n de registro u otros detalles sobre sus productos o servicios. A menos que esta informaci&oacute;n est&eacute; vinculada al nombre o direcci&oacute;n de los usuarios de los Sitios, no se considerar&aacute; identificaci&oacute;n personal.</p>
<p style="margin-left: 41.4pt; text-align: justify;">
    &nbsp;</p>
<p style="text-align: justify;">
    LG Electronics Panam&aacute;, S.A. recoge este tipo de informaci&oacute;n que no lo identifica plenamente al respecto de todos los visitantes a nuestros sitios. Si usted no desea que LG Electronics Panam&aacute;, S.A. tenga acceso a esta informaci&oacute;n, por favor abst&eacute;ngase de visitar los Sitios Web de LG Electronics Panam&aacute;, S.A y de participar en las actividades, concursos o sorteos que organiza.</p>
<p style="text-align: justify;">
    &nbsp;</p>
<p style="text-align: justify;">
    <strong>B.&nbsp;&nbsp;&nbsp; Uso de la informaci&oacute;n recopilada por medio de los Sitios</strong></p>
<p style="text-align: justify;">
    &nbsp;</p>
<p style="text-align: justify;">
    <strong>(1) Informaci&oacute;n de Identificaci&oacute;n Personal:</strong></p>
<p style="text-align: justify;">
    LG Electronics Panam&aacute;, S.A. utiliza su informaci&oacute;n personal identificable recopilada a trav&eacute;s de sus sitios principalmente para los siguientes prop&oacute;sitos:</p>
<p style="text-align: justify;">
    &nbsp;</p>
<p style="margin-left: 41.4pt; text-align: justify;">
    &bull; Brindarle servicios;</p>
<p style="margin-left: 41.4pt; text-align: justify;">
    &bull; Para avisarle de las ofertas especiales, informaci&oacute;n actualizada y otros nuevos productos o servicios de LG Electronics Panam&aacute;, S.A., u otros terceros, o transmitirle materiales publicitarios;</p>
<p style="margin-left: 41.4pt; text-align: justify;">
    &bull; Para realizar una transacci&oacute;n o servicio solicitada por usted;</p>
<p style="margin-left: 41.4pt; text-align: justify;">
    &bull; Para cumplir con los t&eacute;rminos de una promoci&oacute;n;</p>
<p style="margin-left: 41.4pt; text-align: justify;">
    &bull; Para asegurar que el sitio se ajuste a sus necesidades;</p>
<p style="margin-left: 41.4pt; text-align: justify;">
    &bull; Para ayudar a crear y publicar contenido m&aacute;s relevante para usted;</p>
<p style="margin-left: 41.4pt; text-align: justify;">
    &bull; Para notificarle acerca de un cambio importante en esta pol&iacute;tica de privacidad o de las Condiciones de Uso, si es necesario;</p>
<p style="margin-left: 41.4pt; text-align: justify;">
    &bull; Para permitir el acceso a las &aacute;reas de entrada limitada de los Sitios, y</p>
<p style="margin-left: 41.4pt; text-align: justify;">
    &bull; Para comunicarse con usted en respuesta a formularios de registro puestos a su disposici&oacute;n como &ldquo;Contacto&rdquo; o cualquier otro tipo de formulario.</p>
<p style="margin-left: 41.4pt; text-align: justify;">
    &nbsp;</p>
<p style="text-align: justify;">
    <strong>(2) Informaci&oacute;n que no le identifica plenamente.</strong></p>
<p style="text-align: justify;">
    Esta informaci&oacute;n se utiliza como se ha descrito anteriormente y de conformidad con lo establecido por la legislaci&oacute;n vigente aplicable, incluyendo la combinaci&oacute;n de esta informaci&oacute;n con datos que s&iacute; le identifiquen parcialmente.</p>
<p style="text-align: justify;">
    &nbsp;</p>
<p style="text-align: justify;">
    <strong>C. Intercambio y divulgaci&oacute;n de informaci&oacute;n.</strong></p>
<p style="text-align: justify;">
    Informaci&oacute;n de Identificaci&oacute;n Personal. LG Electronics Panam&aacute;, S.A. podr&aacute; compartir o divulgar su informaci&oacute;n de identificaci&oacute;n personal en los siguientes casos:</p>
<p style="text-align: justify;">
    &nbsp;</p>
<p style="text-align: justify;">
    <strong>i.&nbsp;</strong>Para el cumplimiento de un servicio para usted. Por ejemplo, si usted compra o descarga un elemento de uno de los Sitios, LG Electronics Panam&aacute;, S.A. podr&aacute; compartir su informaci&oacute;n de identificaci&oacute;n personal con el fin de ofrecer y entregarle dicha compra o descarga. Igualmente, en el evento en que usted env&iacute;e por correo electr&oacute;nico cualquier tipo de pregunta o solicitud, LG Electronics Panam&aacute;, S.A. podr&aacute; utilizar su direcci&oacute;n de correo electr&oacute;nico para procesar dicha solicitud y responder a su pregunta. Adem&aacute;s, si usted compra un producto a trav&eacute;s de los Sitios, es posible que LG Electronics Panam&aacute;, S.A deba compartir sus datos con terceros socios comerciales a fin de que el producto adquirido sea entregado a usted e instalado como usted lo haya solicitado. Adicionalmente, si usted participa de cualquiera de los sorteos, concursos o promociones realizados por LGECB o cualquiera de sus socios comerciales, LG Electronics Panam&aacute;, S.A. podr&aacute; usar su informaci&oacute;n de identificaci&oacute;n personal con el fin de cumplir con los t&eacute;rminos de dicho sorteo, concurso o promoci&oacute;n.</p>
<p style="text-align: justify;">
    &nbsp;</p>
<p style="text-align: justify;">
    <strong>ii.</strong>&nbsp;Con cualquiera de sus subsidiarias y afiliadas con el fin de ofrecerle un mejor servicio. Es posible que LG Electronics Panam&aacute;, S.A. tenga que compartir sus datos con sus subsidiarias y/o afiliadas con el fin de ofrecerle los servicios que usted ha solicitado, o que LG Electronics Panam&aacute;, S.A. pueda realizar sus mejores esfuerzos para buscar asegurar que sus servicios y productos satisfagan sus necesidades. Por ejemplo, en algunos casos puede ser necesario para LG Electronics Panam&aacute;, S.A. transferir sus datos a su casa matriz con el fin de ofrecerle un producto o servicio solicitado por usted.</p>
<p style="text-align: justify;">
    &nbsp;</p>
<p style="text-align: justify;">
    <strong>iii.&nbsp;</strong>Para los socios estrat&eacute;gicos, agentes, vendedores de terceras partes u otras partes no afiliadas que ofrezcan productos o servicios que LG Electronics Panam&aacute;, S.A. considere pueden ser de inter&eacute;s para usted. Estos terceros podr&aacute;n utilizar su informaci&oacute;n de identificaci&oacute;n personal para ponerse en contacto con usted sobre ofertas o publicidad relacionada con un producto o servicio. Si usted no desea que compartamos su informaci&oacute;n personal de esta manera, por favor abstenerse de proporcionar dicha informaci&oacute;n.</p>
<p style="text-align: justify;">
    &nbsp;</p>
<p style="text-align: justify;">
    <strong>iv.&nbsp;</strong>Para los proveedores de servicios no afiliados de terceros, agentes o contratistas independientes que colaboren con LG Electronics Panam&aacute;, S.A. a mantener sus Sitios y le proporcionen distintos tipos de servicios administrativos (Incluyendo pero no limitados a, procesamiento de pedidos y cumplimiento, proporcionando servicio al cliente, mantenimiento y an&aacute;lisis de datos, env&iacute;o de comunicaciones con clientes en nombre LG Electronics Panam&aacute;, S.A., y selecci&oacute;n de ganadores y entrega de premios en concursos, sorteos y otras promociones). LG Electronics Panam&aacute;, S.A realizar&aacute; sus mejores esfuerzos para asegurar que terceros no afiliados usen la informaci&oacute;n personalmente identificable para prop&oacute;sitos distintos a proporcionar los servicios administrativos de los que estos son responsables. Debido a que tales terceros proveedores de servicios que ayudan a LG Electronics Panam&aacute;, S.A. a administrar sus Sitios tendr&aacute;n acceso a su informaci&oacute;n personal, si usted no desea que los proveedores no afiliados de servicios de terceros de LG Electronics Panam&aacute;, S.A tengan acceso a su informaci&oacute;n, por favor abstenerse de registrarse o enviar informaci&oacute;n personalmente identificable.</p>
<p style="text-align: justify;">
    &nbsp;</p>
<p style="text-align: justify;">
    <strong>v.</strong>&nbsp;Para completar su compra. Si usted decide hacer una compra en el Sitio, LG Electronics Panam&aacute;, S.A. podr&aacute; recopilar su n&uacute;mero de tarjeta de cr&eacute;dito, direcci&oacute;n de facturaci&oacute;n y cualquier otra informaci&oacute;n relacionada con dicha compra, y podr&aacute; usar esta informaci&oacute;n recopilada con el fin de cumplir con los t&eacute;rminos de su compra. LG Electronics Panam&aacute;, S.A. tambi&eacute;n podr&aacute; proporcionar dicha informaci&oacute;n, o cualquier otra informaci&oacute;n de identificaci&oacute;n personal proporcionada por usted, a terceros no afiliados seg&uacute;n sea necesario para completar su compra (por ejemplo, para procesar su tarjeta de cr&eacute;dito).</p>
<p style="text-align: justify;">
    &nbsp;</p>
<p style="text-align: justify;">
    <strong>vi.</strong>&nbsp;Para cumplir con las disposiciones legales o en la creencia de buena fe que dicha acci&oacute;n es necesaria a fin de cumplir con los requisitos establecidos por ley o cumplir con procesos legales que sean necesarios para proteger y defender los derechos o la propiedad de LG Electronics Panam&aacute;, S.A., incluidos los derechos derivados para actuar en circunstancias apremiantes para proteger la seguridad personal de nuestros usuarios finales.</p>
<p style="text-align: justify;">
    &nbsp;</p>
<p style="text-align: justify;">
    <strong>vii.</strong>&nbsp;A terceros como parte de un proceso de reorganizaci&oacute;n corporativa, incluyendo pero no limitado a, fusiones, adquisiciones y venta de todos o parte de los activos de LG Electronics Panam&aacute;, S.A.</p>
<p style="text-align: justify;">
    &nbsp;</p>
<p style="text-align: justify;">
    <strong>viii.</strong>&nbsp;Para hacer un seguimiento y analizar sin identificaci&oacute;n plena, el uso global y la informaci&oacute;n estad&iacute;stica de los visitantes y clientes de LG Electronics Panam&aacute;, S.A., y proporcionar dicha informaci&oacute;n a terceros.</p>
<p style="text-align: justify;">
    &nbsp;</p>
<p style="text-align: justify;">
    <strong>ix.&nbsp;</strong>Para protegerse contra posibles fraudes, LG Electronics Panam&aacute;, S.A. podr&aacute; verificar con terceros la informaci&oacute;n obtenida de los Sitios. En el transcurso de dicha verificaci&oacute;n, LG Electronics Panam&aacute;, S.A. podr&aacute; recibir informaci&oacute;n personalmente identificable acerca de usted en relaci&oacute;n con estos servicios. En particular, si utiliza una tarjeta de cr&eacute;dito o d&eacute;bito para la compra de productos o servicios de LG Electronics Panam&aacute;, S.A, LG Electronics Panam&aacute;, S.A. podr&aacute; utilizar la autorizaci&oacute;n de dicha tarjeta y los servicios de investigaci&oacute;n de fraude para verificar que la informaci&oacute;n de su tarjeta y la direcci&oacute;n coincide con la informaci&oacute;n que usted haya proporcionado, y que la tarjeta no ha sido reportada como perdida o robada.</p>
<p style="text-align: justify;">
    Salvo por lo descrito en esta Pol&iacute;tica de Privacidad o en el momento que se solicita la informaci&oacute;n, LG Electronics Panam&aacute;, S.A. no usar&aacute;, compartir&aacute; o revelar&aacute; su informaci&oacute;n de identificaci&oacute;n personal a terceros de ninguna otra forma.</p>
<p style="text-align: justify;">
    <strong>(2) Informaci&oacute;n que no le identifica plenamente</strong></p>
<p style="text-align: justify;">
    Dicha informaci&oacute;n es la recolectada en los Sitios de la manera descrita anteriormente, en la Secci&oacute;n A (2); Podemos compartir esta informaci&oacute;n no personal con terceros.</p>
<p style="text-align: justify;">
    &nbsp;</p>
<p style="text-align: justify;">
    <strong>D. Recopilaci&oacute;n y uso de informaci&oacute;n de ni&ntilde;os menores de 13 a&ntilde;os.</strong></p>
<p style="text-align: justify;">
    LG Electronics Panam&aacute;, S.A. no recopila ni solicita informaci&oacute;n personal de cualquier persona menor de 18 a&ntilde;os ni permite que esas personas hagan uso de los Sitios. Si usted es menor de 18 a&ntilde;os por favor no intente registrarse en el sitio o enviar cualquier informaci&oacute;n personal.</p>
<p style="text-align: justify;">
    &nbsp;</p>
<p style="text-align: justify;">
    <strong>E. Transferencia Internacional de Informaci&oacute;n</strong></p>
<p style="text-align: justify;">
    Si decide proporcionarnos informaci&oacute;n de identificaci&oacute;n personal, LG Electronics Panam&aacute;, S.A. puede transferir esa informaci&oacute;n a sus afiliados y subsidiarias o a otros terceros, a trav&eacute;s de las fronteras, y desde su pa&iacute;s o jurisdicci&oacute;n a otros pa&iacute;ses o jurisdicciones de todo el mundo. Si usted viene de otro pa&iacute;s con otra normatividad a este respecto, por lo que al proporcionar su informaci&oacute;n de identificaci&oacute;n personal, usted acepta que:</p>
<p style="text-align: justify;">
    &nbsp;</p>
<p style="margin-left: 41.4pt; text-align: justify;">
    &bull; El uso de su informaci&oacute;n personal identificable para los usos mencionados anteriormente, de acuerdo con esta pol&iacute;tica de privacidad, y</p>
<p style="margin-left: 41.4pt; text-align: justify;">
    &bull; La transferencia de su informaci&oacute;n de identificaci&oacute;n personal en Panam&aacute; o Rep&uacute;blica Domicana como se indic&oacute; anteriormente.</p>
<p style="margin-left: 41.4pt; text-align: justify;">
    &nbsp;</p>
<p style="text-align: justify;">
    <strong>F. Cancelaci&oacute;n de registro</strong></p>
<p style="text-align: justify;">
    De vez en cuando, LG Electronics Panam&aacute;, S.A. se comunica con los usuarios que se suscriben a sus servicios por correo electr&oacute;nico o mensaje de texto bajo el entendido que el usuario acepta dichos env&iacute;os mediante la aceptaci&oacute;n de la presente pol&iacute;tica. Por ejemplo, LG Electronics Panam&aacute;, S.A. utilizar&aacute; su direcci&oacute;n de correo electr&oacute;nico para confirmar solicitudes cuando aplique, enviar&aacute; notificaciones de pago, enviar&aacute; informaci&oacute;n sobre los cambios en productos y servicios, y enviar&aacute; avisos y otra informaci&oacute;n como exija la ley. En general, los usuarios que hayan optado por solicitar un producto, servicio o informaci&oacute;n a LG Electronics Panam&aacute;, S.A. no pueden optar por no recibir respuestas a sus solicitudes.</p>
<p style="text-align: justify;">
    &nbsp;</p>
<p style="text-align: justify;">
    Sin embargo, LG Electronics Panam&aacute;, S.A le ofrece la oportunidad de ejercer una opci&oacute;n de exclusi&oacute;n si no desea recibir otro tipo de comunicaci&oacute;n, como correos electr&oacute;nicos o actualizaciones de nuevos servicios y productos ofrecidos en los Sitios o si usted no quiere que LG Electronics Panam&aacute;, S.A comparta su informaci&oacute;n de identificaci&oacute;n personal con terceros. La opci&oacute;n de exclusi&oacute;n puede ser ejercida seleccionando la opci&oacute;n correspondiente en el Sitio dentro de las pantallas de recolecci&oacute;n de informaci&oacute;n de identificaci&oacute;n personal o poni&eacute;ndose en contacto con LG Electronics Panam&aacute;, S.A. LG Electronics Panam&aacute;, S.A procesara su retiro a la mayor brevedad posible, no obstante, en algunas circunstancias es posible que el usuario reciba unos cuantos mensajes adicionales hasta que el retiro sea efectivo. Usted tambi&eacute;n puede optar por no recibir dichos correos electr&oacute;nicos haciendo clic en el enlace del texto del correo electr&oacute;nico.</p>
<p style="text-align: justify;">
    &nbsp;</p>
<p style="text-align: justify;">
    <strong>G. Foros, chats y otras &aacute;reas de publicaci&oacute;n</strong></p>
<p style="text-align: justify;">
    Tenga en cuenta que cualquier informaci&oacute;n que usted incluya en un mensaje que env&iacute;e a los comit&eacute;s de revisi&oacute;n de productos, sala de chat, foros u otras &aacute;reas p&uacute;blicas, est&aacute; disponible para cualquier persona con acceso a Internet. Si no quieren que se sepa su direcci&oacute;n de correo electr&oacute;nico, por ejemplo, no lo incluya en los mensajes que escribe p&uacute;blicamente.</p>
<p style="text-align: justify;">
    &nbsp;</p>
<p style="text-align: justify;">
    <strong>H. Sitios web de terceros</strong></p>
<p style="text-align: justify;">
    Esta pol&iacute;tica de privacidad se aplica &uacute;nicamente a la informaci&oacute;n recolectada en los Sitios. Los Sitios pueden contener enlaces a otros sitios web de terceros. LG Electronics Panam&aacute;, S.A. no se hace responsable de las pol&iacute;ticas y / o pr&aacute;cticas de privacidad o del contenido de estos sitios web.</p>
<p style="text-align: justify;">
    &nbsp;</p>
<p style="text-align: justify;">
    <strong>I. Cesi&oacute;n</strong></p>
<p style="text-align: justify;">
    En el caso de que la totalidad o parte de los activos de LG Electronics Panam&aacute;, S.A. sean vendidos o adquiridos por un tercero, o en el caso de una fusi&oacute;n, nos reservamos el derecho de ceder tambi&eacute;n la informaci&oacute;n de identificaci&oacute;n personal y no personal recopilada a trav&eacute;s de los Sitios.</p>
<p style="text-align: justify;">
    &nbsp;</p>
<p style="text-align: justify;">
    <strong>J. Cambios en esta Pol&iacute;tica de Privacidad</strong></p>
<p style="text-align: justify;">
    LG Electronics Panam&aacute;, S.A. se reserva el derecho a modificar esta pol&iacute;tica de privacidad bajo su propia discreci&oacute;n. Cuando LG Electronics Panam&aacute;, S.A. lo haga, tambi&eacute;n actualizar&aacute; la fecha de &quot;&uacute;ltima actualizaci&oacute;n&quot; de esta pol&iacute;tica de privacidad.</p>
<p style="text-align: justify;">
    &nbsp;</p>
<p style="text-align: justify;">
    <strong>K. Seguridad</strong></p>
<p style="text-align: justify;">
    LG Electronics Panam&aacute;, S.A. no podr&aacute; garantizar que las transmisiones de datos a trav&eacute;s de Internet sean 100% seguras. En consecuencia, LG Electronics Panam&aacute;, S.A. no podr&aacute; asegurar o garantizar la seguridad de la informaci&oacute;n que usted transmita y el usuario, al aceptar la presente pol&iacute;tica de privacidad entiende que cualquier informaci&oacute;n que transfiera a LG Electronics Panam&aacute;, S.A. se realiza bajo su propio riesgo.</p>
<p style="text-align: justify;">
    &nbsp;</p>
<p style="text-align: justify;">
    Una vez que LGECB recibe la transmisi&oacute;n de sus datos, har&aacute; todos los esfuerzos razonables a su alcance para garantizar la seguridad en sus sistemas. LG Electronics Panam&aacute;, S.A. usa cortafuegos (&ldquo;Firewall&rdquo;) para proteger su informaci&oacute;n contra el acceso no autorizado por parte de terceros, su revelaci&oacute;n, alteraci&oacute;n o destrucci&oacute;n. Sin embargo, tenga en cuenta que esto no es una garant&iacute;a de que dicha informaci&oacute;n no pueda ser, divulgada, alterada o destruida por falla en los firewalls y software de los servidores.</p>
<p style="text-align: justify;">
    &nbsp;</p>
<p style="text-align: justify;">
    Si LG Electronics Panam&aacute;, S.A. se entera de una violaci&oacute;n de los sistemas de seguridad, realzar&aacute; todas las gestiones a su alcance para solucionar el problema e intentar&aacute; notificarle electr&oacute;nicamente, de manera que usted pueda tomar las medidas de protecci&oacute;n pertinentes. Mediante el uso de estos Sitios o proporcionando informaci&oacute;n personal identificable, el usuario acepta que LG Electronics Panam&aacute;, S.A. puede comunicarse con &eacute;l electr&oacute;nicamente en relaci&oacute;n con las cuestiones de seguridad, privacidad y administrativas relacionados con el uso de estos Sitios. En el evento de un fallo de seguridad, LG Electronics Panam&aacute;, S.A. puede colocar un aviso en sus Sitio Web afectados. Tambi&eacute;n puede enviarle al usuario un correo electr&oacute;nico a la direcci&oacute;n de correo electr&oacute;nico que haya sido proporcionado en estas circunstancias.</p>
<p style="text-align: justify;">
    &nbsp;</p>
<p style="text-align: justify;">
    <strong>L. Ley aplicable</strong></p>
<p style="text-align: justify;">
    Esta Pol&iacute;tica de privacidad se ajusta a lo que al respecto dispongan la legislaci&oacute;n paname&ntilde;a vigente aplicable.&nbsp; Cualquier controversia surgida en relaci&oacute;n con la presente pol&iacute;tica, deber&aacute; ser resuelta por las autoridades pertinentes.</p>
<p style="text-align: justify;">
    &nbsp;</p>
<p style="text-align: justify;">
    <strong>M. Contacto </strong><strong>LG Electronics Panam&aacute;, S.A<strong>.</strong></strong></p>
<p style="text-align: justify;">
    Si usted tiene alguna pregunta sobre esta pol&iacute;tica de privacidad o las pr&aacute;cticas de privacidad de LG Electronics Panam&aacute;, S.A., por favor p&oacute;ngase en contacto con nosotros. Toda la informaci&oacute;n que usted proporciona en cualquier comunicaci&oacute;n escrita referente con los Sitios tambi&eacute;n estar&aacute; cubierta por esta Pol&iacute;tica.&nbsp;&nbsp;</p>
<p style="text-align: justify;">
    &nbsp;</p>
<p style="text-align: justify;">
    <em>&Uacute;ltima Actualizaci&oacute;n: Jueves (16) de octubre de 2014.</em></p>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btnFormSmall trnstn" data-dismiss="modal">ACEPTAR</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Receipt -->
<div id="modalReceipt" class="modal fade">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times"></i></button>
            </div>
            <?php foreach( $user_purchase_data as $key => $upd ): ?>
            <div class="modal-body">
                <h3 class="modal-title"><i class="fa fa-file-text-o"></i> FACTURA: <?php echo $upd->bill_number ?></h3>
                <table class="table product">
                  <tr>
                    <td width="30%" class="bgGrey text-center">Referencia de TV</td>
                    <td width="40%"  class="bgGrey text-center">Premio</td>
                    <td width="30%" class="bgGrey text-center">Estado de la solicitud</td>
                  </tr>

                  <?php foreach( $upd->prizes_detail  as $pd): ?>
                  <tr>
                    <td class="text-center"><strong><?php echo $pd->tv_reference ?></strong></td>
                    <td class="text-center"><img src="assets/uploads/files/<?php echo $pd->prize_detail_image ?>" class="imgPrize" width="120" height="120"><?php echo $pd->prize_detail_title ?></td>
                    <td class="text-center"><?php echo $upd->prize_application_status_human ?></td>
                  </tr>
                  <?php endforeach; ?>

                </table>
            </div>
            <?php endforeach ?>
            <div class="modal-footer"></div>
        </div>
    </div>
</div>

<!-- Modal Prize -->
<div id="modalPrize" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog">
   <div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times"></i></button>
    </div>
    <div class="modal-body prizeImg"></div>
    <div class="modal-footer"></div>
   </div>
  </div>
</div>

<!-- Modal Alert -->
<div id="modalAlert" class="modal fade">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times"></i></button>
            </div>
            <div class="modal-body text-center">
                <h4 class="colorRed"> {{ modalAlertMessage }} </h4>
            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>

<div id="modalErrorAlert" class="modal fade">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times"></i></button>
            </div>
            <div class="modal-body text-center">
                 <h4 class="colorRed"> {{ modalErrorAlertTitle }} </h4>
                <span ng-bind-html="modalErrorAlertMessage.message"></span>
            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>

<!-- Modal error contact form -->
<div id="modalSuccessCForm" class="modal fade">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times"></i></button>
            </div>
            <div class="modal-body text-center">
                <h4 class="colorRed">¡Gracias por contactarnos!</h4>
                <span>Nos colocaremos en contacto contigo lo más pronto posible.</span>
            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>

<span ng-bind-html="p.terms"></span>

<!-- CONTENT -->
<div class="container">
    <!-- HEADER -->
    <header class="brdrBottom">
        <img class="logoBrand" src="img/lg-logo.png">
        <ul id="social" class="text-right">
            <li><span><a data-toggle="modal" href="#modalContact">Contactenos</a></span></li>
            <li class="hidden-xs"><span>Síguenos en:</span></li>
            <li><a href="http://www.facebook.com/LGLatino" target="_blank" class="btn btSocial btFacebook trnstn"><i class="fa fa-facebook"></i></a></li>
            <li><a href="http://www.twitter.com/LivingLG" target="_blank" class="btn btSocial btTwitter trnstn"><i class="fa fa-twitter"></i></a></li>
            <li><a href="http://www.youtube.com/livingLGvideo" target="_blank" class="btn btSocial btYoutube trnstn"><i class="fa fa-youtube"></i></a></li>
        </ul>
        <div class="clear-both"></div>
    </header>
    <!-- HEADER END-->