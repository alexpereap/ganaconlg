<form action="<?php echo base_url() ?>site/deny_request" method="GET" >
          <input type="hidden" name="token" value="<?php echo $token ?>" >
            <p style="font-size:14px; margin-top:20px">La información presenta inconsistencias. Selecciona una de estas opciones:</p>
            <select name="denial_option" size="1" style="width:80%; border:1px solid #ccc; padding:5px; color:#666666; font-family:Tahoma, Geneva, sans-serif; font-size:14px;">
              <?php foreach( $denial_options as $do ): ?>
                <option value="<?php echo $do->id ?>"><?php echo $do->value ?></option>
              <?php endforeach; ?>
            </select>
            <p style="font-size:14px; margin-top:20px">O escribe la razón por la cual va a ser denegada la información:</p>
            <textarea name="observation" style="width:80%; height:200px; border:1px solid #ccc; padding:5px; color:#666; font-family:Tahoma, Geneva, sans-serif; color:#666666; font-size:14px;"></textarea>
            <input value="" type="submit"  style="
    border: 0;
    margin: 0;
    padding: 0;
    cursor:pointer !important;
 background-image:url(<?php echo base_url() ?>img/email/btDecline.gif); width:290px; height:70px;" />
          </form>