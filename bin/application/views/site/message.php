<!-- CONTENT -->
  <section>
   
    <div class="row">
      <div class="col-sm-12">
        <h3 class="colorRed"><?php echo $message_title ?></h3>
        <p><?php echo $message_content ?></p>
      </div>
    </div>
    
  </section>
  <!-- CONTENT END --> 