<!-- CONTENT -->
  <section>

    <div class="row">
      <div class="col-sm-12">
        <h3 class="colorRed">¡Felicitaciones! Tu formulario fue enviado con éxito.</h3>
        <h4>Te hemos enviado un correo a <span class="colorRed"><?php echo $prize_request->email ?></span> con esta misma información</h4>
        <h2 class="colorRed">TUS BENEFICIOS:</h2>
        <table class="table product">
          <tr>
            <td width="30%" class="bgGrey text-center">Referencia de TV</td>
            <td width="40%" class="bgGrey text-center">Premio</td>
          </tr>
          <?php foreach( $prizes_view as $pr ):  ?>
          <tr>
            <td class="text-center"><strong><?php echo $pr->tv_reference ?></strong></td>
            <td class="text-center"><a  title="<?php echo $pr->prize_detail_title ?>" class="linkTerms"><div><img src="timthumb.php?src=<?php echo base_url() ?>assets/uploads/files/<?php echo $pr->prize_detail_image ?>&w=120&h=120&zc=2&q=100" class="imgPrize" width="120" height="120"></div><p>Este es tu código Netflix: </p><?php echo $pr->prize_detail_title ?></a></td>
          </tr>
        <?php endforeach; ?>

        </table>
        <a class="btn btnForm trnstn" href="<?php echo base_url('site/register') ?>">REGISTRAR OTRA COMPRA</a>
      </div>

    </div>

  </section>
  <!-- CONTENT END -->