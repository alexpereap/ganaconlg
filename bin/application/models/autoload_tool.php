<?php

/** Include path **/
set_include_path( dirname(__FILE__).'/../../vendor/php_excel_1.8.0/Classes' );

/** PHPExcel_IOFactory */
include 'PHPExcel/IOFactory.php';

class Autoload_tool extends CI_Model{

    private $problematic_serials = array();
    private $success_beneficts = 0;

    public function execFile( $filename, $benefict_img, $public_img ){

        $inputFileName = dirname(__FILE__) . '/../../assets/uploads/autoload_files/' . $filename;

        $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
        $objReader     = PHPExcel_IOFactory::createReader($inputFileType);
        $objReader->setReadDataOnly(true);
        $objPHPExcel   = $objReader->load($inputFileName);

        $objPHPExcel->setActiveSheetIndex(0);
        $sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);


        foreach( $sheetData as $key => $row ){
            // row 1 its informative
            if( $key > 1 ){

                // serial, netflix code, datestart, dateend are mandatory
                if( !empty($row['A']) && !empty($row['B']) && !empty($row['C']) && !empty($row['D']) ){

                    $data = array();

                    // adds TV serial
                    $data['reference'] = $row['A'];

                    $tv_id = $this->main->saveTv( $data );

                    if( $tv_id ){


                        // adds benefict data
                        $data = array(
                            'title'               => $row['B'],
                            'image'               => $benefict_img,
                            'has_list_of_codes'   => 'no',
                            'reclaim_description' => '<p>Ingresa a <a href="http://www.netflix.com/code" >www.netflix.com/code</a> y utiliza el siguiente c&oacute;digo <strong>'. $row['B'] .'</strong> para que empieces a disfrutar tu suscripci&oacute;n al mejor entretenimiento con <strong>NETFLIX</strong></p>',
                            'available_from'      => $row['C'],
                            'available_until'     => $row['D']
                        );

                        $prize_detail_id = $this->main->savePrizeDetail($data);

                        if( $prize_detail_id ){

                            // associates a tv with a benefict

                            empty( $row['G'] ) ? $terms = '' : $terms = $row['G'];

                            $prize_package = array(
                                'fk_tv_id'        => $tv_id,
                                'title'           => $row['E'],
                                'image'           => $public_img,
                                'description'     => $row['F'],
                                'quanty'          => 1,
                                'terms'           => $terms,
                                'has_promo_code'  => 'no'
                            );

                            $prize_id = $this->main->savePrizePackageData( $prize_package );

                            if( $prize_id ){


                                $prize_elements = array();

                                // Prize ITEM assoc
                                $prize_elements[] = array(
                                    'fk_prize_id'        => $prize_id,
                                    'fk_prize_detail_id' => $prize_detail_id
                                );

                                $this->main->savePrizeElements($prize_elements);

                                $this->success_beneficts++;

                            } else {
                                $this->addProblemSerial( $key, $row['A'], 'Inconsistencia de datos' );
                            }

                        }
                        else {
                            $this->addProblemSerial( $key, $row['A'], 'Inconsistencia de datos' );
                        }

                    } else {
                        $this->addProblemSerial( $key, $row['A'], 'Inconsistencia de datos' );
                    }


                } else {
                    $this->addProblemSerial( $key, $row['A'], 'Datos faltantes (serial o netflix o fecha de inicio o fecha de fin)' );
                }

            }

        }

        return array(
            'success' => $this->success_beneficts,
            'errors'  => $this->problematic_serials
        );
    }

    private function addProblemSerial($row, $serial, $error){

        $this->problematic_serials[] = array(
            'fila'   => $row,
            'serial' => $serial,
            'error'  => $error
            );
    }

}