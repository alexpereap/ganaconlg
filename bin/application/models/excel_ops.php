<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/** Include path **/
set_include_path( dirname(__FILE__).'/../../vendor/php_excel_1.8.0/Classes' );

/** PHPExcel_IOFactory */
include 'PHPExcel/IOFactory.php';


class Excel_ops extends CI_Model{

    public function setPrizeCodes( $prize_detail_id, $filename ){

        $inputFileName = dirname(__FILE__) . '/../../assets/uploads/files/' . $filename;

        $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
        $objReader     = PHPExcel_IOFactory::createReader($inputFileType);
        $objReader->setReadDataOnly(true);
        $objPHPExcel   = $objReader->load($inputFileName);

        $objPHPExcel->setActiveSheetIndex(0);
        $sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);

        foreach( $sheetData as $val ){

            $data = array(
                'fk_prize_detail_id' => $prize_detail_id,
                'value' => $val["A"]
            );

            $this->db->insert('tbl_prize_codes', $data);
        }
    }

    public function setLgCodes($filename){
        $inputFileName = dirname(__FILE__) . '/../../assets/uploads/files/' . $filename;

        $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
        $objReader     = PHPExcel_IOFactory::createReader($inputFileType);
        $objReader->setReadDataOnly(true);
        $objPHPExcel   = $objReader->load($inputFileName);

        $objPHPExcel->setActiveSheetIndex(0);
        $sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);

        foreach( $sheetData as $val ){

            $data = array(
                'value' => $val["A"]
            );

            $this->db->insert('tbl_codes', $data);
        }

    }
}